# This file is licensed under the Affero General Public License version 3 or
# later. See the COPYING file.
app_name=$(notdir $(CURDIR))
build_tools_directory=$(CURDIR)/build/tools
composer=$(shell which composer 2> /dev/null)
mysqlpath=$(shell which mysql 2> /dev/null)
versionvalue=$(shell grep '<version>' appinfo/info.xml | sed 's|.*<version>\(.*\)\.\(.*\)\.\(.*\)</version>.*|\1.\2|g')
buildvalue=$(shell git log -1 --format=%at 2> /dev/null | gawk '{print strftime("%Y%m%d%H", $$0)}' 2> /dev/null)

MYSQL_DATABASE?=db_nctest
MYSQL_USER?=user_nctest
MYSQL_PASSWORD?=Nctest123
MYSQL_HOST?=localhost
MYSQL_ROOT_PASSWORD?=
NEXTCLOUD_VERSION?=22.1.1
NEXTCLOUD_PROD_PATH?=/var/www/nextcloud

mysqloption=""
ifneq ("$(MYSQL_ROOT_PASSWORD)", "")
mysqloption="-p$(MYSQL_ROOT_PASSWORD)"
endif

all: dev-setup lint version build-js-production dist/Cerana.zip dist/Cerana.pdf

test: clean-test dev-setup dist/webresult.xml dist/testresult.xml

# Dev env management
dev-setup: clean clean-dev composer npm-init


# Installs and updates the composer dependencies. If composer is not installed
# a copy is fetched from the web
composer:
ifeq (, $(composer))
	@echo "No composer command available, downloading a copy from the web"
	mkdir -p $(build_tools_directory)
	curl -sS https://getcomposer.org/installer | php
	mv composer.phar $(build_tools_directory)
	php $(build_tools_directory)/composer.phar install --prefer-dist
	php $(build_tools_directory)/composer.phar update --prefer-dist
else
	composer install --prefer-dist
	composer update --prefer-dist
endif

npm-init:
	npm install

npm-ci:	
	npm ci

npm-update:
	npm update

# Building
build-js:
	npm run dev
	
version:
# | sed 's|.*<version>\([0-9]+\)\.\([0-9]+\)\..*</version>.*|\1.\2|g'
	@echo "version = $(versionvalue) - build = $(buildvalue)"
	sed -i 's|<version>.*</version>|<version>$(versionvalue).$(buildvalue)</version>|g' appinfo/info.xml
	sed -i 's|"version": ".*",|"version": "$(versionvalue).$(buildvalue)",|g' package.json
	sed -i 's|"version": ".*",|"version": "$(versionvalue).$(buildvalue)",|g' package-lock.json
	sed -i "s|version = '.*'|version = '$(versionvalue)'|g" doc/conf.py
	sed -i "s|release = '.*'|release = '$(versionvalue).$(buildvalue)'|g" doc/conf.py

build-js-production:
	npm run build
	rm -rf dist/Cerana.zip

watch-js:
	npm run watch

# Linting
lint:
	npm run lint

lint-fix:
	npm run lint:fix

# Style linting
stylelint:
	npm run stylelint

stylelint-fix:
	npm run stylelint:fix

# Cleaning
clean:
	rm -rf js/*
	rm -rf dist

clean-dev:
	rm -rf node_modules
	rm -rf venv
	rm -rf build
	rm -rf package-lock.json
	
clean-test:	
	rm -rf nextcloud_data nextcloud_web

dist:
	mkdir -p dist

dist/Cerana.zip: build-js-production dist
	zip -r dist/Cerana.zip appinfo lib templates js img css composer.json composer.lock COPYING README.md 
	touch dist/Cerana-$(versionvalue).$(buildvalue).version

dist/webresult.xml: dist
	npm run test
	cp webresult.xml dist/webresult.xml

venv:
	python3 -m virtualenv venv
	./venv/bin/python -m pip install -U Sphinx

dist/Cerana.pdf: dist venv
	cd doc && ../venv/bin/sphinx-build -b latex -d ../build/doctrees -D latex_paper_size=a4 . ../build/latex
	cd build/latex && $(MAKE) all-pdf
	cp build/latex/Cerana.pdf dist

nextcloud_web:
	curl -L https://github.com/nextcloud/server/archive/v$(NEXTCLOUD_VERSION).zip -o server-master.zip
	unzip server-master.zip > /dev/null
	mv server-$(NEXTCLOUD_VERSION) nextcloud_web
	rm -rf server-master.zip
	curl -L https://github.com/nextcloud/3rdparty/archive/v$(NEXTCLOUD_VERSION).zip -o 3rdparty-master.zip
	cd nextcloud_web && rm -rf 3rdparty && unzip ../3rdparty-master.zip > /dev/null
	mv nextcloud_web/3rdparty-$(NEXTCLOUD_VERSION) nextcloud_web/3rdparty
	rm -rf 3rdparty-master.zip
	echo '' >> nextcloud_web/.eslintignore
	echo '**/*.js' >> nextcloud_web/.eslintignore
	cd nextcloud_web && $(MAKE) clean npm-update build-js 2>&1 1>"nexcloud_build.log"

nextcloud_data: nextcloud_web
ifneq ("$(wildcard $(mysqlpath))","")
	$(mysqlpath) -h $(MYSQL_HOST) -u root $(mysqloption) -e "DROP DATABASE $(MYSQL_DATABASE);" || echo "no drop $(MYSQL_DATABASE)" 
	$(mysqlpath) -h $(MYSQL_HOST) -u root $(mysqloption) -e "DROP USER $(MYSQL_USER)@$(MYSQL_HOST);" || echo "no drop $(MYSQL_USER)"
	$(mysqlpath) -h $(MYSQL_HOST) -u root $(mysqloption) -e "CREATE USER $(MYSQL_USER)@$(MYSQL_HOST) identified by '$(MYSQL_PASSWORD)';"
	$(mysqlpath) -h $(MYSQL_HOST) -u root $(mysqloption) -e "CREATE DATABASE $(MYSQL_DATABASE) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;"
	$(mysqlpath) -h $(MYSQL_HOST) -u root $(mysqloption) -e "GRANT ALL PRIVILEGES on $(MYSQL_DATABASE).* to $(MYSQL_USER)@$(MYSQL_HOST);"
	$(mysqlpath) -h $(MYSQL_HOST) -u root $(mysqloption) -e "FLUSH privileges;"
endif
	mkdir -p nextcloud_data
	php nextcloud_web/occ maintenance:install --database "mysql" --database-host $(MYSQL_HOST) --database-name $(MYSQL_DATABASE) --database-user $(MYSQL_USER) --database-pass "$(MYSQL_PASSWORD)" --admin-user "admin" --admin-pass "admin" --data-dir "$(PWD)/nextcloud_data"

dist/testresult.xml: dist nextcloud_data
	rm -rf $(PWD)/nextcloud_web/apps/cerana_adhesion
	mkdir -p $(PWD)/nextcloud_web/apps/cerana_adhesion
	cp -r $(PWD)/appinfo $(PWD)/nextcloud_web/apps/cerana_adhesion/appinfo
	cp -r $(PWD)/tests $(PWD)/nextcloud_web/apps/cerana_adhesion/tests
	cp -r $(PWD)/lib $(PWD)/nextcloud_web/apps/cerana_adhesion/lib
	cp $(PWD)/phpunit.xml $(PWD)/nextcloud_web/apps/cerana_adhesion/phpunit.xml
	rm -rf $(PWD)/nextcloud_web/apps/cerana_adhesion/tests/ui
	php nextcloud_web/occ app:enable cerana_adhesion
	cd nextcloud_web/apps/cerana_adhesion/ && XDEBUG_MODE=coverage ../../../vendor/phpunit/phpunit/phpunit -c phpunit.xml --log-junit ../../../dist/testresult.xml --coverage-clover ../../../dist/php-coverage.xml --coverage-html ../../../dist/cover --coverage-text --colors=never
	
deploy: dist/Cerana.zip
	rm -rf $(NEXTCLOUD_PROD_PATH)/apps/cerana_adhesion
	mkdir -p $(NEXTCLOUD_PROD_PATH)/apps/cerana_adhesion
	unzip dist/Cerana.zip -d $(NEXTCLOUD_PROD_PATH)/apps/cerana_adhesion
	sudo -u www-data php $(NEXTCLOUD_PROD_PATH)/occ app:enable cerana_adhesion

manual: dist version venv
	rm -rf dist/html
	cd doc && ../venv/bin/sphinx-build -b html -d ../build/doctrees . ../dist/html
ifneq ("$(wildcard ../web)","")
	rm -rf ../web/doc
	mkdir -p ../web/doc
	cp -r dist/html/* ../web/doc
endif