
DELETE FROM `oc_cerana_practice`;
DELETE FROM `oc_cerana_responsability`;
DELETE FROM `oc_cerana_subscription`;
DELETE FROM `oc_cerana_adherent`;

DELETE FROM `oc_cerana_structure`;
DELETE FROM `oc_cerana_typeadhesion`;
DELETE FROM `oc_cerana_typerespo`;
DELETE FROM `oc_cerana_typepractice`;
DELETE FROM `oc_cerana_level`;

INSERT INTO `oc_cerana_level`
(id,level,name,comment) VALUES
(1,1,'National','Échelon national'),
(2,2,'Secteur','Échelon intermédiaire'),
(3,3,'Antenne locale','Échelon local');
ALTER TABLE `oc_cerana_level` AUTO_INCREMENT = 4;

INSERT INTO `oc_cerana_structure`
(id,level,parent,name,phone1,phone2,email,website,address,postalcode,city,comment) VALUES
(1,1,null,'Le national','','','','','','','',''),
(2,2,1,'Ile-de-France','','','','','','','',''),
(3,2,1,'Nord-Ouest','','','','','','','',''),
(4,2,1,'Nord-Est','','','','','','','',''),
(5,2,1,'Sud-Est','','','','','','','',''),
(6,2,1,'Sud-Ouest','','','','','','','',''),
(7,2,1,'Dom-Tom','','','','','','','',''),
(8,3,2,'Paris','','','','','','75001','Paris',''),
(9,3,2,'Versailles','','','','','','78000','Versailles',''),
(10,3,2,'Saint-Denis','','','','','','93200','Saint-Denis',''),
(11,3,2,'Melun','','','','','','77000','Melun',''),
(12,3,3,'Rouen','','','','','','76000','Rouen',''),
(13,3,3,'Rennes','','','','','','35000','Rennes',''),
(14,3,3,'Le Mans','','','','','','72000','Le Mans',''),
(15,3,3,'Brest','','','','','','29200','Brest',''),
(16,3,4,'Strasbourg','','','','','','67000','Strasbourg',''),
(17,3,4,'Nancy','','','','','','54000','Nancy',''),
(18,3,4,'Reims','','','','','','51100','Reims',''),
(19,3,4,'Dijon','','','','','','21000','Dijon',''),
(20,3,5,'Lyon','','','','','','69001','Lyon',''),
(21,3,5,'Marseille','','','','','','13001','Marseille',''),
(22,3,5,'Valence','','','','','','26000','Valence',''),
(23,3,5,'Montpellier','','','','','','34000','Montpellier',''),
(24,3,6,'Toulouse','','','','','','31000','Toulouse',''),
(25,3,6,'Bordeau','','','','','','33000','Bordeau',''),
(26,3,6,'Bayonne','','','','','','64100','Bayonne',''),
(27,3,6,'Brive-la-Gaillarde','','','','','','19100','Brive-la-Gaillarde',''),
(28,3,7,'Pointe-à-Pitre','','','','','','97110','Pointe-à-Pitre',''),
(29,3,7,'Fort-de-France','','','','','','97200','Fort-de-France',''),
(30,3,7,'Cayenne','','','','','','97300','Cayenne',''),
(31,3,7,'Saint-Denis','','','','','','97400','Saint-Denis','');
ALTER TABLE `oc_cerana_structure` AUTO_INCREMENT = 32;

INSERT INTO `oc_cerana_typeadhesion` 
(id,name) VALUES
(1,'Membres d\'honneur'),
(2,'Adhésion jeune'),
(3,'Adhésion responsable');
ALTER TABLE `oc_cerana_typeadhesion` AUTO_INCREMENT = 4;

INSERT INTO `oc_cerana_typerespo` 
(id,level,name) VALUES
(1,1,'Big boss'),
(2,1,'Grand argentier'),
(3,2,'Boss de secteur'),
(4,2,'Trésorier de secteur'),
(5,2,'Gentil Organisateur'),
(6,3,'Boss local'),
(7,3,'Trésorier local'),
(8,3,'Organisateur local');
ALTER TABLE `oc_cerana_typerespo` AUTO_INCREMENT = 9;

INSERT INTO `oc_cerana_typepractice`
(id,name) VALUES
(1,'Les ptits'),
(2,'Les moyens'),
(3,'Les grands');
ALTER TABLE `oc_cerana_typepractice` AUTO_INCREMENT = 3;
