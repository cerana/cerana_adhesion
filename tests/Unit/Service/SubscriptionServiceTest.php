<?php
namespace OCA\CeranaAdhesion\Tests\Unit\Service;

use PHPUnit\Framework\TestCase;
use OCP\AppFramework\Db\DoesNotExistException;
use OCA\CeranaAdhesion\Service\AdhesionObjectNotFound;
use OCA\CeranaAdhesion\Service\SubscriptionService;
use OCA\CeranaAdhesion\Db\Subscription;
use OCA\CeranaAdhesion\Db\SubscriptionMapper;
use OCA\CeranaAdhesion\Db\PracticeMapper;
use OCA\CeranaAdhesion\Db\Practice;
use OCA\CeranaAdhesion\Db\ResponsabilityMapper;
use OCA\CeranaAdhesion\Db\Responsability;

class SubscriptionServiceTest extends TestCase
{

    private $service;

    private $mapper;

    public function setUp(): void
    {
        $this->mapper = $this->getMockBuilder(SubscriptionMapper::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->mapper->method('createPractice')->willReturn($this->getMockBuilder(PracticeMapper::class)
            ->disableOriginalConstructor()
            ->getMock());
        $this->mapper->method('createResponsability')->willReturn($this->getMockBuilder(ResponsabilityMapper::class)
            ->disableOriginalConstructor()
            ->getMock());
        $this->service = new SubscriptionService($this->mapper);
    }

    public function testUpdate()
    {
        // the existing object
        $object = Subscription::fromRow([
            'id' => 3,
            'adherent' => 2,
            'typeadhesion' => 4,
            'structure' => 7,
            'dateref' => '2018/09/01',
            'comment' => 'foofoofoo'
        ]);
        $this->mapper->expects($this->once())
            ->method('find')
            ->with($this->equalTo(3))
            ->will($this->returnValue($object));
        
        // the object when updated
        $updatedobject = Subscription::fromRow([
            'id' => 3
        ]);
        $updatedobject->setAdherent(4);
        $updatedobject->setTypeadhesion(7);
        $updatedobject->setStructure(5);
        $updatedobject->setDateref("2019/09/01");
        $updatedobject->setComment("blabla");
        $this->mapper->expects($this->once())
            ->method('update')
            ->with($this->equalTo($updatedobject))
            ->will($this->returnValue($updatedobject));
        
        $result = $this->service->update(3, 4, 7, 5, "2019/09/01", "blabla");
        
        $this->assertEquals($updatedobject, $result);
    }

    public function testUpdateNotFound()
    {
        $this->expectException(AdhesionObjectNotFound::class);
        // test the correct status code if no object is found
        $this->mapper->expects($this->once())
            ->method('find')
            ->with($this->equalTo(3))
            ->will($this->throwException(new DoesNotExistException('')));
        
        $this->service->update(3, 4, 7, 5, "2019/09/01", "blabla");
    }
}
