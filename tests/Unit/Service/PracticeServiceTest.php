<?php
namespace OCA\CeranaAdhesion\Tests\Unit\Service;

use PHPUnit\Framework\TestCase;
use OCP\AppFramework\Db\DoesNotExistException;
use OCA\CeranaAdhesion\Service\AdhesionObjectNotFound;
use OCA\CeranaAdhesion\Service\PracticeService;
use OCA\CeranaAdhesion\Db\Practice;
use OCA\CeranaAdhesion\Db\PracticeMapper;

class PracticeServiceTest extends TestCase
{

    private $service;

    private $mapper;

    public function setUp(): void
    {
        $this->mapper = $this->getMockBuilder(PracticeMapper::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->service = new PracticeService($this->mapper);
    }

    public function testUpdate()
    {
        // the existing object
        $object = Practice::fromRow([
            'id' => 3,
            'subscription' => 2,
            'typepractice' => 25,
            'comment' => "pract. #2"
        ]);
        $this->mapper->expects($this->once())
            ->method('find')
            ->with($this->equalTo(3))
            ->will($this->returnValue($object));
        
        // the object when updated
        $updatedobject = Practice::fromRow([
            'id' => 3
        ]);
        $updatedobject->setSubscription(7);
        $updatedobject->setTypepractice(33);
        $updatedobject->setComment("pract. #3");
        $this->mapper->expects($this->once())
            ->method('update')
            ->with($this->equalTo($updatedobject))
            ->will($this->returnValue($updatedobject));
        
        $result = $this->service->update(3, 7, 33, "pract. #3");
        
        $this->assertEquals($updatedobject, $result);
    }

    public function testUpdateNotFound()
    {
        $this->expectException(AdhesionObjectNotFound::class);
        // test the correct status code if no object is found
        $this->mapper->expects($this->once())
            ->method('find')
            ->with($this->equalTo(3))
            ->will($this->throwException(new DoesNotExistException('')));
        
        $this->service->update(3, 7, 33, "resp. #3");
    }

    public function testCreate()
    {
        // the existing object
        $newobject = Practice::fromRow([]);
        $newobject->setSubscription(4);
        $newobject->setTypepractice(17);
        $newobject->setComment("pract. #4");
        
        $expectedobject = Practice::fromRow([
            'id' => 54,
            'subscription' => 4,
            'typepractice' => 17,
            'comment' => "pract. #4"
        ]);
        $this->mapper->expects($this->once())
            ->method('insert')
            ->with($this->equalTo($newobject))
            ->will($this->returnValue($expectedobject));
        $result = $this->service->create(4, 17, "pract. #4");
        $this->assertEquals($expectedobject, $result);
    }

    public function testDelete()
    {
        // the existing object
        $object = Practice::fromRow([
            'id' => 34,
            'subscription' => 7,
            'typepractice' => 35,
            'comment' => "pract. #7"
        ]);
        $this->mapper->expects($this->once())
            ->method('find')
            ->with($this->equalTo(34))
            ->will($this->returnValue($object));
        $this->mapper->expects($this->once())
            ->method('delete')
            ->with($this->equalTo($object))
            ->will($this->returnValue($object));
        
        $result = $this->service->delete(34);
        
        $this->assertEquals($object, $result);
    }

    public function testDeleteNotFound()
    {
        $this->expectException(AdhesionObjectNotFound::class);
        // test the correct status code if no object is found
        $this->mapper->expects($this->once())
            ->method('find')
            ->with($this->equalTo(13))
            ->will($this->throwException(new DoesNotExistException('')));
        $this->service->delete(13);
    }

    public function testFindAll()
    {
        // the existing object
        $objects = [
            'nb' => 55,
            'list' => [
                Practice::fromRow([
                    'id' => 34,
                    'subscription' => 7,
                    'typepractice' => 11,
                    'comment' => "pract. #11"
                ]),
                Practice::fromRow([
                    'id' => 34,
                    'subscription' => 7,
                    'typepractice' => 21,
                    'comment' => "pract. #21"
                ])
            ]
        ];
        $this->mapper->expects($this->once())
            ->method('findAll')
            ->with($this->equalTo(34), $this->equalTo(0), $this->equalTo(10))
            ->will($this->returnValue($objects));
        
        $result = $this->service->findAllEx(34, 0, 10);
        $this->assertEquals($objects, $result);
    }

    public function testFindAdherents()
    {
        // the existing object
        $objects = [
            Practice::fromRow([
                'id' => 34,
                'subscription' => 7,
                'typepractice' => 11,
                'comment' => "pract. #11"
            ]),
            Practice::fromRow([
                'id' => 34,
                'subscription' => 7,
                'typepractice' => 21,
                'comment' => "pract. #21"
            ])
        ];
        $this->mapper->expects($this->once())
            ->method('findAdherents')
            ->with($this->equalTo(77), $this->equalTo('2018-09-01'), $this->equalTo(8), $this->equalTo(1), $this->equalTo(100))
            ->will($this->returnValue($objects));
        
        $result = $this->service->findAdherents(77, '2018-09-01', 8, 1, 100);
        $this->assertEquals($objects, $result);
    }
}
