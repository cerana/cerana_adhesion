<?php
namespace OCA\CeranaAdhesion\Tests\Unit\Service;

use PHPUnit\Framework\TestCase;
use OCP\AppFramework\Db\DoesNotExistException;
use OCA\CeranaAdhesion\Service\AdhesionObjectNotFound;
use OCA\CeranaAdhesion\Service\TypeResponsabilityService;
use OCA\CeranaAdhesion\Db\TypeResponsability;
use OCA\CeranaAdhesion\Db\TypeResponsabilityMapper;

class TypeResponsabilityServiceTest extends TestCase
{

    private $service;

    private $mapper;

    public function setUp(): void
    {
        $this->mapper = $this->getMockBuilder(TypeResponsabilityMapper::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->service = new TypeResponsabilityService($this->mapper);
    }

    public function testUpdate()
    {
        // the existing object
        $object = TypeResponsability::fromRow([
            'id' => 3,
            'level' => 2,
            'name' => "resp. #2"
        ]);
        $this->mapper->expects($this->once())
            ->method('find')
            ->with($this->equalTo(3))
            ->will($this->returnValue($object));
        
        // the object when updated
        $updatedobject = TypeResponsability::fromRow([
            'id' => 3
        ]);
        $updatedobject->setLevel(3);
        $updatedobject->setName("resp. #3");
        $this->mapper->expects($this->once())
            ->method('update')
            ->with($this->equalTo($updatedobject))
            ->will($this->returnValue($updatedobject));
        
        $result = $this->service->update(3, 3, "resp. #3");
        
        $this->assertEquals($updatedobject, $result);
    }

    public function testUpdateNotFound()
    {
        $this->expectException(AdhesionObjectNotFound::class);
        // test the correct status code if no object is found
        $this->mapper->expects($this->once())
            ->method('find')
            ->with($this->equalTo(3))
            ->will($this->throwException(new DoesNotExistException('')));
        
        $this->service->update(3, 3, "resp. #3");
    }

    public function testCreate()
    {
        // the existing object
        $newobject = TypeResponsability::fromRow([]);
        $newobject->setLevel(4);
        $newobject->setName('nice');
        
        $expectedobject = TypeResponsability::fromRow([
            'id' => 10,
            'level' => 4,
            'name' => 'nice'
        ]);
        $this->mapper->expects($this->once())
            ->method('insert')
            ->with($this->equalTo($newobject))
            ->will($this->returnValue($expectedobject));
        $result = $this->service->create(4, "nice");
        $this->assertEquals($expectedobject, $result);
    }

    public function testDelete()
    {
        // the existing object
        $object = TypeResponsability::fromRow([
            'id' => 3,
            'name' => "nice"
        ]);
        $this->mapper->expects($this->once())
            ->method('find')
            ->with($this->equalTo(3))
            ->will($this->returnValue($object));
        $this->mapper->expects($this->once())
            ->method('delete')
            ->with($this->equalTo($object))
            ->will($this->returnValue($object));
        
        $result = $this->service->delete(3);
        
        $this->assertEquals($object, $result);
    }

    public function testDeleteNotFound()
    {
        $this->expectException(AdhesionObjectNotFound::class);
        // test the correct status code if no object is found
        $this->mapper->expects($this->once())
            ->method('find')
            ->with($this->equalTo(3))
            ->will($this->throwException(new DoesNotExistException('')));
        $this->service->delete(3);
    }

    public function testFindAll()
    {
        // the existing object
        $objects = [
            'nb' => 127,
            'list' => [
                TypeResponsability::fromRow([
                    'id' => 3,
                    'name' => "nice"
                ]),
                TypeResponsability::fromRow([
                    'id' => 4,
                    'name' => "bad"
                ]),
                TypeResponsability::fromRow([
                    'id' => 7,
                    'name' => "wonderfull"
                ])
            ]
        ];
        $this->mapper->expects($this->once())
            ->method('findAll')
            ->with($this->equalTo(1), $this->equalTo(25))
            ->will($this->returnValue($objects));
        
        $result = $this->service->findAll(1, 25);
        $this->assertEquals($objects, $result);
    }
}
