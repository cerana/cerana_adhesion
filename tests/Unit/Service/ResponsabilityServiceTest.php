<?php
namespace OCA\CeranaAdhesion\Tests\Unit\Service;

use PHPUnit\Framework\TestCase;
use OCP\AppFramework\Db\DoesNotExistException;
use OCA\CeranaAdhesion\Service\AdhesionObjectNotFound;
use OCA\CeranaAdhesion\Service\ResponsabilityService;
use OCA\CeranaAdhesion\Db\Responsability;
use OCA\CeranaAdhesion\Db\ResponsabilityMapper;

class ResponsabilityServiceTest extends TestCase
{

    private $service;

    private $mapper;

    public function setUp(): void
    {
        $this->mapper = $this->getMockBuilder(ResponsabilityMapper::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->service = new ResponsabilityService($this->mapper);
    }

    public function testUpdate()
    {
        // the existing object
        $object = Responsability::fromRow([
            'id' => 3,
            'subscription' => 2,
            'structure' => 10,
            'typeresponsability' => 25,
            'comment' => "resp. #2"
        ]);
        $this->mapper->expects($this->once())
            ->method('find')
            ->with($this->equalTo(3))
            ->will($this->returnValue($object));
        
        // the object when updated
        $updatedobject = Responsability::fromRow([
            'id' => 3
        ]);
        $updatedobject->setSubscription(7);
        $updatedobject->setTyperesponsability(33);
        $updatedobject->setStructure(15);
        $updatedobject->setComment("resp. #3");
        $this->mapper->expects($this->once())
            ->method('update')
            ->with($this->equalTo($updatedobject))
            ->will($this->returnValue($updatedobject));
        
        $result = $this->service->update(3, 7, 33, 15, "resp. #3");
        
        $this->assertEquals($updatedobject, $result);
    }

    public function testUpdateNotFound()
    {
        $this->expectException(AdhesionObjectNotFound::class);
        // test the correct status code if no object is found
        $this->mapper->expects($this->once())
            ->method('find')
            ->with($this->equalTo(3))
            ->will($this->throwException(new DoesNotExistException('')));
        
        $this->service->update(3, 7, 33, 15, "resp. #3");
    }

    public function testCreate()
    {
        // the existing object
        $newobject = Responsability::fromRow([]);
        $newobject->setSubscription(2);
        $newobject->setTyperesponsability(25);
        $newobject->setStructure(10);
        $newobject->setComment("resp. #2");
        
        $expectedobject = Responsability::fromRow([
            'id' => 4,
            'subscription' => 2,
            'typeresponsability' => 25,
            'structure' => 10,
            'comment' => "resp. #2"
        ]);
        $this->mapper->expects($this->once())
            ->method('insert')
            ->with($this->equalTo($newobject))
            ->will($this->returnValue($expectedobject));
        $result = $this->service->create(2, 25, 10, "resp. #2");
        $this->assertEquals($expectedobject, $result);
    }

    public function testDelete()
    {
        // the existing object
        $object = Responsability::fromRow([
            'id' => 3,
            'subscription' => 2,
            'structure' => 10,
            'typeresponsability' => 25,
            'comment' => "resp. #2"
        ]);
        $this->mapper->expects($this->once())
            ->method('find')
            ->with($this->equalTo(3))
            ->will($this->returnValue($object));
        $this->mapper->expects($this->once())
            ->method('delete')
            ->with($this->equalTo($object))
            ->will($this->returnValue($object));
        
        $result = $this->service->delete(3);
        
        $this->assertEquals($object, $result);
    }

    public function testDeleteNotFound()
    {
        $this->expectException(AdhesionObjectNotFound::class);
        // test the correct status code if no object is found
        $this->mapper->expects($this->once())
            ->method('find')
            ->with($this->equalTo(3))
            ->will($this->throwException(new DoesNotExistException('')));
        $this->service->delete(3);
    }

    public function testFindAll()
    {
        // the existing object
        $objects = [
            'nb' => 94,
            'list' => [
                Responsability::fromRow([
                    'id' => 3,
                    'subscription' => 2,
                    'structure' => 10,
                    'typeresponsability' => 25,
                    'comment' => "resp. #2"
                ]),
                Responsability::fromRow([
                    'id' => 4,
                    'subscription' => 2,
                    'structure' => 7,
                    'typeresponsability' => 12,
                    'comment' => "resp. #7"
                ])
            ]
        ];
        $this->mapper->expects($this->once())
            ->method('findAll')
            ->with($this->equalTo(2), $this->equalTo(0), $this->equalTo(25))
            ->will($this->returnValue($objects));
        
        $result = $this->service->findAllEx(2, 0, 25);
        $this->assertEquals($objects, $result);
    }

    public function testFindAdherents()
    {
        // the existing object
        $objects = [
            Responsability::fromRow([
                'id' => 3,
                'subscription' => 2,
                'structure' => 10,
                'typeresponsability' => 25,
                'comment' => "resp. #2"
            ]),
            Responsability::fromRow([
                'id' => 4,
                'subscription' => 2,
                'structure' => 7,
                'typeresponsability' => 12,
                'comment' => "resp. #7"
            ])
        ];
        $this->mapper->expects($this->once())
            ->method('findAdherents')
            ->with($this->equalTo(7), $this->equalTo('2018-09-01'), $this->equalTo(4), $this->equalTo(33))
            ->will($this->returnValue($objects));
        
        $result = $this->service->findAdherents(7, '2018-09-01', 4, 33);
        $this->assertEquals($objects, $result);
    }
}
