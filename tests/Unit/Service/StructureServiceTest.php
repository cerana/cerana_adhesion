<?php
namespace OCA\CeranaAdhesion\Tests\Unit\Service;

use PHPUnit\Framework\TestCase;
use OCP\AppFramework\Db\DoesNotExistException;
use OCA\CeranaAdhesion\Service\AdhesionObjectNotFound;
use OCA\CeranaAdhesion\Service\StructureService;
use OCA\CeranaAdhesion\Db\Structure;
use OCA\CeranaAdhesion\Db\StructureMapper;

class StructureServiceTest extends TestCase
{

    private $service;

    private $mapper;

    public function setUp(): void
    {
        $this->mapper = $this->getMockBuilder(StructureMapper::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->service = new StructureService($this->mapper);
    }

    public function testUpdate()
    {
        // the existing object
        $object = Structure::fromRow([
            'id' => 3,
            'name' => "Truc",
            'phone1' => "123456789",
            'phone2' => "",
            'email' => "",
            'website' => "",
            'address' => "rue machin",
            'postalcode' => "99123",
            'city' => "TRUC",
            'comment' => "blabla",
            'level' => 2,
            'parent' => 1
        ]);
        $this->mapper->expects($this->once())
            ->method('find')
            ->with($this->equalTo(3))
            ->will($this->returnValue($object));
        
        // the object when updated
        $updatedobject = Structure::fromRow([
            'id' => 3
        ]);
        $updatedobject->setName("Truc de fou");
        $updatedobject->setPhone1("0987654321");
        $updatedobject->setPhone2("0123456789");
        $updatedobject->setEmail("contact@truc.org");
        $updatedobject->setWebsite("http://truc.org");
        $updatedobject->setAddress("bd machin");
        $updatedobject->setPostalcode("98765");
        $updatedobject->setCity("ville");
        $updatedobject->setComment("foo foo foo");
        $updatedobject->setLevel(3);
        $updatedobject->setParent(2);
        $this->mapper->expects($this->once())
            ->method('update')
            ->with($this->equalTo($updatedobject))
            ->will($this->returnValue($updatedobject));
        
        $result = $this->service->update(3, "Truc de fou", "0987654321", "0123456789", "contact@truc.org", "http://truc.org", "bd machin", "98765", "ville", "foo foo foo", 3, 2);
        
        $this->assertEquals($updatedobject, $result);
    }

    public function testUpdateNotFound()
    {
        $this->expectException(AdhesionObjectNotFound::class);
        // test the correct status code if no object is found
        $this->mapper->expects($this->once())
            ->method('find')
            ->with($this->equalTo(3))
            ->will($this->throwException(new DoesNotExistException('')));
        
        $result = $this->service->update(3, "Truc de fou", "0987654321", "0123456789", "contact@truc.org", "http://truc.org", "bd machin", "98765", "ville", "foo foo foo", 3, 2);
    }

    public function testCreate()
    {
        // the existing object
        $newobject = Structure::fromRow([]);
        $newobject->setName("Truc de fou");
        $newobject->setPhone1("0987654321");
        $newobject->setPhone2("0123456789");
        $newobject->setEmail("contact@truc.org");
        $newobject->setWebsite("http://truc.org");
        $newobject->setAddress("bd machin");
        $newobject->setPostalcode("98765");
        $newobject->setCity("ville");
        $newobject->setComment("foo foo foo");
        $newobject->setLevel(3);
        $newobject->setParent(2);
        
        $expectedobject = Structure::fromRow([
            'id' => 10,
            'name' => "Truc de fou",
            'phone1' => "0987654321",
            'phone2' => "0123456789",
            'email' => "contact@truc.org",
            'website' => "http://truc.org",
            'address' => "bd machin",
            'postalcode' => "98765",
            'city' => "ville",
            'comment' => "foo foo foo",
            'level' => 3,
            'parent' => 2
        ]);
        $this->mapper->expects($this->once())
            ->method('insert')
            ->with($this->equalTo($newobject))
            ->will($this->returnValue($expectedobject));
        $result = $this->service->create("Truc de fou", "0987654321", "0123456789", "contact@truc.org", "http://truc.org", "bd machin", "98765", "ville", "foo foo foo", 3, 2);
        $this->assertEquals($expectedobject, $result);
    }

    public function testDelete()
    {
        // the existing object
        $object = Structure::fromRow([
            'id' => 3,
            'name' => "nice"
        ]);
        $this->mapper->expects($this->once())
            ->method('find')
            ->with($this->equalTo(3))
            ->will($this->returnValue($object));
        $this->mapper->expects($this->once())
            ->method('delete')
            ->with($this->equalTo($object))
            ->will($this->returnValue($object));
        
        $result = $this->service->delete(3);
        
        $this->assertEquals($object, $result);
    }

    public function testDeleteNotFound()
    {
        $this->expectException(AdhesionObjectNotFound::class);
        // test the correct status code if no object is found
        $this->mapper->expects($this->once())
            ->method('find')
            ->with($this->equalTo(3))
            ->will($this->throwException(new DoesNotExistException('')));
        $this->service->delete(3);
    }

    public function testFindAll()
    {
        // the existing object
        $objects = [
            'nb' => 178,
            'list' => [
                Structure::fromRow([
                    'id' => 3,
                    'name' => "nice"
                ]),
                Structure::fromRow([
                    'id' => 5,
                    'name' => "bad"
                ]),
                Structure::fromRow([
                    'id' => 8,
                    'name' => "wonderfull"
                ]),
                Structure::fromRow([
                    'id' => 13,
                    'name' => "aaaaa"
                ]),
                Structure::fromRow([
                    'id' => 21,
                    'name' => "zzzzz"
                ]),
                Structure::fromRow([
                    'id' => 44,
                    'name' => "why not"
                ])
            ]
        ];
        $this->mapper->expects($this->once())
            ->method('findAll')
            ->with($this->equalTo(1), $this->equalTo(25))
            ->will($this->returnValue($objects));
        
        $result = $this->service->findAll(1, 25);
        $this->assertEquals($objects, $result);
    }
}
