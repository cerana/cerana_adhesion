<?php
namespace OCA\CeranaAdhesion\Tests\Unit\Service;

use PHPUnit\Framework\TestCase;
use OCP\AppFramework\Db\DoesNotExistException;
use OCA\CeranaAdhesion\Service\AdhesionObjectNotFound;
use OCA\CeranaAdhesion\Service\LevelService;
use OCA\CeranaAdhesion\Db\Level;
use OCA\CeranaAdhesion\Db\LevelMapper;

class LevelServiceTest extends TestCase
{

    private $service;

    private $mapper;

    public function setUp(): void
    {
        $this->mapper = $this->getMockBuilder(LevelMapper::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->service = new LevelService($this->mapper);
    }

    public function testUpdate()
    {
        // the existing object
        $object = Level::fromRow([
            'id' => 3,
            'level' => 2,
            'name' => "level #2",
            'Comment' => "2nd level"
        ]);
        $this->mapper->expects($this->once())
            ->method('find')
            ->with($this->equalTo(3))
            ->will($this->returnValue($object));
        
        // the object when updated
        $updatedobject = Level::fromRow([
            'id' => 3
        ]);
        $updatedobject->setLevel(3);
        $updatedobject->setName("level #3");
        $updatedobject->setComment("3st level");
        $this->mapper->expects($this->once())
            ->method('update')
            ->with($this->equalTo($updatedobject))
            ->will($this->returnValue($updatedobject));
        
        $result = $this->service->update(3, 3, "level #3", "3st level");
        
        $this->assertEquals($updatedobject, $result);
    }

    public function testUpdateNotFound()
    {
        $this->expectException(AdhesionObjectNotFound::class);
        // test the correct status code if no object is found
        $this->mapper->expects($this->once())
            ->method('find')
            ->with($this->equalTo(3))
            ->will($this->throwException(new DoesNotExistException('')));
        
        $this->service->update(3, 3, "level #3", "3st level");
    }

    public function testCreate()
    {
        // the existing object
        $newobject = Level::fromRow([]);
        $newobject->setLevel(3);
        $newobject->setName("level #3");
        $newobject->setComment("3st level");
        
        $expectedobject = Level::fromRow([
            'id' => 4,
            'level' => 3,
            'name' => "level #3",
            'comment' => "3st level"
        ]);
        $this->mapper->expects($this->once())
            ->method('insert')
            ->with($this->equalTo($newobject))
            ->will($this->returnValue($expectedobject));
        $result = $this->service->create(3, "level #3", "3st level");
        $this->assertEquals($expectedobject, $result);
    }

    public function testDelete()
    {
        // the existing object
        $object = Level::fromRow([
            'id' => 3,
            'level' => 3,
            'name' => "level #3",
            'comment' => "3st level"
        ]);
        $this->mapper->expects($this->once())
            ->method('find')
            ->with($this->equalTo(3))
            ->will($this->returnValue($object));
        $this->mapper->expects($this->once())
            ->method('delete')
            ->with($this->equalTo($object))
            ->will($this->returnValue($object));
        
        $result = $this->service->delete(3);
        
        $this->assertEquals($object, $result);
    }

    public function testDeleteNotFound()
    {
        $this->expectException(AdhesionObjectNotFound::class);
        // test the correct status code if no object is found
        $this->mapper->expects($this->once())
            ->method('find')
            ->with($this->equalTo(3))
            ->will($this->throwException(new DoesNotExistException('')));
        $this->service->delete(3);
    }

    public function testFindAll()
    {
        // the existing object
        $objects = [
            'nb' => 30,
            'list' => [
                Level::fromRow([
                    'id' => 3,
                    'level' => 3,
                    'name' => "level #3",
                    'comment' => "3st level"
                ]),
                Level::fromRow([
                    'id' => 4,
                    'level' => 4,
                    'name' => "level #4",
                    'comment' => "4st level"
                ]),
                Level::fromRow([
                    'id' => 7,
                    'level' => 5,
                    'name' => "level #5",
                    'comment' => "5st level"
                ])
            ]
        ];
        $this->mapper->expects($this->once())
            ->method('findAll')
            ->with($this->equalTo(1), $this->equalTo(25))
            ->will($this->returnValue($objects));
        
        $result = $this->service->findAll(1, 25);
        $this->assertEquals($objects, $result);
    }
}
