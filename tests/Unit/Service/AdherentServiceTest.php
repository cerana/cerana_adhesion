<?php
namespace OCA\CeranaAdhesion\Tests\Unit\Service;

use PHPUnit\Framework\TestCase;
use OCP\AppFramework\Db\DoesNotExistException;
use OCA\CeranaAdhesion\Service\AdhesionObjectNotFound;
use OCA\CeranaAdhesion\Service\AdherentService;
use OCA\CeranaAdhesion\Db\Adherent;
use OCA\CeranaAdhesion\Db\AdherentMapper;
use OCA\CeranaAdhesion\Db\Subscription;
use OCA\CeranaAdhesion\Db\SubscriptionMapper;
use OCA\CeranaAdhesion\Db\Practice;
use OCA\CeranaAdhesion\Db\PracticeMapper;
use OCA\CeranaAdhesion\Db\Responsability;
use OCA\CeranaAdhesion\Db\ResponsabilityMapper;

class AdherentServiceTest extends TestCase
{

    private $service;

    private $mapper;

    private $subscription_mapper;

    private $practice_mapper;

    private $responsability_mapper;

    public function setUp(): void
    {
        $this->mapper = $this->getMockBuilder(AdherentMapper::class)
            ->disableOriginalConstructor()
            ->getMock();
        
        $this->subscription_mapper = $this->getMockBuilder(SubscriptionMapper::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->practice_mapper = $this->getMockBuilder(PracticeMapper::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->responsability_mapper = $this->getMockBuilder(ResponsabilityMapper::class)
            ->disableOriginalConstructor()
            ->getMock();
        
        $this->subscription_mapper->method('createPractice')->willReturn($this->practice_mapper);
        $this->subscription_mapper->method('createResponsability')->willReturn($this->responsability_mapper);
        $this->mapper->method('createSubscription')->willReturn($this->subscription_mapper);
        $this->service = new AdherentService($this->mapper);
    }

    public function testUpdate()
    {
        // the existing adherent
        $adherent = [
            'id' => 3,
            'num' => "FR0001245",
            'civility' => 1,
            'lastname' => "Attand",
            'firstname' => "Charles",
            'birthdaydate' => "1987-06-17",
            'birthdayplace' => "ICI",
            'email' => "charles@attend.org",
            'address' => "rue machin",
            'postalcode' => "99123",
            'city' => "TRUC",
            'country' => "FR",
            'phone1' => "09876441121",
            'phone2' => "",
            'comment' => "",
            'photoid' => "",
            'uid' => null
        ];
        $this->mapper->expects($this->once())
            ->method('find')
            ->with($this->equalTo(3))
            ->will($this->returnValue($adherent));
        
        // the adherent when updated
        $updatedAdherent = Adherent::fromRow([
            'id' => 3,
            'num' => "FR0001245"
        ]);
        $updatedAdherent->setCivility(2);
        $updatedAdherent->setLastname("nom");
        $updatedAdherent->setFirstname("prenom");
        $updatedAdherent->setBirthdaydate("1991-11-02");
        $updatedAdherent->setBirthdayplace("lieu");
        $updatedAdherent->setEmail("truc@machin.fr");
        $updatedAdherent->setAddress("place centrale");
        $updatedAdherent->setPostalcode("12345");
        $updatedAdherent->setCity("ville");
        $updatedAdherent->setCountry("CH");
        $updatedAdherent->setPhone1("654321147");
        $updatedAdherent->setPhone2("484128171");
        $updatedAdherent->setComment("blabla");
        $updatedAdherent->setPhotoid("qldflqdmfkmlqdfsù.jpg");
        
        $expectedAdherent = Adherent::fromRow([
            'id' => 3,
            'num' => "FR0001245",
            'civility' => 2,
            'lastname' => "nom",
            'firstname' => "prenom",
            'birthdaydate' => "1991-11-02",
            'birthdayplace' => "lieu",
            'email' => "truc@machin.fr",
            'address' => "place centrale",
            'postalcode' => "12345",
            'city' => "ville",
            'country' => "CH",
            'phone1' => "654321147",
            'phone2' => "484128171",
            'comment' => "blabla",
            'photoid' => "qldflqdmfkmlqdfsù.jpg"
        ]);
        $this->mapper->expects($this->once())
            ->method('update')
            ->with($this->equalTo($updatedAdherent))
            ->will($this->returnValue($expectedAdherent));
        
        $result = $this->service->update(3, 2, "nom", "prenom", "1991-11-02", "lieu", "truc@machin.fr", "place centrale", "12345", "ville", "CH", "654321147", "484128171", "blabla", "qldflqdmfkmlqdfsù.jpg");
        
        $this->assertEquals($expectedAdherent, $result);
    }

    public function testUpdateNotFound()
    {
        $this->expectException(AdhesionObjectNotFound::class);
        // test the correct status code if no adherent is found
        $this->mapper->expects($this->once())
            ->method('find')
            ->with($this->equalTo(3))
            ->will($this->throwException(new DoesNotExistException('')));
        
        $this->service->update(3, 1, "nom", "prenom", "2001-03-24", "lieuNaissance", "email", "adresse", "codePostal", "ville", "FR", "tel1", "tel2", "commentaires", "photoId");
    }

    public function testCreate()
    {
        // the existing object
        $newobject = Adherent::fromRow([]);
        $newobject->setNum("CH0001245");
        $newobject->setCivility(2);
        $newobject->setLastname("nom");
        $newobject->setFirstname("prenom");
        $newobject->setBirthdaydate("1991-11-02");
        $newobject->setBirthdayplace("lieu");
        $newobject->setEmail("truc@machin.fr");
        $newobject->setAddress("place centrale");
        $newobject->setPostalcode("12345");
        $newobject->setCity("ville");
        $newobject->setCountry("CH");
        $newobject->setPhone1("654321147");
        $newobject->setPhone2("484128171");
        $newobject->setComment("blabla");
        $newobject->setPhotoid("");
        
        $expectedobject = Adherent::fromRow([
            'id' => 10,
            'num' => "CH0001245",
            'civility' => 2,
            'lastname' => "nom",
            'firstname' => "prenom",
            'birthdaydate' => "1991-11-02",
            'birthdayplace' => "lieu",
            'email' => "truc@machin.fr",
            'address' => "place centrale",
            'postalcode' => "12345",
            'city' => "ville",
            'country' => "CH",
            'phone1' => "654321147",
            'phone2' => "484128171",
            'comment' => "blabla",
            'photoid' => ""
        ]);
        $this->mapper->expects($this->once())
            ->method('getNextNum')
            ->will($this->returnValue(1245));
        $this->mapper->expects($this->once())
            ->method('insert')
            ->with($this->equalTo($newobject))
            ->will($this->returnValue($expectedobject));
        $result = $this->service->create(2, "nom", "prenom", "1991-11-02", "lieu", "truc@machin.fr", "place centrale", "12345", "ville", "CH", "654321147", "484128171", "blabla", "");
        $this->assertEquals($expectedobject, $result);
    }

    public function testCreateWithSubscription()
    {
        // the existing object
        $newobject = Adherent::fromRow([]);
        $newobject->setNum("CH0001245");
        $newobject->setCivility(2);
        $newobject->setLastname("nom");
        $newobject->setFirstname("prenom");
        $newobject->setBirthdaydate("1991-11-02");
        $newobject->setBirthdayplace("lieu");
        $newobject->setEmail("truc@machin.fr");
        $newobject->setAddress("place centrale");
        $newobject->setPostalcode("12345");
        $newobject->setCity("ville");
        $newobject->setCountry("CH");
        $newobject->setPhone1("654321147");
        $newobject->setPhone2("484128171");
        $newobject->setComment("blabla");
        $newobject->setPhotoid("");
        
        $expectedobject = Adherent::fromRow([
            'id' => 10,
            'num' => "CH0001245",
            'civility' => 2,
            'lastname' => "nom",
            'firstname' => "prenom",
            'birthdaydate' => "1991-11-02",
            'birthdayplace' => "lieu",
            'email' => "truc@machin.fr",
            'address' => "place centrale",
            'postalcode' => "12345",
            'city' => "ville",
            'country' => "CH",
            'phone1' => "654321147",
            'phone2' => "484128171",
            'comment' => "blabla",
            'photoid' => ""
        ]);
        $this->mapper->expects($this->once())
            ->method('getNextNum')
            ->will($this->returnValue(1245));
        $this->mapper->expects($this->once())
            ->method('insert')
            ->with($this->equalTo($newobject))
            ->will($this->returnValue($expectedobject));
        
        $new_subscription = new Subscription();
        $new_subscription->setAdherent(10);
        $new_subscription->setTypeadhesion(8);
        $new_subscription->setStructure(4);
        $new_subscription->setDateref("2019-09-01");
        $new_subscription->setComment("");
        $this->subscription_mapper->expects($this->once())
            ->method('insert')
            ->with($this->equalTo($new_subscription))
            ->will($this->returnValue(Subscription::fromRow(array_merge($new_subscription->jsonSerialize(), [
            'id' => 123
        ]))));
        
        $new_practice = new Practice();
        $new_practice->setSubscription(123);
        $new_practice->setTypepractice(5);
        $new_practice->setComment('');
        $this->practice_mapper->expects($this->once())
            ->method('insert')
            ->with($this->equalTo($new_practice))
            ->will($this->returnValue(Practice::fromRow(array_merge($new_practice->jsonSerialize(), [
            'id' => 456
        ]))));
        
        $new_responsability = new Responsability();
        $new_responsability->setSubscription(123);
        $new_responsability->setTyperesponsability(11);
        $new_responsability->setStructure(4);
        $new_responsability->setComment('');
        $this->responsability_mapper->expects($this->once())
            ->method('insert')
            ->with($this->equalTo($new_responsability))
            ->will($this->returnValue(Responsability::fromRow(array_merge($new_responsability->jsonSerialize(), [
            'id' => 789
        ]))));
        
        $result = $this->service->create(2, "nom", "prenom", "1991-11-02", "lieu", "truc@machin.fr", "place centrale", "12345", "ville", "CH", "654321147", "484128171", "blabla", "", "2019-09-01", 4, 8, 5, 11);
        $this->assertEquals($expectedobject, $result);
    }

    public function testDelete()
    {
        // the existing object
        $object = [
            'id' => 7,
            'lastname' => "nom",
            'firstname' => "prenom"
        ];
        $this->mapper->expects($this->once())
            ->method('find')
            ->with($this->equalTo(7))
            ->will($this->returnValue($object));
        $object = Adherent::fromRow($object);
        $this->mapper->expects($this->once())
            ->method('delete')
            ->with($this->equalTo($object))
            ->will($this->returnValue($object));
        
        $result = $this->service->delete(7);
        
        $this->assertEquals($object, $result);
    }

    public function testDeleteNotFound()
    {
        $this->expectException(AdhesionObjectNotFound::class);
        // test the correct status code if no object is found
        $this->mapper->expects($this->once())
            ->method('find')
            ->with($this->equalTo(111))
            ->will($this->throwException(new DoesNotExistException('')));
        $this->service->delete(111);
    }

    public function testFindAll()
    {
        // the existing object
        $objects = [
            'nb' => 741,
            'list' => [
                Adherent::fromRow([
                    'id' => 3,
                    'lastname' => "nom1",
                    'firstname' => "prenom1"
                ]),
                Adherent::fromRow([
                    'id' => 5,
                    'lastname' => "nom2",
                    'firstname' => "prenom2"
                ]),
                Adherent::fromRow([
                    'id' => 8,
                    'lastname' => "nom3",
                    'firstname' => "prenom3"
                ]),
                Adherent::fromRow([
                    'id' => 13,
                    'lastname' => "nom4",
                    'firstname' => "prenom4"
                ]),
                Adherent::fromRow([
                    'id' => 21,
                    'lastname' => "nom5",
                    'firstname' => "prenom5"
                ]),
                Adherent::fromRow([
                    'id' => 44,
                    'lastname' => "nom6",
                    'firstname' => "prenom6"
                ]),
                Adherent::fromRow([
                    'id' => 65,
                    'lastname' => "nom7",
                    'firstname' => "prenom7"
                ])
            ]
        ];
        $this->mapper->expects($this->once())
            ->method('findAll')
            ->with($this->equalTo("nom"), $this->equalTo(1), $this->equalTo(25))
            ->will($this->returnValue($objects));
        
        $result = $this->service->findAllEx("nom", 1, 25);
        $this->assertEquals($objects, $result);
    }

    public function testFindAdherent()
    {
        // the existing object
        $objects = [
            Adherent::fromRow([
                'id' => 3,
                'lastname' => "nom1",
                'firstname' => "prenom1"
            ]),
            Adherent::fromRow([
                'id' => 5,
                'lastname' => "nom2",
                'firstname' => "prenom2"
            ])
        ];
        $this->mapper->expects($this->once())
            ->method('findAdherents')
            ->with($this->equalTo(12), $this->equalTo("2019-09-01"), $this->equalTo(1), $this->equalTo(25))
            ->will($this->returnValue($objects));
        $result = $this->service->findAdherents(12, "2019-09-01", 1, 25);
        $this->assertEquals($objects, $result);
    }

    public function testFindId()
    {
        $object = [
            'id' => 10,
            'num' => "CH0001245",
            'civility' => 2,
            'lastname' => "nom",
            'firstname' => "prenom",
            'birthdaydate' => "1991-11-02",
            'birthdayplace' => "lieu",
            'email' => "truc@machin.fr",
            'address' => "place centrale",
            'postalcode' => "12345",
            'city' => "ville",
            'country' => "CH",
            'phone1' => "654321147",
            'phone2' => "484128171",
            'comment' => "blabla",
            'photoid' => "",
            'uid' => 'nom.prenom'
        ];
        $this->mapper->expects($this->once())
            ->method('find')
            ->with($this->equalTo(10))
            ->will($this->returnValue($object));
        
        $subscriptions = [
            Subscription::fromRow([
                'id' => 3,
                'adherent' => 10,
                'typeadhesion' => 4,
                'structure' => 11,
                'dateref' => '2019-09-01',
                'comment' => 'foofoofoo'
            ]),
            Subscription::fromRow([
                'id' => 4,
                'adherent' => 10,
                'typeadhesion' => 5,
                'structure' => 7,
                'dateref' => '2018-09-01',
                'comment' => 'foofoofoo'
            ]),
            Subscription::fromRow([
                'id' => 8,
                'adherent' => 10,
                'typeadhesion' => 4,
                'structure' => 4,
                'dateref' => '2017-09-01',
                'comment' => 'foofoofoo'
            ])
        ];
        $this->subscription_mapper->expects($this->once())
            ->method('searchAdherent')
            ->with($this->equalTo(10))
            ->will($this->returnValue($subscriptions));
        
        $practices = [
            'nb' => 0,
            'list' => []
        ];
        $this->practice_mapper->expects($this->once())
            ->method('findAll')
            ->with($this->equalTo(3), $this->equalTo(0), $this->equalTo(1))
            ->will($this->returnValue($practices));
        
        $responsabilities = [
            'nb' => 55,
            'list' => [
                Responsability::fromRow([
                    'id' => 34,
                    'subscription' => 3,
                    'structure' => 11,
                    'typeresponsability' => 25,
                    'comment' => "resp. #2"
                ])
            ]
        ];
        $this->responsability_mapper->expects($this->once())
            ->method('findAll')
            ->with($this->equalTo(3), $this->equalTo(0), $this->equalTo(1), $this->equalTo(11))
            ->will($this->returnValue($responsabilities));
        
        $other = [
            'last_structure' => 11,
            'last_season' => '2019-09-01',
            'last_typeadhesion' => 4,
            'last_practice' => 0,
            'last_responsability' => 25,
            'list_year' => Array(
                '2019-09-01',
                '2018-09-01',
                '2017-09-01'
            )
        ];
        $expected_object = array_merge($object, $other);
        $expected_object['uid'] = null;
        $result = $this->service->findEx(10, 'nom.prenom');
        $this->assertEquals($expected_object, $result);
    }

    public function testFindUser()
    {
        $object = [
            'id' => 10,
            'num' => "CH0001245",
            'civility' => 2,
            'lastname' => "nom",
            'firstname' => "prenom",
            'birthdaydate' => "1991-11-02",
            'birthdayplace' => "lieu",
            'email' => "truc@machin.fr",
            'address' => "place centrale",
            'postalcode' => "12345",
            'city' => "ville",
            'country' => "CH",
            'phone1' => "654321147",
            'phone2' => "484128171",
            'comment' => "blabla",
            'photoid' => "",
            'uid' => 'nom.prenom'
        ];
        $this->mapper->expects($this->once())
            ->method('findCurrent')
            ->with($this->equalTo('nom.prenom'))
            ->will($this->returnValue($object));
        
        $subscriptions = [
            Subscription::fromRow([
                'id' => 3,
                'adherent' => 10,
                'typeadhesion' => 4,
                'structure' => 11,
                'dateref' => '2019-09-01',
                'comment' => 'foofoofoo'
            ]),
            Subscription::fromRow([
                'id' => 4,
                'adherent' => 10,
                'typeadhesion' => 5,
                'structure' => 7,
                'dateref' => '2018-09-01',
                'comment' => 'foofoofoo'
            ]),
            Subscription::fromRow([
                'id' => 8,
                'adherent' => 10,
                'typeadhesion' => 4,
                'structure' => 4,
                'dateref' => '2017-09-01',
                'comment' => 'foofoofoo'
            ])
        ];
        $this->subscription_mapper->expects($this->once())
            ->method('searchAdherent')
            ->with($this->equalTo(10))
            ->will($this->returnValue($subscriptions));
        
        $practices = [
            'nb' => 55,
            'list' => [
                Practice::fromRow([
                    'id' => 34,
                    'subscription' => 3,
                    'typepractice' => 11,
                    'comment' => "pract. #11"
                ])
            ]
        ];
        $this->practice_mapper->expects($this->once())
            ->method('findAll')
            ->with($this->equalTo(3), $this->equalTo(0), $this->equalTo(1))
            ->will($this->returnValue($practices));
        
        $responsabilities = [
            'nb' => 0,
            'list' => []
        ];
        $this->responsability_mapper->expects($this->once())
            ->method('findAll')
            ->with($this->equalTo(3), $this->equalTo(0), $this->equalTo(1), $this->equalTo(11))
            ->will($this->returnValue($responsabilities));
        
        $other = [
            'last_structure' => 11,
            'last_season' => '2019-09-01',
            'last_typeadhesion' => 4,
            'last_practice' => 11,
            'last_responsability' => 0,
            'list_year' => Array(
                '2019-09-01',
                '2018-09-01',
                '2017-09-01'
            )
        ];
        $expected_object = array_merge($object, $other);
        $expected_object['uid'] = null;
        $result = $this->service->findEx(- 1, 'nom.prenom');
        $this->assertEquals($expected_object, $result);
    }
}
