<?php
namespace OCA\CeranaAdhesion\Tests\Unit\Controller;

use PHPUnit\Framework\TestCase;
use OCP\AppFramework\Http;
use OCP\IRequest;
use OCA\CeranaAdhesion\Service\AdhesionObjectNotFound;
use OCA\CeranaAdhesion\Service\StructureService;
use OCA\CeranaAdhesion\Controller\StructureController;

class StructureControllerTest extends TestCase
{

    protected $controller;

    protected $service;

    protected $request;

    public function setUp(): void
    {
        $this->request = $this->getMockBuilder(IRequest::class)->getMock();
        $this->service = $this->getMockBuilder(StructureService::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->controller = new StructureController('cerana_adhesion', $this->request, $this->service, 'admin');
    }

    public function testUpdate()
    {
        $return = 'test update';
        $this->service->expects($this->once())
            ->method('update')
            ->with($this->equalTo(3), $this->equalTo("Truc de fou"), $this->equalTo("0987654321"), $this->equalTo("0123456789"), $this->equalTo("contact@truc.org"), $this->equalTo("http://truc.org"), $this->equalTo("bd machin"), $this->equalTo("98765"), $this->equalTo("ville"), $this->equalTo("foo foo foo"), $this->equalTo(3), $this->equalTo(2))
            ->will($this->returnValue($return));
        $result = $this->controller->update(3, "Truc de fou", "0987654321", "0123456789", "contact@truc.org", "http://truc.org", "bd machin", "98765", "ville", "foo foo foo", 3, 2);
        $this->assertEquals($return, $result->getData());
    }

    public function testCreate()
    {
        $return = 'test create';
        $this->service->expects($this->once())
            ->method('create')
            ->with($this->equalTo("Truc de fou"), $this->equalTo("0987654321"), $this->equalTo("0123456789"), $this->equalTo("contact@truc.org"), $this->equalTo("http://truc.org"), $this->equalTo("bd machin"), $this->equalTo("98765"), $this->equalTo("ville"), $this->equalTo("foo foo foo"), $this->equalTo(3), $this->equalTo(2))
            ->will($this->returnValue($return));
        $result = $this->controller->create("Truc de fou", "0987654321", "0123456789", "contact@truc.org", "http://truc.org", "bd machin", "98765", "ville", "foo foo foo", 3, 2);
        $this->assertEquals($return, $result->getData());
    }

    public function testDelete()
    {
        $return = 'test delete';
        $this->service->expects($this->once())
            ->method('delete')
            ->with($this->equalTo(15))
            ->will($this->returnValue($return));
        $result = $this->controller->destroy(15);
        $this->assertEquals($return, $result->getData());
    }

    public function testShow()
    {
        $return = 'test show';
        $this->service->expects($this->once())
            ->method('find')
            ->with($this->equalTo(68))
            ->will($this->returnValue($return));
        $result = $this->controller->show(68);
        $this->assertEquals($return, $result->getData());
    }

    public function testIndex()
    {
        $return = [
            'test index'
        ];
        $this->request->method('getParam')->will($this->onConsecutiveCalls(2, 50));
        $this->service->expects($this->once())
            ->method('findAll')
            ->with($this->equalTo(2), $this->equalTo(50))
            ->will($this->returnValue($return));
        $result = $this->controller->index();
        $this->assertEquals($return, $result->getData());
    }
}
