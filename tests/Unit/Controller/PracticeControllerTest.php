<?php
namespace OCA\CeranaAdhesion\Tests\Unit\Controller;

use PHPUnit\Framework\TestCase;
use OCP\AppFramework\Http;
use OCP\IRequest;
use OCA\CeranaAdhesion\Service\AdhesionObjectNotFound;
use OCA\CeranaAdhesion\Service\PracticeService;
use OCA\CeranaAdhesion\Controller\PracticeController;

class PracticeControllerTest extends TestCase
{

    protected $controller;

    protected $service;

    protected $request;

    public function setUp(): void
    {
        $this->request = $this->getMockBuilder(IRequest::class)->getMock();
        $this->service = $this->getMockBuilder(PracticeService::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->controller = new PracticeController('cerana_adhesion', $this->request, $this->service, 'admin');
    }

    public function testUpdate()
    {
        $return = 'test update';
        $this->service->expects($this->once())
            ->method('update')
            ->with($this->equalTo(3), $this->equalTo(42), $this->equalTo(18), $this->equalTo("comment"))
            ->will($this->returnValue($return));
        $result = $this->controller->update(3, 42, 18, "comment");
        $this->assertEquals($return, $result->getData());
    }

    public function testCreate()
    {
        $return = 'test create';
        $this->service->expects($this->once())
            ->method('create')
            ->with($this->equalTo(42), $this->equalTo(18), $this->equalTo("comment"))
            ->will($this->returnValue($return));
        $result = $this->controller->create(42, 18, "comment");
        $this->assertEquals($return, $result->getData());
    }

    public function testDelete()
    {
        $return = 'test delete';
        $this->service->expects($this->once())
            ->method('delete')
            ->with($this->equalTo(15))
            ->will($this->returnValue($return));
        $result = $this->controller->destroy(15);
        $this->assertEquals($return, $result->getData());
    }

    public function testShow()
    {
        $return = 'test show';
        $this->service->expects($this->once())
            ->method('find')
            ->with($this->equalTo(68))
            ->will($this->returnValue($return));
        $result = $this->controller->show(68);
        $this->assertEquals($return, $result->getData());
    }

    public function testIndex_sub()
    {
        $return = [
            'test index sub'
        ];
        $this->request->method('getParam')->will($this->onConsecutiveCalls(0, '', 1, 2, 50));
        $this->service->expects($this->once())
            ->method('findAllEx')
            ->with($this->equalTo(1), $this->equalTo(2), $this->equalTo(50))
            ->will($this->returnValue($return));
        $result = $this->controller->index();
        $this->assertEquals($return, $result->getData());
    }

    public function testIndex_struct()
    {
        $return = [
            'test index struct'
        ];
        $this->request->method('getParam')->will($this->onConsecutiveCalls(4, '2019-09-01', 8, 2, 50));
        $this->service->expects($this->once())
            ->method('findAdherents')
            ->with($this->equalTo(4), $this->equalTo('2019-09-01'), $this->equalTo(8), $this->equalTo(2), $this->equalTo(50))
            ->will($this->returnValue($return));
        $result = $this->controller->index();
        $this->assertEquals($return, $result->getData());
    }
}
