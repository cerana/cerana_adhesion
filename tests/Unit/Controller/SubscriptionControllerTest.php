<?php
namespace OCA\CeranaAdhesion\Tests\Unit\Controller;

use PHPUnit\Framework\TestCase;
use OCP\AppFramework\Http;
use OCP\IRequest;
use OCA\CeranaAdhesion\Service\AdhesionObjectNotFound;
use OCA\CeranaAdhesion\Service\SubscriptionService;
use OCA\CeranaAdhesion\Controller\SubscriptionController;

class SubscriptionControllerTest extends TestCase
{

    protected $controller;

    protected $service;

    protected $request;

    public function setUp(): void
    {
        $this->request = $this->getMockBuilder(IRequest::class)->getMock();
        $this->service = $this->getMockBuilder(SubscriptionService::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->controller = new SubscriptionController('cerana_adhesion', $this->request, $this->service, 'admin');
    }

    public function testUpdate()
    {
        $return = 'test update';
        $this->service->expects($this->once())
            ->method('update')
            ->with($this->equalTo(3), $this->equalTo(1), $this->equalTo(7), $this->equalTo(5), $this->equalTo("2018/09/01"), $this->equalTo("commentaires"))
            ->will($this->returnValue($return));
        $result = $this->controller->update(3, 1, 7, 5, "2018/09/01", "commentaires");
        $this->assertEquals($return, $result->getData());
    }

    public function testCreate()
    {
        $return = 'test create';
        $this->service->expects($this->once())
            ->method('create')
            ->with($this->equalTo(1), $this->equalTo(7), $this->equalTo(5), $this->equalTo("2018/09/01"), $this->equalTo("commentaires"))
            ->will($this->returnValue($return));
        $result = $this->controller->create(1, 7, 5, "2018/09/01", "commentaires");
        $this->assertEquals($return, $result->getData());
    }

    public function testDelete()
    {
        $return = 'test delete';
        $this->service->expects($this->once())
            ->method('delete')
            ->with($this->equalTo(15))
            ->will($this->returnValue($return));
        $result = $this->controller->destroy(15);
        $this->assertEquals($return, $result->getData());
    }

    public function testShow()
    {
        $return = 'test show';
        $this->service->expects($this->once())
            ->method('find')
            ->with($this->equalTo(68))
            ->will($this->returnValue($return));
        $result = $this->controller->show(68);
        $this->assertEquals($return, $result->getData());
    }

    public function testIndex()
    {
        $return = [
            'test index'
        ];
        $this->request->method('getParam')->will($this->onConsecutiveCalls(2, 50));
        $this->service->expects($this->once())
            ->method('findAll')
            ->with($this->equalTo(2), $this->equalTo(50))
            ->will($this->returnValue($return));
        $result = $this->controller->index();
        $this->assertEquals($return, $result->getData());
    }
}
