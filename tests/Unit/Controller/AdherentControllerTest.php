<?php
namespace OCA\CeranaAdhesion\Tests\Unit\Controller;

use PHPUnit\Framework\TestCase;
use OCP\AppFramework\Http;
use OCP\IRequest;
use OCA\CeranaAdhesion\Service\AdhesionObjectNotFound;
use OCA\CeranaAdhesion\Service\AdherentService;
use OCA\CeranaAdhesion\Controller\AdherentController;

class AdherentControllerTest extends TestCase
{

    protected $controller;

    protected $service;

    protected $request;

    public function setUp(): void
    {
        $this->request = $this->getMockBuilder(IRequest::class)->getMock();
        $this->service = $this->getMockBuilder(AdherentService::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->controller = new AdherentController('cerana_adhesion', $this->request, $this->service, 'admin');
    }

    public function testUpdate()
    {
        $return = 'test update';
        $this->service->expects($this->once())
            ->method('update')
            ->with($this->equalTo(3), $this->equalTo(1), $this->equalTo("nom"), $this->equalTo("prenom"), $this->equalTo("2001-03-24"), $this->equalTo("lieuNaissance"), $this->equalTo("email"), $this->equalTo("adresse"), $this->equalTo("codePostal"), $this->equalTo("ville"), $this->equalTo("FR"), $this->equalTo("tel1"), $this->equalTo("tel2"), $this->equalTo("commentaires"), $this->equalTo("photoId"))
            ->will($this->returnValue($return));
        $result = $this->controller->update(3, 1, "nom", "prenom", "2001-03-24", "lieuNaissance", "email", "adresse", "codePostal", "ville", "FR", "tel1", "tel2", "commentaires", "photoId");
        $this->assertEquals($return, $result->getData());
    }

    public function testUpdateNotFound()
    {
        // test the correct status code if no note is found
        $this->service->expects($this->once())
            ->method('update')
            ->will($this->throwException(new AdhesionObjectNotFound()));
        
        $result = $this->controller->update(3, 1, "nom", "prenom", "2001-03-24", "lieuNaissance", "email", "adresse", "codePostal", "ville", "FR", "tel1", "tel2", "commentaires", "photoId");
        
        $this->assertEquals(Http::STATUS_NOT_FOUND, $result->getStatus());
    }

    public function testCreate()
    {
        $return = 'test create';
        $this->service->expects($this->once())
            ->method('create')
            ->with($this->equalTo(1), $this->equalTo("nom"), $this->equalTo("prenom"), $this->equalTo("2001-03-24"), $this->equalTo("lieuNaissance"), $this->equalTo("email"), $this->equalTo("adresse"), $this->equalTo("codePostal"), $this->equalTo("ville"), $this->equalTo("FR"), $this->equalTo("tel1"), $this->equalTo("tel2"), $this->equalTo("commentaires"), $this->equalTo("photoId"))
            ->will($this->returnValue($return));
        $result = $this->controller->create(1, "nom", "prenom", "2001-03-24", "lieuNaissance", "email", "adresse", "codePostal", "ville", "FR", "tel1", "tel2", "commentaires", "photoId");
        $this->assertEquals($return, $result->getData());
    }

    public function testDelete()
    {
        $return = 'test delete';
        $this->service->expects($this->once())
            ->method('delete')
            ->with($this->equalTo(15))
            ->will($this->returnValue($return));
        $result = $this->controller->destroy(15);
        $this->assertEquals($return, $result->getData());
    }

    public function testShow_id()
    {
        $return = 'test show';
        $this->service->expects($this->once())
            ->method('findEx')
            ->with($this->equalTo(68), $this->equalTo('admin'))
            ->will($this->returnValue($return));
        $result = $this->controller->show(68);
        $this->assertEquals($return, $result->getData());
    }

    public function testShow_user()
    {
        $return = 'test show';
        $this->service->expects($this->once())
            ->method('findEx')
            ->with($this->equalTo(- 1), $this->equalTo('admin'))
            ->will($this->returnValue($return));
        $result = $this->controller->show(- 1);
        $this->assertEquals($return, $result->getData());
    }

    public function testIndex_search()
    {
        $return = [
            'test index search'
        ];
        $this->request->method('getParam')->will($this->onConsecutiveCalls(0, '', 'criteria', 2, 50));
        $this->service->expects($this->once())
            ->method('findAllEx')
            ->with($this->equalTo('criteria'), $this->equalTo(2), $this->equalTo(50))
            ->will($this->returnValue($return));
        $result = $this->controller->index();
        $this->assertEquals($return, $result->getData());
    }

    public function testIndex_struct()
    {
        $return = [
            'test index search'
        ];
        $this->request->method('getParam')->will($this->onConsecutiveCalls(4, '2019-09-01', 2, 50));
        $this->service->expects($this->once())
            ->method('findAdherents')
            ->with($this->equalTo(4), $this->equalTo('2019-09-01'), $this->equalTo(2), $this->equalTo(50))
            ->will($this->returnValue($return));
        $result = $this->controller->index();
        $this->assertEquals($return, $result->getData());
    }
}
