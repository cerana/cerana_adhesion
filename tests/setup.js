import Vue from 'vue'
import 'babel-polyfill'

Vue.config.productionTip = false
Vue.prototype.t = window.t
Vue.prototype.n = window.n
Vue.prototype.OC = window.OC
Vue.prototype.OCA = window.OCA
