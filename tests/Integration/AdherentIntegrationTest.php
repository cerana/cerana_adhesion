<?php

namespace OCA\CeranaAdhesion\Tests\Integration\Controller;

use OCP\AppFramework\App;
use OCP\IRequest;
use PHPUnit\Framework\TestCase;


use OCA\CeranaAdhesion\Db\Adherent;
use OCA\CeranaAdhesion\Db\AdherentMapper;
use OCA\CeranaAdhesion\Controller\AdherentController;

class AdherentIntegrationTest extends TestCase {
	private $controller;
	private $mapper;
	private $userId = 'john';

	public function setUp(): void {
		$app = new App('cerana_adhesion');
		$container = $app->getContainer();

		// only replace the user id
		$container->registerService('userId', function () {
			return $this->userId;
		});

		// we do not care about the request but the controller needs it
		$container->registerService(IRequest::class, function () {
			return $this->createMock(IRequest::class);
		});

		$this->controller = $container->query(AdherentController::class);
		$this->mapper = $container->query(AdherentMapper::class);
	}

	public function testUpdate() {
		// create a new adherent that should be updated
		$adherent = new Adherent();
		$adherent->setNum('CH0000001');
		$adherent->setCivility(2);
		$adherent->setLastname("nom");
		$adherent->setFirstname("prenom");
		$adherent->setBirthdaydate("1991-11-02");
		$adherent->setBirthdayplace("lieu");
		$adherent->setEmail("truc@machin.fr");
		$adherent->setAddress("place centrale");
		$adherent->setPostalcode("12345");
		$adherent->setCity("ville");
		$adherent->setCountry("CH");
		$adherent->setPhone1("654321147");
		$adherent->setPhone2("484128171");
		$adherent->setComment("blabla");
		$adherent->setPhotoid("qldflqdmfkmlqdfsù.jpg");
		$adherent->setuid(null);
		$id = $this->mapper->insert($adherent)->getId();

		// fromRow does not set the fields as updated
		$updatedAdherent = Adherent::fromRow([
			'id' => $id,
		    'num' => 'CH0000001',
		]);
		$updatedAdherent->setCivility(1);
		$updatedAdherent->setLastname("Dupond");
		$updatedAdherent->setFirstname("Jean");
		$updatedAdherent->setBirthdaydate("1990-10-14");
		$updatedAdherent->setBirthdayplace("ici");
		$updatedAdherent->setEmail("bidule@truc.com");
		$updatedAdherent->setAddress("rue de la Paix");
		$updatedAdherent->setPostalcode("98765");
		$updatedAdherent->setCity("Town");
		$updatedAdherent->setCountry("BE");
		$updatedAdherent->setPhone1("0321654987");
		$updatedAdherent->setPhone2("0654987321");
		$updatedAdherent->setComment("foofoofoo");
		$updatedAdherent->setPhotoid("gfjhsdfkjgkdskl.gif");
		$result = $this->controller->update($id, 1, "Dupond", "Jean", "1990-10-14", "ici", "bidule@truc.com", "rue de la Paix", "98765", "Town", "BE", "0321654987", "0654987321", "foofoofoo", "gfjhsdfkjgkdskl.gif");

		$this->assertEquals($updatedAdherent, $result->getData());

		// clean up
		$this->mapper->delete($result->getData());
	}
}
