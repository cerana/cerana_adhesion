'use strict'

import { nextTick } from 'vue'
import { shallowMount } from '@vue/test-utils'
import { setSelectValue, initMockAxios } from './functions.js'
import { ceranaI18n, ceranaStore } from '../../src/cerana_lib.js'
import axios from 'axios'
import MenuReport from '../../src/MenuReport'

jest.mock('axios')

beforeEach(() => {
	document.documentElement.innerHTML = '<html><body></body></html>'
	global.console.error = jest.fn()
	global.console.warn = jest.fn()
})

describe('Menu Report', () => {
	it('Show default', async () => {
		ceranaStore.commit('clean')
		ceranaStore.commit('defaultSeason', '2020-09-01')
		initMockAxios()
		const wrapper = shallowMount(MenuReport, {
			ceranaI18n,
			ceranaStore,
			propsData: {},
		})
		while (wrapper.vm.loading) {
			await nextTick()
		}
		await nextTick()
		await nextTick()
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(6)
		expect(axios.get).toHaveBeenNthCalledWith(6, '/apps/cerana_adhesion/statistics?structure=0&date=2020-09-01')
		expect(wrapper.find('#report > h1').text()).toBe('Statistiques')
		expect(wrapper.findAll('div > div > div.selection > label').length).toBe(2)
		expect(wrapper.find('div > div > div.selection > label:nth-child(1)').text()).toBe('Structure')
		expect(wrapper.findAll('#currentstructureid > option').length).toBe(13)
		expect(wrapper.find('div > div > div.selection > label:nth-child(3)').text()).toBe('Saison')
		expect(wrapper.findAll('#currentseason > option').length).toBe(23)
		expect(wrapper.find('div > div > div.show > div.table > fieldset:nth-child(1) > legend').text()).toBe('Cotisations')
		expect(wrapper.find('div > div > div.show > div.table > fieldset:nth-child(1) > complistgeneric-stub').props().objects.length).toBe(4)
		expect(wrapper.find('div > div > div.show > div.table > fieldset:nth-child(1) > complistgeneric-stub').props().showoption).toBe('0')
		expect(wrapper.find('div > div > div.show > div.table > fieldset:nth-child(1) > complistgeneric-stub').props().addoption).toBe('0')
		expect(wrapper.find('div > div > div.show > div.table > fieldset:nth-child(1) > complistgeneric-stub').props().editoption).toBe('0')
		expect(wrapper.find('div > div > div.show > div.table > fieldset:nth-child(1) > complistgeneric-stub').props().deleteoption).toBe('0')
		expect(wrapper.find('div > div > div.show > div.table > fieldset:nth-child(2) > legend').text()).toBe('Pratiques')
		expect(wrapper.find('div > div > div.show > div.table > fieldset:nth-child(2) > complistgeneric-stub').props().objects.length).toBe(5)
		expect(wrapper.find('div > div > div.show > div.table > fieldset:nth-child(2) > complistgeneric-stub').props().showoption).toBe('0')
		expect(wrapper.find('div > div > div.show > div.table > fieldset:nth-child(2) > complistgeneric-stub').props().addoption).toBe('0')
		expect(wrapper.find('div > div > div.show > div.table > fieldset:nth-child(2) > complistgeneric-stub').props().editoption).toBe('0')
		expect(wrapper.find('div > div > div.show > div.table > fieldset:nth-child(2) > complistgeneric-stub').props().deleteoption).toBe('0')
		expect(wrapper.find('div > div > div.show > div.table > fieldset:nth-child(3) > legend').text()).toBe('Responsabilités')
		expect(wrapper.find('div > div > div.show > div.table > fieldset:nth-child(3) > complistgeneric-stub').props().objects.length).toBe(18)
		expect(wrapper.find('div > div > div.show > div.table > fieldset:nth-child(3) > complistgeneric-stub').props().showoption).toBe('0')
		expect(wrapper.find('div > div > div.show > div.table > fieldset:nth-child(3) > complistgeneric-stub').props().addoption).toBe('0')
		expect(wrapper.find('div > div > div.show > div.table > fieldset:nth-child(3) > complistgeneric-stub').props().editoption).toBe('0')
		expect(wrapper.find('div > div > div.show > div.table > fieldset:nth-child(3) > complistgeneric-stub').props().deleteoption).toBe('0')
	})

	it('Select structure', async () => {
		ceranaStore.commit('clean')
		ceranaStore.commit('defaultSeason', '2020-09-01')
		initMockAxios()
		const wrapper = shallowMount(MenuReport, {
			ceranaI18n,
			ceranaStore,
			propsData: {},
		})
		while (wrapper.vm.loading) {
			await nextTick()
		}
		await nextTick()
		expect(axios.get).toHaveBeenNthCalledWith(6, '/apps/cerana_adhesion/statistics?structure=0&date=2020-09-01')
		setSelectValue(wrapper, '#currentstructureid', '10')
		await nextTick()
		while (wrapper.vm.loading) {
			await nextTick()
		}
		await nextTick()
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(7)
		expect(axios.get).toHaveBeenNthCalledWith(7, '/apps/cerana_adhesion/statistics?structure=10&date=2020-09-01')
	})

	it('Select season', async () => {
		ceranaStore.commit('clean')
		ceranaStore.commit('defaultSeason', '2020-09-01')
		initMockAxios()
		const wrapper = shallowMount(MenuReport, {
			ceranaI18n,
			ceranaStore,
			propsData: {},
		})
		while (wrapper.vm.loading) {
			await nextTick()
		}
		await nextTick()
		expect(axios.get).toHaveBeenNthCalledWith(6, '/apps/cerana_adhesion/statistics?structure=0&date=2020-09-01')
		setSelectValue(wrapper, '#currentseason', '2012-09-01')
		await nextTick()
		while (wrapper.vm.loading) {
			await nextTick()
		}
		await nextTick()
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(7)
		expect(axios.get).toHaveBeenNthCalledWith(7, '/apps/cerana_adhesion/statistics?structure=0&date=2012-09-01')
	})
})
