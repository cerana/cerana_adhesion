'use strict'

import { nextTick } from 'vue'
import { shallowMount } from '@vue/test-utils'
import { initMockAxios } from './functions.js'
import { ceranaI18n, ceranaStore } from '../../src/cerana_lib.js'
import axios from 'axios'
import CompMembers from '../../src/CompMembers'

jest.mock('axios')

beforeEach(() => {
	document.documentElement.innerHTML = '<html><body></body></html>'
	global.console.error = jest.fn()
	global.console.warn = jest.fn()
})

describe('Comp Members', () => {
	it('Show default', async () => {
		ceranaStore.commit('clean')
		ceranaStore.commit('defaultSeason', '2019-09-01')
		initMockAxios()
		const wrapper = shallowMount(CompMembers, {
			ceranaI18n,
			ceranaStore,
			propsData: {},
		})
		await nextTick()
		while (wrapper.vm.loading) {
			await nextTick()
		}
		await nextTick()
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(5)
		expect(console.error).toHaveBeenCalledTimes(0)
		expect(wrapper.find('#members > div > div.selection > label').text()).toBe('Saison')
		expect(wrapper.findAll('#currentseason > option').length).toBe(23)
		expect(wrapper.find('#members > div > div.list > input').element.value).toBe('Nouveau')
		expect(wrapper.find('#members > compeditgeneric-stub').exists()).toBe(false)
		expect(wrapper.vm.currentseason).toStrictEqual('2019-09-01')
		expect(wrapper.vm.notEmpty()).toStrictEqual(true)
	})

	it('Show structure', async () => {
		ceranaStore.commit('clean')
		ceranaStore.commit('defaultSeason', '2019-09-01')
		initMockAxios()
		const wrapper = shallowMount(CompMembers, {
			ceranaI18n,
			ceranaStore,
			propsData: {
				structureid: 777,
			},
		})
		await nextTick()
		while (wrapper.vm.loading) {
			await nextTick()
		}
		await nextTick()
		expect(wrapper.find('#members > div > div.selection > label').text()).toBe('Saison')
		expect(wrapper.findAll('#currentseason > option').length).toBe(23)
		expect(wrapper.find('#members > div > div.list > input').element.value).toBe('Nouveau')
		expect(wrapper.findAll('#members > div > tabs-stub > tab-stub').length).toBe(3)
		expect(wrapper.find('#members > div > tabs-stub > tab-stub:nth-child(1) > div > label').text()).toBe('Filtre par pratique')
		expect(wrapper.findAll('#members > div > tabs-stub > tab-stub:nth-child(1) > div > select > option').length).toBe(6)
		expect(wrapper.find('#members > div > tabs-stub > tab-stub:nth-child(1) > complistgeneric-stub').props().models).toBe('practices')
		expect(wrapper.find('#members > div > tabs-stub > tab-stub:nth-child(1) > complistgeneric-stub').props().fields.length).toBe(8)
		expect(wrapper.find('#members > div > tabs-stub > tab-stub:nth-child(1) > complistgeneric-stub').props().searching).toStrictEqual({ structure: 777, date: '2019-09-01', typepractice: 0 })
		expect(wrapper.find('#members > div > tabs-stub > tab-stub:nth-child(1) > complistgeneric-stub').props().showoption).toBe('2')
		expect(wrapper.find('#members > div > tabs-stub > tab-stub:nth-child(1) > complistgeneric-stub').props().addoption).toBe('0')
		expect(wrapper.find('#members > div > tabs-stub > tab-stub:nth-child(1) > complistgeneric-stub').props().editoption).toBe('2')
		expect(wrapper.find('#members > div > tabs-stub > tab-stub:nth-child(1) > complistgeneric-stub').props().deleteoption).toBe('0')

		expect(wrapper.find('#members > div > tabs-stub > tab-stub:nth-child(2) > complistgeneric-stub').props().models).toBe('responsabilitys')
		expect(wrapper.find('#members > div > tabs-stub > tab-stub:nth-child(2) > complistgeneric-stub').props().fields.length).toBe(8)
		expect(wrapper.find('#members > div > tabs-stub > tab-stub:nth-child(2) > complistgeneric-stub').props().searching).toStrictEqual({ structure: 777, date: '2019-09-01', typepractice: 0 })
		expect(wrapper.find('#members > div > tabs-stub > tab-stub:nth-child(2) > complistgeneric-stub').props().showoption).toBe('2')
		expect(wrapper.find('#members > div > tabs-stub > tab-stub:nth-child(2) > complistgeneric-stub').props().addoption).toBe('0')
		expect(wrapper.find('#members > div > tabs-stub > tab-stub:nth-child(2) > complistgeneric-stub').props().editoption).toBe('2')
		expect(wrapper.find('#members > div > tabs-stub > tab-stub:nth-child(2) > complistgeneric-stub').props().deleteoption).toBe('0')

		expect(wrapper.find('#members > div > tabs-stub > tab-stub:nth-child(3) > complistgeneric-stub').props().models).toBe('adherents')
		expect(wrapper.find('#members > div > tabs-stub > tab-stub:nth-child(3) > complistgeneric-stub').props().fields.length).toBe(20)
		expect(wrapper.find('#members > div > tabs-stub > tab-stub:nth-child(3) > complistgeneric-stub').props().searching).toStrictEqual({ structure: 777, date: '2019-09-01', typepractice: 0 })
		expect(wrapper.find('#members > div > tabs-stub > tab-stub:nth-child(3) > complistgeneric-stub').props().showoption).toBe('2')
		expect(wrapper.find('#members > div > tabs-stub > tab-stub:nth-child(3) > complistgeneric-stub').props().addoption).toBe('0')
		expect(wrapper.find('#members > div > tabs-stub > tab-stub:nth-child(3) > complistgeneric-stub').props().editoption).toBe('2')
		expect(wrapper.find('#members > div > tabs-stub > tab-stub:nth-child(3) > complistgeneric-stub').props().deleteoption).toBe('0')
		expect(wrapper.find('#members > compeditgeneric-stub').exists()).toBe(false)

		wrapper.vm.tabchange({ tab: { id: 'practice' } })
		await nextTick()
		wrapper.vm.tabchange({ tab: { id: 'responsability' } })
		await nextTick()
		wrapper.vm.tabchange({ tab: { id: 'associate' } })
		await nextTick()
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(5)
		expect(console.error).toHaveBeenCalledTimes(0)
	})

	it('Show adherent', async () => {
		ceranaStore.commit('clean')
		ceranaStore.commit('defaultSeason', '2019-09-01')
		initMockAxios()
		const wrapper = shallowMount(CompMembers, {
			ceranaI18n,
			ceranaStore,
			propsData: {
				structureid: 777,
			},
		})
		await nextTick()
		while (wrapper.vm.loading) {
			await nextTick()
		}
		await nextTick()
		await wrapper.vm.showadherent({ id: 159 })
		await nextTick()
		expect(wrapper.find('#members > div > input').element.value).toBe('Retour')
		expect(wrapper.find('#members > div > h1').text()).toBe('Fiche d\'adhérent')
		expect(wrapper.find('#members > div > compadherent-stub').props().initadherentid).toBe(159)
		expect(wrapper.find('#members > compeditgeneric-stub').exists()).toBe(false)
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(5)
		expect(console.error).toHaveBeenCalledTimes(0)
	})

	it('Edit adherent', async () => {
		ceranaStore.commit('clean')
		ceranaStore.commit('defaultSeason', '2019-09-01')
		initMockAxios()
		const wrapper = shallowMount(CompMembers, {
			ceranaI18n,
			ceranaStore,
			propsData: {
				structureid: 777,
			},
		})
		await nextTick()
		while (wrapper.vm.loading) {
			await nextTick()
		}
		await nextTick()
		await wrapper.vm.editadherent({ adherentid: 159 })
		await nextTick()
		expect(wrapper.find('#members > div > div.selection > label').text()).toBe('Saison')
		expect(wrapper.findAll('#currentseason > option').length).toBe(23)
		expect(wrapper.find('#members > div > div.list > input').element.value).toBe('Nouveau')
		expect(wrapper.find('#members > compeditgeneric-stub').exists()).toBe(true)
		expect(wrapper.find('#members > compeditgeneric-stub').props().itemid).toBe(159)
		expect(wrapper.find('#members > compeditgeneric-stub').props().models).toBe('adherents')
		expect(wrapper.find('#members > compeditgeneric-stub').props().fields.length).toBe(20)
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(5)
		expect(console.error).toHaveBeenCalledTimes(0)
	})

	it('Add adherent', async () => {
		ceranaStore.commit('clean')
		ceranaStore.commit('defaultSeason', '2019-09-01')
		initMockAxios()
		const wrapper = shallowMount(CompMembers, {
			ceranaI18n,
			ceranaStore,
			propsData: {
				structureid: 777,
			},
		})
		await nextTick()
		while (wrapper.vm.loading) {
			await nextTick()
		}
		await nextTick()

		await wrapper.vm.editadherent()
		await nextTick()
		expect(wrapper.find('#members > div > div.selection > label').text()).toBe('Saison')
		expect(wrapper.findAll('#currentseason > option').length).toBe(23)
		expect(wrapper.find('#members > div > div.list > input').element.value).toBe('Nouveau')
		expect(wrapper.find('#members > compeditgeneric-stub').exists()).toBe(true)
		expect(wrapper.find('#members > compeditgeneric-stub').props().itemid).toBe(-1)
		expect(wrapper.find('#members > compeditgeneric-stub').props().models).toBe('adherents')
		expect(wrapper.find('#members > compeditgeneric-stub').props().fields.length).toBe(20)

		await wrapper.vm.refreshEdit({ id: 444 })
		while (wrapper.vm.loading) {
			await nextTick()
		}
		await nextTick()
		expect(wrapper.find('#members > div > input').element.value).toBe('Retour')
		expect(wrapper.find('#members > div > h1').text()).toBe('Fiche d\'adhérent')
		expect(wrapper.find('#members > div > compadherent-stub').props().initadherentid).toBe(444)
		expect(wrapper.find('#members > compeditgeneric-stub').exists()).toBe(false)

		await wrapper.vm.refresh()
		await nextTick()
		expect(wrapper.find('#members > div > div.selection > label').text()).toBe('Saison')
		expect(wrapper.findAll('#currentseason > option').length).toBe(23)
		expect(wrapper.find('#members > div > div.list > input').element.value).toBe('Nouveau')
		expect(wrapper.findAll('#members > div > tabs-stub > tab-stub').length).toBe(3)
		expect(wrapper.find('#members > compeditgeneric-stub').exists()).toBe(false)

		expect(axios.post).toHaveBeenCalledTimes(1)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(5)
		expect(axios.post).toHaveBeenCalledWith('/apps/cerana_adhesion/subscriptions', { adherent: 444, dateref: '2019-09-01', structure: 777, typeadhesion: 1, comment: '' })
		expect(console.error).toHaveBeenCalledTimes(0)
	})

})
