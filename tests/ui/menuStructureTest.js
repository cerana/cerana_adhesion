'use strict'

import { nextTick } from 'vue'
import { shallowMount } from '@vue/test-utils'
import { setSelectValue, initMockAxios } from './functions.js'
import axios from 'axios'
import MenuStructure from '../../src/MenuStructure'
import { ceranaI18n, ceranaStore } from '../../src/cerana_lib.js'

jest.mock('axios')

beforeEach(() => {
	document.documentElement.innerHTML = '<html><body></body></html>'
	global.console.error = jest.fn()
	global.console.warn = jest.fn()
})

describe('Menu Structure', () => {
	it('Show no default', async () => {
		ceranaStore.commit('clean')
		initMockAxios()
		const wrapper = shallowMount(MenuStructure, {
			ceranaI18n,
			ceranaStore,
			propsData: {},
		})
		while (wrapper.vm.loading) {
			await nextTick()
		}
		await nextTick()
		expect(wrapper.find('div > h1').text()).toBe('Les structures')
		expect(wrapper.find('div > div > #structure-selection > label').text()).toBe('Structure')
		expect(wrapper.findAll('div > div > #structure-selection > select > option').length).toBe(12)
		expect(wrapper.find('div > div > #structure-selection > input').element.value).toBe('Nouveau')
		expect(wrapper.find('div > div > #emptycontent > h2').text()).toBe('Aucune structure !')
		expect(wrapper.find('div > div > div.show').exists()).toBe(false)
		expect(wrapper.find('div > div > CompConfirm-Stub').exists()).toBe(false)
		expect(wrapper.find('div > div > CompEditGeneric-Stub').exists()).toBe(false)
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(5)
	})

	it('Show default', async () => {
		ceranaStore.commit('clean')
		ceranaStore.commit('defaultStructure', 3)
		initMockAxios()
		const wrapper = shallowMount(MenuStructure, {
			ceranaI18n,
			ceranaStore,
			propsData: {},
		})
		while (wrapper.vm.loading) {
			await nextTick()
		}
		await nextTick()
		expect(wrapper.find('div > div > #emptycontent').exists()).toBe(false)
		expect(wrapper.find('div > div > div.show').exists()).toBe(true)
		expect(wrapper.find('div > div > div.show > div.table > fieldset:nth-child(1) > legend').text()).toBe('Identité')
		expect(wrapper.find('div > div > div.show > div.table > fieldset:nth-child(1) > compshowgeneric-stub').props().models).toBe('structures')
		expect(wrapper.find('div > div > div.show > div.table > fieldset:nth-child(1) > compshowgeneric-stub').props().itemid).toBe(3)
		expect(wrapper.find('div > div > div.show > div.table > fieldset:nth-child(1) > compshowgeneric-stub').props().fields.length).toBe(11)
		expect(wrapper.find('div > div > div.show > div.table > fieldset:nth-child(1) > div.cerana-button > input:nth-child(1)').element.value).toBe('Éditer')
		expect(wrapper.find('div > div > div.show > div.table > fieldset:nth-child(1) > div.cerana-button > input:nth-child(2)').element.value).toBe('Supprimer')
		expect(wrapper.find('div > div > div.show > div.table > fieldset:nth-child(2) > legend').text()).toBe('Cotisations')
		expect(wrapper.find('div > div > div.show > div.table > fieldset:nth-child(2) > compmembers-stub').props().structureid).toBe(3)
		expect(wrapper.find('div > div > CompConfirm-Stub').exists()).toBe(false)
		expect(wrapper.find('div > div > CompEditGeneric-Stub').exists()).toBe(false)
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(5)
	})

	it('Show select', async () => {
		ceranaStore.commit('clean')
		initMockAxios()
		const wrapper = shallowMount(MenuStructure, {
			ceranaI18n,
			ceranaStore,
			propsData: {},
		})
		while (wrapper.vm.loading) {
			await nextTick()
		}
		await nextTick()
		expect(wrapper.findAll('div > div > #structure-selection > select > option').length).toBe(12)
		expect(wrapper.find('div > div > #emptycontent > h2').text()).toBe('Aucune structure !')
		setSelectValue(wrapper, 'div > div > #structure-selection > select', '7')
		await nextTick()
		while (wrapper.vm.loading) {
			await nextTick()
		}
		await nextTick()
		expect(wrapper.find('div > div > #emptycontent').exists()).toBe(false)
		expect(wrapper.find('div > div > div.show').exists()).toBe(true)
		expect(wrapper.find('div > div > div.show > div.table > fieldset:nth-child(1) > compshowgeneric-stub').props().itemid).toBe(7)
		expect(wrapper.find('div > div > div.show > div.table > fieldset:nth-child(2) > compmembers-stub').props().structureid).toBe(7)
		expect(wrapper.find('div > div > CompConfirm-Stub').exists()).toBe(false)
		expect(wrapper.find('div > div > CompEditGeneric-Stub').exists()).toBe(false)
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(5)
	})

	it('Edit default', async () => {
		ceranaStore.commit('clean')
		ceranaStore.commit('defaultStructure', 3)
		initMockAxios()
		const wrapper = shallowMount(MenuStructure, {
			ceranaI18n,
			ceranaStore,
			propsData: {},
		})
		while (wrapper.vm.loading) {
			await nextTick()
		}
		await nextTick()
		expect(wrapper.find('div > div > #emptycontent').exists()).toBe(false)
		expect(wrapper.find('div > div > div.show').exists()).toBe(true)
		expect(wrapper.find('div > div > div.show > div.table > fieldset:nth-child(1) > compshowgeneric-stub').props().itemid).toBe(3)
		expect(wrapper.find('div > div > div.show > div.table > fieldset:nth-child(2) > compmembers-stub').props().structureid).toBe(3)
		expect(wrapper.find('div > div > div.show > div.table > fieldset:nth-child(1) > div.cerana-button > input:nth-child(1)').element.value).toBe('Éditer')
		expect(wrapper.find('div > div > div.show > div.table > fieldset:nth-child(1) > div.cerana-button > input:nth-child(2)').element.value).toBe('Supprimer')
		expect(wrapper.find('div > div > CompConfirm-Stub').exists()).toBe(false)
		expect(wrapper.find('div > div > CompEditGeneric-Stub').exists()).toBe(false)
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(5)

		await wrapper.find('div > div > div.show > div.table > fieldset:nth-child(1) > div.cerana-button > input:nth-child(1)').trigger('click')
		while (wrapper.vm.loading) {
			await nextTick()
		}
		await nextTick()
		expect(wrapper.find('div > div > #emptycontent').exists()).toBe(false)
		expect(wrapper.find('div > div > div.show').exists()).toBe(true)
		expect(wrapper.find('div > div > CompConfirm-Stub').exists()).toBe(false)
		expect(wrapper.find('div > div > CompEditGeneric-Stub').exists()).toBe(true)
		expect(wrapper.find('div > div > CompEditGeneric-Stub').props().itemid).toBe(3)
		expect(wrapper.find('div > div > CompEditGeneric-Stub').props().models).toBe('structures')
		expect(wrapper.find('div > div > CompEditGeneric-Stub').props().fields.length).toBe(11)
		await wrapper.vm.updated(true)
		while (wrapper.vm.loading) {
			await nextTick()
		}
		await nextTick()
		expect(wrapper.find('div > div > #emptycontent').exists()).toBe(false)
		expect(wrapper.find('div > div > div.show').exists()).toBe(true)
		expect(wrapper.find('div > div > CompConfirm-Stub').exists()).toBe(false)
		expect(wrapper.find('div > div > CompEditGeneric-Stub').exists()).toBe(false)
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(6)
	})

	it('New', async () => {
		ceranaStore.commit('clean')
		initMockAxios()
		const wrapper = shallowMount(MenuStructure, {
			ceranaI18n,
			ceranaStore,
			propsData: {},
		})
		while (wrapper.vm.loading) {
			await nextTick()
		}
		await nextTick()
		expect(wrapper.find('div > div > #structure-selection > input').element.value).toBe('Nouveau')
		expect(wrapper.find('div > div > #emptycontent > h2').text()).toBe('Aucune structure !')
		expect(wrapper.find('div > div > div.show').exists()).toBe(false)
		expect(wrapper.find('div > div > CompConfirm-Stub').exists()).toBe(false)
		expect(wrapper.find('div > div > CompEditGeneric-Stub').exists()).toBe(false)
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(5)

		await wrapper.find('div > div > #structure-selection > input').trigger('click')
		while (wrapper.vm.loading) {
			await nextTick()
		}
		await nextTick()
		expect(wrapper.find('div > div > #emptycontent').exists()).toBe(false)
		expect(wrapper.find('div > div > div.show').exists()).toBe(true)
		expect(wrapper.find('div > div > CompConfirm-Stub').exists()).toBe(false)
		expect(wrapper.find('div > div > CompEditGeneric-Stub').exists()).toBe(true)
		expect(wrapper.find('div > div > CompEditGeneric-Stub').props().itemid).toBe(-1)
		expect(wrapper.find('div > div > CompEditGeneric-Stub').props().models).toBe('structures')
		expect(wrapper.find('div > div > CompEditGeneric-Stub').props().fields.length).toBe(11)
	})

	it('Delete default', async () => {
		ceranaStore.commit('clean')
		ceranaStore.commit('defaultStructure', 3)
		initMockAxios()
		const wrapper = shallowMount(MenuStructure, {
			ceranaI18n,
			ceranaStore,
			propsData: {},
		})
		while (wrapper.vm.loading) {
			await nextTick()
		}
		await nextTick()
		while (wrapper.vm.loading) {
			await nextTick()
		}
		await nextTick()
		expect(wrapper.find('div > div > #emptycontent').exists()).toBe(false)
		expect(wrapper.find('div > div > div.show').exists()).toBe(true)
		expect(wrapper.find('div > div > div.show > div.table > fieldset:nth-child(1) > compshowgeneric-stub').props().itemid).toBe(3)
		expect(wrapper.find('div > div > div.show > div.table > fieldset:nth-child(2) > compmembers-stub').props().structureid).toBe(3)
		expect(wrapper.find('div > div > div.show > div.table > fieldset:nth-child(1) > div.cerana-button > input:nth-child(1)').element.value).toBe('Éditer')
		expect(wrapper.find('div > div > div.show > div.table > fieldset:nth-child(1) > div.cerana-button > input:nth-child(2)').element.value).toBe('Supprimer')
		expect(wrapper.find('div > div > CompConfirm-Stub').exists()).toBe(false)
		expect(wrapper.find('div > div > CompEditGeneric-Stub').exists()).toBe(false)
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(5)

		await wrapper.find('div > div > div.show > div.table > fieldset:nth-child(1) > div.cerana-button > input:nth-child(2)').trigger('click')
		while (wrapper.vm.loading) {
			await nextTick()
		}
		await nextTick()
		expect(wrapper.find('div > div > #emptycontent').exists()).toBe(false)
		expect(wrapper.find('div > div > div.show').exists()).toBe(true)
		expect(wrapper.find('div > div > CompConfirm-Stub').exists()).toBe(true)
		expect(wrapper.find('div > div > CompConfirm-Stub').props().message).toBe('Voulez-vous supprimer cette structure ?')
		expect(wrapper.find('div > div > CompEditGeneric-Stub').exists()).toBe(false)
		await wrapper.vm.confirmDelete(true)
		while (wrapper.vm.loading) {
			await nextTick()
		}
		await nextTick()
		while (wrapper.vm.loading) {
			await nextTick()
		}
		await nextTick()
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(1)
		expect(axios.get).toHaveBeenCalledTimes(6)
		expect(axios.delete).toHaveBeenCalledWith('/apps/cerana_adhesion/structures/3')
		expect(console.error).toHaveBeenCalledTimes(0)
		expect(wrapper.find('div > div > #emptycontent > h2').text()).toBe('Aucune structure !')
		expect(wrapper.find('div > div > div.show').exists()).toBe(false)
		expect(wrapper.find('div > div > CompConfirm-Stub').exists()).toBe(false)
		expect(wrapper.find('div > div > CompEditGeneric-Stub').exists()).toBe(false)
	})

})
