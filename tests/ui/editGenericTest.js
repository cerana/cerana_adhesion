'use strict'

import { nextTick } from 'vue'
import { shallowMount } from '@vue/test-utils'
import { setInputValue, setSelectValue, getSimpleFieldsj, initMockAxios } from './functions.js'
import { ceranaI18n, ceranaStore } from '../../src/cerana_lib'
import CompEditGeneric from '../../src/CompEditGeneric'
import axios from 'axios'
const $ = require('jquery')

jest.mock('axios')
const specialOpt = [{ name: 'aaa', value: 1 }, { name: 'bbb', value: 2 }, { name: 'ccc', value: 3 }]

beforeEach(() => {
	document.documentElement.innerHTML = '<html><body></body></html>'
	global.console.error = jest.fn()
	global.console.warn = jest.fn()
})

describe('Edit Generic Component', () => {
	it('Empty', async () => {
		initMockAxios()
		const wrapper = shallowMount(CompEditGeneric, {
			ceranaI18n,
			ceranaStore,
			propsData: {},
		})
		while (wrapper.vm.loading) {
			await nextTick()
		}
		expect(wrapper.find('modal-stub > div.edit').exists()).toBe(false)
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(0)
		expect(console.error).toHaveBeenCalledTimes(0)
	})

	it('Open', async () => {
		initMockAxios()
		const wrapper = shallowMount(CompEditGeneric, {
			ceranaI18n,
			ceranaStore,
			propsData: {
				models: 'simple',
				itemid: 123,
				fields: getSimpleFieldsj(specialOpt),
			},
		})
		while (wrapper.vm.loading) {
			await nextTick()
		}
		expect(console.error).toHaveBeenCalledTimes(0)
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(1)
		expect(axios.get).toHaveBeenCalledWith('/apps/cerana_adhesion/simple/123')

		expect(wrapper.find('modal-stub > div.edit').exists()).toBe(true)
		expect(wrapper.find('modal-stub > div.edit > h1').text()).toBe('Modification')
		expect(wrapper.findAll('modal-stub > div.edit > div').length).toBe(2)
		expect(wrapper.findAll('modal-stub > div.edit > div:nth-child(2) > div').length).toBe(8)

		expect(wrapper.find('modal-stub > div.edit > div:nth-child(2) > div:nth-child(1) > label').text()).toBe('Name')
		expect(wrapper.find('modal-stub > div.edit > div:nth-child(2) > div:nth-child(2) > label').text()).toBe('Value')
		expect(wrapper.find('modal-stub > div.edit > div:nth-child(2) > div:nth-child(3) > label').text()).toBe('Birthday')
		expect(wrapper.find('modal-stub > div.edit > div:nth-child(2) > div:nth-child(4) > label').text()).toBe('Email')
		expect(wrapper.find('modal-stub > div.edit > div:nth-child(2) > div:nth-child(5) > label').text()).toBe('URL')
		expect(wrapper.find('modal-stub > div.edit > div:nth-child(2) > div:nth-child(6) > label').text()).toBe('Comment')
		expect(wrapper.find('modal-stub > div.edit > div:nth-child(2) > div:nth-child(7) > label').text()).toBe('Image')
		expect(wrapper.find('modal-stub > div.edit > div:nth-child(2) > div:nth-child(8) > label').text()).toBe('Hide')

		expect(wrapper.find('modal-stub > div.edit > div:nth-child(2) > div:nth-child(1) > input').element.value).toBe('foo')
		expect(wrapper.find('modal-stub > div.edit > div:nth-child(2) > div:nth-child(2) > select').element.value).toBe('2')
		expect(wrapper.find('modal-stub > div.edit > div:nth-child(2) > div:nth-child(3) > datetimepicker-stub').attributes('id')).toBe('birth')
		expect(wrapper.find('modal-stub > div.edit > div:nth-child(2) > div:nth-child(4) > input').element.value).toBe('truc@machin.org')
		expect(wrapper.find('modal-stub > div.edit > div:nth-child(2) > div:nth-child(5) > input').element.value).toBe('https://www.machin.org')
		expect(wrapper.find('modal-stub > div.edit > div:nth-child(2) > div:nth-child(6) > textarea').element.value).toBe('aaa\nbbbb\nccc')
		expect(wrapper.find('modal-stub > div.edit > div:nth-child(2) > div:nth-child(7) > span > img').attributes('src')).toBe('/apps/cerana_adhesion/img/empty.png')
		expect(wrapper.find('modal-stub > div.edit > div:nth-child(2) > div:nth-child(7) > span > input').attributes('type')).toBe('file')
		expect(wrapper.find('modal-stub > div.edit > div:nth-child(2) > div:nth-child(8) > span').text()).toBe('uvw')

		expect(wrapper.findAll('modal-stub > div.edit > div.cerana-button > input').length).toBe(2)
		expect(wrapper.find('modal-stub > div.edit > div.cerana-button > input:nth-child(1)').element.value).toBe('Sauver')
		expect(wrapper.find('modal-stub > div.edit > div.cerana-button > input:nth-child(2)').element.value).toBe('Annuler')
	})

	it('Save', async () => {
		initMockAxios()
		const wrapper = shallowMount(CompEditGeneric, {
			ceranaI18n,
			ceranaStore,
			propsData: {
				models: 'simple',
				itemid: 123,
				fields: getSimpleFieldsj(specialOpt),
			},
		})
		while (wrapper.vm.loading) {
			await nextTick()
		}
		expect(console.error).toHaveBeenCalledTimes(0)
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(1)
		expect(axios.get).toHaveBeenCalledWith('/apps/cerana_adhesion/simple/123')

		setInputValue(wrapper, 'input[id="name"]', 'new name')
		setSelectValue(wrapper, 'select[id="value"]', '3')
		setInputValue(wrapper, 'input[id="mail"]', 'truc@machin.com')
		setInputValue(wrapper, 'input[id="link"]', 'https://www.machin.com')

		const fs = require('fs')
		const path = require('path')
		const imageContent = fs.readFileSync(path.join(__dirname, 'image.jpg'))
		const imageFile = new File([imageContent], 'image.jpg', { type: 'image/jpg' })
		wrapper.vm.loadimage({ target: { id: 'image', files: [imageFile] } })

		await wrapper.find('modal-stub > div.edit > div.cerana-button > input:nth-child(1)').trigger('click')
		while (wrapper.vm.loading) {
			await nextTick()
		}
		const expectedObj = {
			id: 123,
			name: 'new name',
			value: 3,
			birth: '1983-07-21',
			mail: 'truc@machin.com',
			link: 'https://www.machin.com',
			comment: 'aaa\nbbbb\nccc',
			hide: 'uvw',
			new: 'zzz',
			unknown: 753,
		}
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(1)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(1)
		expect(axios.put).toHaveBeenCalledWith('/apps/cerana_adhesion/simple/123', expectedObj)
		expect($('div.toastify').length).toBe(1)
		expect(console.error).toHaveBeenCalledTimes(0)
		expect(wrapper.emitted('update')).toStrictEqual([[expectedObj]])
	})

	it('Cancel', async () => {
		initMockAxios()
		const wrapper = shallowMount(CompEditGeneric, {
			ceranaI18n,
			ceranaStore,
			propsData: {
				models: 'simple',
				itemid: 123,
				fields: getSimpleFieldsj(specialOpt),
			},
		})
		while (wrapper.vm.loading) {
			await nextTick()
		}
		expect(console.error).toHaveBeenCalledTimes(0)
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(1)
		expect(axios.get).toHaveBeenCalledWith('/apps/cerana_adhesion/simple/123')

		await wrapper.find('modal-stub > div.edit > div.cerana-button > input:nth-child(2)').trigger('click')
		while (wrapper.vm.loading) {
			await nextTick()
		}
		expect(wrapper.emitted('update')).toStrictEqual([[false]])
	})

	it('New', async () => {
		initMockAxios()
		const wrapper = shallowMount(CompEditGeneric, {
			ceranaI18n,
			ceranaStore,
			propsData: {
				models: 'simple',
				itemid: -1,
				fields: getSimpleFieldsj(specialOpt),
			},
		})
		while (wrapper.vm.loading) {
			await nextTick()
		}
		await nextTick()
		expect(console.error).toHaveBeenCalledTimes(0)
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(0)

		expect(wrapper.find('modal-stub > div.edit').exists()).toBe(true)
		expect(wrapper.find('modal-stub > div.edit > h1').text()).toBe('Création')
		expect(wrapper.findAll('modal-stub > div.edit > div').length).toBe(2)
		expect(wrapper.findAll('modal-stub > div.edit > div:nth-child(2) > div').length).toBe(9)

		expect(wrapper.find('modal-stub > div.edit > div:nth-child(2) > div:nth-child(1) > label').text()).toBe('Name')
		expect(wrapper.find('modal-stub > div.edit > div:nth-child(2) > div:nth-child(2) > label').text()).toBe('Value')
		expect(wrapper.find('modal-stub > div.edit > div:nth-child(2) > div:nth-child(3) > label').text()).toBe('Birthday')
		expect(wrapper.find('modal-stub > div.edit > div:nth-child(2) > div:nth-child(4) > label').text()).toBe('Email')
		expect(wrapper.find('modal-stub > div.edit > div:nth-child(2) > div:nth-child(5) > label').text()).toBe('URL')
		expect(wrapper.find('modal-stub > div.edit > div:nth-child(2) > div:nth-child(6) > label').text()).toBe('Comment')
		expect(wrapper.find('modal-stub > div.edit > div:nth-child(2) > div:nth-child(7) > label').text()).toBe('Image')
		expect(wrapper.find('modal-stub > div.edit > div:nth-child(2) > div:nth-child(8) > label').text()).toBe('Hide')
		expect(wrapper.find('modal-stub > div.edit > div:nth-child(2) > div:nth-child(9) > label').text()).toBe('New')

		expect(wrapper.find('modal-stub > div.edit > div:nth-child(2) > div:nth-child(1) > input').element.value).toBe('???')
		expect(wrapper.find('modal-stub > div.edit > div:nth-child(2) > div:nth-child(2) > select').element.value).toBe('1')
		expect(wrapper.find('modal-stub > div.edit > div:nth-child(2) > div:nth-child(3) > datetimepicker-stub').attributes('id')).toBe('birth')
		expect(wrapper.find('modal-stub > div.edit > div:nth-child(2) > div:nth-child(4) > input').element.value).toBe('')
		expect(wrapper.find('modal-stub > div.edit > div:nth-child(2) > div:nth-child(5) > input').element.value).toBe('')
		expect(wrapper.find('modal-stub > div.edit > div:nth-child(2) > div:nth-child(6) > textarea').element.value).toBe('')
		expect(wrapper.find('modal-stub > div.edit > div:nth-child(2) > div:nth-child(7) > span > img').attributes('src')).toBe('/apps/cerana_adhesion/img/empty.png')
		expect(wrapper.find('modal-stub > div.edit > div:nth-child(2) > div:nth-child(7) > span > input').attributes('type')).toBe('file')
		expect(wrapper.find('modal-stub > div.edit > div:nth-child(2) > div:nth-child(8) > span').text()).toBe('---')
		expect(wrapper.find('modal-stub > div.edit > div:nth-child(2) > div:nth-child(9) > input').text()).toBe('')

		expect(wrapper.findAll('modal-stub > div.edit > div.cerana-button > input').length).toBe(2)
		expect(wrapper.find('modal-stub > div.edit > div.cerana-button > input:nth-child(1)').element.value).toBe('Sauver')
		expect(wrapper.find('modal-stub > div.edit > div.cerana-button > input:nth-child(2)').element.value).toBe('Annuler')
	})

	it('Create', async () => {
		initMockAxios()
		const wrapper = shallowMount(CompEditGeneric, {
			ceranaI18n,
			ceranaStore,
			propsData: {
				models: 'simple',
				itemid: -1,
				fields: getSimpleFieldsj(specialOpt),
			},
		})
		while (wrapper.vm.loading) {
			await nextTick()
		}
		await nextTick()
		expect(console.error).toHaveBeenCalledTimes(0)
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(0)

		setInputValue(wrapper, 'input[id="name"]', 'new name')
		setSelectValue(wrapper, 'select[id="value"]', '3')
		setInputValue(wrapper, 'input[id="mail"]', 'truc@machin.com')
		setInputValue(wrapper, 'input[id="link"]', 'https://www.machin.com')
		setInputValue(wrapper, 'input[id="new"]', 'yes')

		await wrapper.find('modal-stub > div.edit > div.cerana-button > input:nth-child(1)').trigger('click')
		while (wrapper.vm.loading) {
			await nextTick()
		}
		const expectedObj = {
			id: -1,
			name: 'new name',
			value: 3,
			birth: '1900-01-01',
			mail: 'truc@machin.com',
			link: 'https://www.machin.com',
			comment: '',
			hide: '---',
			new: 'yes',
			unknown: 753,
		}
		expect(axios.post).toHaveBeenCalledTimes(1)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(0)
		expect(axios.post).toHaveBeenCalledWith('/apps/cerana_adhesion/simple', expectedObj)
		expect($('div.toastify').length).toBe(1)
		expect(console.error).toHaveBeenCalledTimes(0)
		expect(wrapper.emitted('update')).toStrictEqual([[Object.assign({}, expectedObj, { id: 987 })]])
	})

	it('Invalid', async () => {
		initMockAxios()
		const wrapper = shallowMount(CompEditGeneric, {
			ceranaI18n,
			ceranaStore,
			propsData: {
				models: 'simple',
				itemid: -1,
				fields: getSimpleFieldsj(specialOpt),
			},
		})
		while (wrapper.vm.loading) {
			await nextTick()
		}
		await nextTick()
		expect(console.error).toHaveBeenCalledTimes(0)
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(0)
		setInputValue(wrapper, 'input[id="mail"]', 'truc-machin.com')

		await wrapper.find('modal-stub > div.edit > div.cerana-button > input:nth-child(1)').trigger('click')
		while (wrapper.vm.loading) {
			await nextTick()
		}
		await nextTick()
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(0)
		expect($('div.toastify').length).toBe(1)
		expect(console.error).toHaveBeenCalledTimes(0)
		expect(wrapper.emitted('update')).toStrictEqual([[false]])
	})

	it('Save bad', async () => {
		initMockAxios()
		const wrapper = shallowMount(CompEditGeneric, {
			ceranaI18n,
			ceranaStore,
			propsData: {
				models: 'simple',
				itemid: 111,
				fields: getSimpleFieldsj(specialOpt),
			},
		})
		while (wrapper.vm.loading) {
			await nextTick()
		}
		expect(console.error).toHaveBeenCalledTimes(0)
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(1)
		expect(axios.get).toHaveBeenCalledWith('/apps/cerana_adhesion/simple/111')

		setInputValue(wrapper, 'input[id="name"]', 'new name')
		setSelectValue(wrapper, 'select[id="value"]', '3')
		setInputValue(wrapper, 'input[id="mail"]', 'truc@machin.com')
		setInputValue(wrapper, 'input[id="link"]', 'https://www.machin.com')

		await wrapper.find('modal-stub > div.edit > div.cerana-button > input:nth-child(1)').trigger('click')
		while (wrapper.vm.loading) {
			await nextTick()
		}
		const expectedObj = {
			id: 111,
			name: 'new name',
			value: 3,
			birth: '1983-07-21',
			mail: 'truc@machin.com',
			link: 'https://www.machin.com',
			comment: 'aaa\nbbbb\nccc',
			hide: 'uvw',
			new: 'zzz',
			unknown: 753,
		}
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(1)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(1)
		expect(axios.put).toHaveBeenCalledWith('/apps/cerana_adhesion/simple/111', expectedObj)
		expect($('div.toastify').length).toBe(1)
		expect(console.error).toHaveBeenCalledTimes(1)
		expect(wrapper.emitted('update')).toStrictEqual([[false]])
	})

	it('bad model', async () => {
		initMockAxios()
		const wrapper = shallowMount(CompEditGeneric, {
			ceranaI18n,
			ceranaStore,
			propsData: {
				models: 'bad',
				itemid: 123,
				fields: getSimpleFieldsj(specialOpt),
			},
		})
		while (wrapper.vm.loading) {
			await nextTick()
		}
		expect(console.error).toHaveBeenCalledTimes(3)
		expect(console.error).toHaveBeenNthCalledWith(1, { response: { data: 'no address!', status: 404 } })
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(1)
		expect(axios.get).toHaveBeenCalledWith('/apps/cerana_adhesion/bad/123')

		expect(wrapper.find('modal-stub > div.edit').exists()).toBe(false)
		expect(wrapper.emitted('update')).toStrictEqual([[false]])
	})

})
