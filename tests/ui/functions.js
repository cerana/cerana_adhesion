import Vue from 'vue'
import VueI18n from 'vue-i18n'
import axios from 'axios'

export function getVueI18nTest() {
	Vue.use(VueI18n)
	return new VueI18n({
		locale: 'fr',
		fallbackLocale: 'fr',
		messages: {
			fr: {},
			en: {},
		},
	})
}

export function setInputValue (wrapper, xpath, value) {
	const comp = wrapper.find(xpath)
	comp.element.value = value
	comp.trigger('input')
	comp.trigger('change')
}

export function setSelectValue (wrapper, xpath, value) {
	const comp = wrapper.find(xpath)
	comp.find('option[value="' + value + '"]').element.selected = true
	comp.trigger('input')
	comp.trigger('change')
}

export function getSimpleFieldsj(specialOpt) {
	return [
		{ id: 'name', value: 'Name', type: 'text', default: '???' },
		{ id: 'value', value: 'Value', type: 'select', default: 1, options: specialOpt },
		{ id: 'birth', value: 'Birthday', type: 'date', default: '1900-01-01' },
		{ id: 'mail', value: 'Email', type: 'email', default: '', needed: true },
		{ id: 'link', value: 'URL', type: 'url', default: '' },
		{ id: 'comment', value: 'Comment', type: 'textarea', default: '' },
		{ id: 'image', value: 'Image', type: 'file', default: '', nolisting: true },
		{ id: 'hide', value: 'Hide', type: 'label', default: '---', noshow: true },
		{ id: 'new', value: 'New', type: 'text', default: '', onlynew: true, needed: true },
		{ id: 'unknown', value: 'Unknown', type: '', default: 753 },
	]
}

export function getSimpleObj(objId) {
	return {
		id: objId,
		name: 'foo',
		value: 2,
		birth: '1983-07-21',
		mail: 'truc@machin.org',
		link: 'https://www.machin.org',
		comment: 'aaa\nbbbb\nccc',
		image: '',
		hide: 'uvw',
		new: 'zzz',
		unknown: 123,
	}
}

function getAdherentObj(adhId) {
	return {
		id: adhId,
		num: String(adhId),
		civility: 1,
		lastname: 'Attand',
		firstname: 'Charles',
		birthdaydate: '1986-04-28',
		birthdayplace: 'Là-bas',
		address: '4 place centrale',
		postalcode: '99876',
		city: 'Ici',
		country: 'FR',
		email: 'charles.attend@costy.net',
		phone1: '08.12.23.45.78',
		phone2: '07.98.76.54.32',
		comment: 'Bla bla bla\nBla bla',
		photoid: 'data:*/*;base64,/9j/4AAQSkZJRgABAQEBLAEsAAD/4QvKRXhpZgAASUkqAAgAAAAGABoBBQABAAAAVgAAABsBBQAB',
		status: 0,
	}
}

function getLevels(levelNb) {
	const levelList = []
	for (let idxlevel = 0; idxlevel < levelNb; idxlevel++) {
		levelList.push({ id: idxlevel + 1, name: 'level #' + String(idxlevel + 1), level: idxlevel + 1 })
	}
	return levelList
}

export function getStructures(structureNb, levelNb) {
	const structureList = []
	let structureid = 1
	for (let idxlevel = 0; idxlevel < levelNb; idxlevel++) {
		for (let idxstructure = 0; idxstructure < structureNb * (idxlevel + 1); idxstructure++) {
			structureList.push({ id: structureid, name: 'structure #' + String(structureid), level: idxlevel + 1 })
			structureid = structureid + 1
		}
	}
	return structureList
}

function getAdhesions(adhesionNb) {
	const adhesionList = []
	for (let idxadhesion = 0; idxadhesion < adhesionNb; idxadhesion++) {
		adhesionList.push({ id: idxadhesion + 1, name: 'adhesion #' + String(idxadhesion + 1) })
	}
	return adhesionList
}

function getResponsabilities(responsabilityNb, levelNb) {
	const responsabilityList = []
	let responsabilityid = 1
	for (let idxlevel = 0; idxlevel < levelNb; idxlevel++) {
		for (let idxresponsability = 0; idxresponsability < responsabilityNb * (idxlevel + 1); idxresponsability++) {
			responsabilityList.push({ id: responsabilityid, name: 'responsability #' + String(responsabilityid), level: idxlevel + 1 })
			responsabilityid = responsabilityid + 1
		}
	}
	return responsabilityList
}

function getPractices(practiceNb) {
	const practiceList = []
	for (let idxpractice = 0; idxpractice < practiceNb; idxpractice++) {
		practiceList.push({ id: idxpractice + 1, name: 'practice #' + String(idxpractice + 1) })
	}
	return practiceList
}

function getStats(nameStat, statsNb) {
	const statsList = []
	for (let idxstats = 0; idxstats < statsNb; idxstats++) {
		const newStats = {}
		newStats[nameStat] = (idxstats + 1)
		newStats.nb = idxstats * 13 - 4
		statsList.push(newStats)
	}
	return statsList
}

export function initMockAxios(addon) {
	axios.get.mockClear()
	axios.delete.mockClear()
	axios.put.mockClear()
	axios.post.mockClear()
	axios.get.mockImplementation((url) => {
		let dataGet = null
		let error = 'no address!'
		const urlAndParams = url.split('?')
		const urlSplited = urlAndParams[0].split('/')
		if (!url.startsWith('/apps/cerana_adhesion/')) {
			return Promise.resolve({ status: 500, data: 'bad url ' + url })
		}
		const urlMiddle = urlSplited[3]
		const urlEnd = urlSplited[urlSplited.length - 1]
		if (urlMiddle === 'simple') {
			if (urlSplited.length === 4) {
				dataGet = { list: [getSimpleObj(123)], total: 1 }
			} else if (urlSplited.length === 5) {
				if (urlEnd === '666') {
					error = 'not exist!'
				} else {
					dataGet = getSimpleObj(parseInt(urlEnd))
				}
			}
		} else if (urlMiddle === 'adherents') {
			if (urlSplited.length === 5) {
				if (urlEnd === '666') {
					error = 'not exist!'
				} else {
					dataGet = getAdherentObj(parseInt(urlEnd))
					if (addon !== undefined) {
						dataGet = Object.assign({}, dataGet, addon)
					}
				}
			}
		} else if (urlMiddle === 'levels') {
			if (urlSplited.length === 4) {
				dataGet = { list: getLevels(3), total: 3 }
			}
		} else if (urlMiddle === 'structures') {
			if (urlSplited.length === 4) {
				dataGet = { list: getStructures(2, 3), total: 14 }
			}
		} else if (urlMiddle === 'type_adhesions') {
			if (urlSplited.length === 4) {
				dataGet = { list: getAdhesions(4), total: 4 }
			}
		} else if (urlMiddle === 'type_responsabilitys') {
			if (urlSplited.length === 4) {
				dataGet = { list: getResponsabilities(3, 3), total: 18 }
			}
		} else if (urlMiddle === 'type_practices') {
			if (urlSplited.length === 4) {
				dataGet = { list: getPractices(5), total: 5 }
			}
		} else if (urlMiddle === 'statistics') {
			if (urlSplited.length === 4) {
				dataGet = { subscriptions: getStats('typeadhesion', 4), practices: getStats('typepractice', 5), responsabilities: getStats('typeresponsability', 18) }
			}
		}
		if (dataGet !== null) {
			return Promise.resolve({ status: 200, data: dataGet })
		} else {
			return Promise.reject({ response: { status: 404, data: error } })
		}
	})
	axios.put.mockImplementation((url, data) => {
		let dataPut = null
		let error = 'no address!'
		const urlSplited = url.split('/')
		if (!url.startsWith('/apps/cerana_adhesion/')) {
			return Promise.resolve({ status: 500, data: 'bad url ' + url })
		}
		const urlMiddle = urlSplited[3]
		const urlEnd = urlSplited[urlSplited.length - 1]
		if (urlMiddle === 'simple') {
			if (urlEnd === '111') {
				error = 'bad!'
			} else {
				dataPut = Object.assign({}, data)
			}
		} else if (urlMiddle === 'connection') {
			if (urlSplited.length === 4) {
				dataPut = Object.assign({}, data)
			}
		}
		if (dataPut !== null) {
			return Promise.resolve({ status: 200, data: dataPut })
		} else {
			return Promise.reject({ response: { status: 404, data: error } })
		}
	})

	axios.post.mockImplementation((url, data) => {
		let dataPut = null
		let error = 'no address!'
		const urlSplited = url.split('/')
		if (!url.startsWith('/apps/cerana_adhesion/')) {
			return Promise.resolve({ status: 500, data: 'bad url ' + url })
		}
		const urlMiddle = urlSplited[3]
		if (urlMiddle === 'simple') {
			dataPut = Object.assign({}, data)
			dataPut.id = 987
		} else if (urlMiddle === 'subscriptions') {
			dataPut = Object.assign({}, data)
		}
		if (dataPut !== null) {
			return Promise.resolve({ status: 200, data: dataPut })
		} else {
			return Promise.reject({ response: { status: 404, data: error } })
		}
	})

	axios.delete.mockImplementation((url) => {
		let dataDelete = null
		let error = 'no address!'
		const urlAndParams = url.split('?')
		const urlSplited = urlAndParams[0].split('/')
		if (!url.startsWith('/apps/cerana_adhesion/')) {
			return Promise.resolve({ status: 500, data: 'bad url ' + url })
		}
		const urlMiddle = urlSplited[3]
		const urlEnd = urlSplited[urlSplited.length - 1]
		if (urlMiddle === 'simple') {
			if (urlSplited.length === 5) {
				if (urlEnd === '666') {
					error = 'not exist!'
				} else {
					dataDelete = 'OK'
				}
			}
		} else if (urlMiddle === 'structures') {
			if (urlSplited.length === 5) {
				if (urlEnd === '666') {
					error = 'not exist!'
				} else {
					dataDelete = 'OK'
				}
			}
		} else if (urlMiddle === 'subscriptions') {
			if (urlSplited.length === 5) {
				if (urlEnd === '666') {
					error = 'not exist!'
				} else {
					dataDelete = 'OK'
				}
			}
		}
		if (dataDelete !== null) {
			return Promise.resolve({ status: 200, data: dataDelete })
		} else {
			return Promise.reject({ response: { status: 404, data: error } })
		}
	})

}
