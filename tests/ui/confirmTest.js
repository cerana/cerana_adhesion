'use strict'

import { nextTick } from 'vue'
import { shallowMount } from '@vue/test-utils'
import { ceranaI18n, ceranaStore } from '../../src/cerana_lib.js'
import CompConfirm from '../../src/CompConfirm'


beforeEach(() => {
	document.documentElement.innerHTML = '<html><body></body></html>'
	global.console.error = jest.fn()
	global.console.warn = jest.fn()
})

describe('Confirm Component', () => {
	it('Show', async () => {
		const wrapper = shallowMount(CompConfirm, {
			ceranaI18n,
			ceranaStore,
			propsData: {
				message: 'yes/no ?',
			},
		})
		while (wrapper.vm.loading) {
			await nextTick()
		}
		expect(wrapper.find('modal-stub > div.confirm').exists()).toBe(true)
		expect(wrapper.find('modal-stub > div.confirm > h1').text()).toBe('yes/no ?')
		expect(wrapper.findAll('modal-stub > div.confirm > div.cerana-button > input').length).toBe(2)
		expect(wrapper.find('modal-stub > div.confirm > div.cerana-button > input:nth-child(1)').element.value).toBe('OK')
		expect(wrapper.find('modal-stub > div.confirm > div.cerana-button > input:nth-child(2)').element.value).toBe('Annuler')
	})

	it('OK', async () => {
		const wrapper = shallowMount(CompConfirm, {
			ceranaI18n,
			ceranaStore,
			propsData: {
				message: 'yes/no ?',
			},
		})
		while (wrapper.vm.loading) {
			await nextTick()
		}
		await wrapper.find('modal-stub > div.confirm > div.cerana-button > input:nth-child(1)').trigger('click')
		while (wrapper.vm.loading) {
			await nextTick()
		}
		expect(wrapper.emitted('confirm')).toStrictEqual([[true]])
	})

	it('OK', async () => {
		const wrapper = shallowMount(CompConfirm, {
			ceranaI18n,
			ceranaStore,
			propsData: {
				message: 'yes/no ?',
			},
		})
		while (wrapper.vm.loading) {
			await nextTick()
		}
		await wrapper.find('modal-stub > div.confirm > div.cerana-button > input:nth-child(2)').trigger('click')
		while (wrapper.vm.loading) {
			await nextTick()
		}
		expect(wrapper.emitted('confirm')).toStrictEqual([[false]])
	})
})
