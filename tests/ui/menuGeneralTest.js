'use strict'

import { nextTick } from 'vue'
import { shallowMount } from '@vue/test-utils'
import { initMockAxios } from './functions.js'
import { ceranaI18n, ceranaStore } from '../../src/cerana_lib.js'
import axios from 'axios'
import MenuGeneral from '../../src/MenuGeneral'

jest.mock('axios')

beforeEach(() => {
	document.documentElement.innerHTML = '<html><body></body></html>'
	global.console.error = jest.fn()
	global.console.warn = jest.fn()
})

describe('Menu General', () => {
	it('Show default', async () => {
		ceranaStore.commit('clean')
		initMockAxios()
		const wrapper = shallowMount(MenuGeneral, {
			ceranaI18n,
			ceranaStore,
			propsData: {},
		})
		while (wrapper.vm.loading) {
			await nextTick()
		}
		expect(wrapper.find('div > h1').text()).toBe('Général')
		expect(wrapper.find('div > tabs-stub > tab-stub').attributes('name')).toBe('Les échelons')
		expect(wrapper.find('div > tabs-stub > tab-stub > complistgeneric-stub').props().models).toBe('levels')
		expect(wrapper.find('div > tabs-stub > tab-stub > complistgeneric-stub').props().fields).toStrictEqual([
			{ id: 'level', value: 'Niveau N°', type: 'number', default: 1, needed: true, attribs: { min: 1, max: 10 } },
			{ id: 'name', value: 'Nom', type: 'text', needed: true, default: '' },
			{ id: 'comment', value: 'Commentaires', type: 'textarea', default: '', large: true },
		])
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(5)
	})
	it('change', async () => {
		ceranaStore.commit('clean')
		initMockAxios()
		const wrapper = shallowMount(MenuGeneral, {
			ceranaI18n,
			ceranaStore,
			propsData: {},
		})
		 while (wrapper.vm.loading) {
			await nextTick()
		}
		await wrapper.vm.tabchange({ tab: 'levels' })
		await nextTick()
		await wrapper.vm.updatedLevels(true)
		while (wrapper.vm.loading) {
			await nextTick()
		}
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(6)
	})
})
