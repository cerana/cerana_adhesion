'use strict'

import { nextTick } from 'vue'
import { shallowMount } from '@vue/test-utils'
import { initMockAxios } from './functions.js'
import { ceranaI18n, ceranaStore } from '../../src/cerana_lib.js'
import axios from 'axios'
import MenuCurrent from '../../src/MenuCurrent'

jest.mock('axios')

beforeEach(() => {
	document.documentElement.innerHTML = '<html><body></body></html>'
	global.console.error = jest.fn()
	global.console.warn = jest.fn()
})

describe('Menu Current', () => {
	it('No show', async () => {
		ceranaStore.commit('clean')
		initMockAxios()
		const wrapper = shallowMount(MenuCurrent, {
			ceranaI18n,
			ceranaStore,
			propsData: {},
		})
		while (wrapper.vm.loading) {
			await nextTick()
		}
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(5)
		expect(wrapper.find('#current > h1').text()).toBe('Ma fiche')
		expect(wrapper.find('#current > div > #emptycontent > h2').text()).toBe('Compte non adhérent !')
		expect(wrapper.find('#current > div > div.currentfile > CompAdherent-stub').exists()).toBe(true)
		expect(wrapper.find('#current > div > div.currentfile > CompAdherent-stub').props().initadherentid).toBe(-1)
		expect(wrapper.find('#current > div > div.currentfile > CompAdherent-stub').element.style.display).toBe('none')
		expect(wrapper.find('#current > div > div.currentfile > CompEditGeneric-stub').exists()).toBe(false)
		expect(wrapper.find('#current > div > div.currentfile > div.cerana-button').exists()).toBe(false)
	})

	it('Show current', async () => {
		ceranaStore.commit('clean')
		initMockAxios()
		const wrapper = shallowMount(MenuCurrent, {
			ceranaI18n,
			ceranaStore,
			propsData: {},
		})
		while (wrapper.vm.loading) {
			await nextTick()
		}
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(5)

		await wrapper.vm.showCurrent({ id: 123 })
		await nextTick()
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(5)
		expect(wrapper.find('#current > h1').text()).toBe('Ma fiche')
		expect(wrapper.find('#current > div > #emptycontent').exists()).toBe(false)
		expect(wrapper.find('#current > div > div.currentfile > CompAdherent-stub').props().initadherentid).toBe(123)
		expect(wrapper.find('#current > div > div.currentfile > CompAdherent-stub').element.style.display).toBe('')
		expect(wrapper.find('#current > div > div.currentfile > CompEditGeneric-stub').exists()).toBe(false)
		expect(wrapper.find('#current > div > div.currentfile > div.cerana-button > input').element.value).toBe('Éditer')

		await wrapper.find('#current > div > div.currentfile > div.cerana-button > input').trigger('click')
		await nextTick()
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(5)
		expect(wrapper.find('#current > h1').text()).toBe('Ma fiche')
		expect(wrapper.find('#current > div > #emptycontent').exists()).toBe(false)
		expect(wrapper.find('#current > div > div.currentfile > CompAdherent-stub').props().initadherentid).toBe(123)
		expect(wrapper.find('#current > div > div.currentfile > CompEditGeneric-stub').props().itemid).toBe(123)
		expect(wrapper.find('#current > div > div.currentfile > CompEditGeneric-stub').props().models).toBe('adherents')
		expect(wrapper.find('#current > div > div.currentfile > CompEditGeneric-stub').props().fields.length).toBe(15)
		expect(wrapper.find('#current > div > div.currentfile > div.cerana-button > input').element.value).toBe('Éditer')

		await wrapper.vm.refresh()
		await nextTick()
		expect(wrapper.find('#current > h1').text()).toBe('Ma fiche')
		expect(wrapper.find('#current > div > #emptycontent').exists()).toBe(false)
		expect(wrapper.find('#current > div > div.currentfile > CompAdherent-stub').props().initadherentid).toBe(123)
		expect(wrapper.find('#current > div > div.currentfile > CompEditGeneric-stub').exists()).toBe(false)
		expect(wrapper.find('#current > div > div.currentfile > div.cerana-button > input').element.value).toBe('Éditer')
	})
})
