'use strict'

import { nextTick } from 'vue'
import { shallowMount } from '@vue/test-utils'
import { setSelectValue, initMockAxios } from './functions.js'
import { ceranaI18n, ceranaStore } from '../../src/cerana_lib.js'
import axios from 'axios'
import CompAdherent from '../../src/CompAdherent'

jest.mock('axios')

beforeEach(() => {
	document.documentElement.innerHTML = '<html><body></body></html>'
	global.console.error = jest.fn()
	global.console.warn = jest.fn()
})

describe('Comp Adherent', () => {
	it('Show null', async () => {
		ceranaStore.commit('clean')
		initMockAxios()
		const wrapper = shallowMount(CompAdherent, {
			ceranaI18n,
			ceranaStore,
			propsData: {},
		})
		await nextTick()
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(0)
		expect(wrapper.find('#adherent > tabs-stub').exists()).toBe(true)
		expect(wrapper.findAll('#adherent > tabs-stub > tab-stub').length).toBe(1)
		expect(wrapper.find('#adherent > tabs-stub > tab-stub > compshowadherent-stub').props().initadherentid).toBe(-1)
	})

	it('Show default', async () => {
		ceranaStore.commit('clean')
		ceranaStore.commit('defaultSeason', '2020-09-01')
		initMockAxios()
		const wrapper = shallowMount(CompAdherent, {
			ceranaI18n,
			ceranaStore,
			propsData: {
				initadherentid: 357,
			},
		})
		await nextTick()
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(0)
		expect(wrapper.find('#adherent > tabs-stub').exists()).toBe(true)
		expect(wrapper.findAll('#adherent > tabs-stub > tab-stub').length).toBe(1)
		expect(wrapper.find('#adherent > tabs-stub > tab-stub > compshowadherent-stub').props().initadherentid).toBe(357)
		const adhobj = { id: 357, list_year: ['2017-09-01', '2018-09-01', '2019-09-01'] }
		wrapper.vm.showAdherentCurrent(adhobj)
		await nextTick()
		expect(wrapper.vm.adherent).toStrictEqual(adhobj)
		expect(wrapper.emitted('current')).toStrictEqual([[adhobj]])
		expect(wrapper.find('#adherent > tabs-stub').exists()).toBe(true)
		expect(wrapper.findAll('#adherent > tabs-stub > tab-stub').length).toBe(2)
		expect(wrapper.find('#adherent > tabs-stub > tab-stub:nth-child(1) > compshowadherent-stub').props().initadherentid).toBe(357)
		expect(wrapper.find('#adherent > tabs-stub > tab-stub:nth-child(2) > div.selection > label').text()).toBe('Saison')
		expect(wrapper.findAll('#adherent > tabs-stub > tab-stub:nth-child(2) > div.selection > select > option').length).toBe(23)
		expect(wrapper.find('#adherent > tabs-stub > tab-stub:nth-child(2) > CompSubscription-stub').props().currentseason).toBe('2020-09-01')
		expect(wrapper.find('#adherent > tabs-stub > tab-stub:nth-child(2) > CompSubscription-stub').props().adherent).toStrictEqual(adhobj)

		setSelectValue(wrapper, '#adherent > tabs-stub > tab-stub:nth-child(2) > div.selection > select', '2018-09-01')
		await nextTick()
		expect(wrapper.find('#adherent > tabs-stub').exists()).toBe(true)
		expect(wrapper.findAll('#adherent > tabs-stub > tab-stub').length).toBe(2)
		expect(wrapper.find('#adherent > tabs-stub > tab-stub:nth-child(1) > compshowadherent-stub').props().initadherentid).toBe(357)
		expect(wrapper.find('#adherent > tabs-stub > tab-stub:nth-child(2) > div.selection > label').text()).toBe('Saison')
		expect(wrapper.findAll('#adherent > tabs-stub > tab-stub:nth-child(2) > div.selection > select > option').length).toBe(23)
		expect(wrapper.find('#adherent > tabs-stub > tab-stub:nth-child(2) > CompSubscription-stub').props().currentseason).toBe('2018-09-01')
		expect(wrapper.find('#adherent > tabs-stub > tab-stub:nth-child(2) > CompSubscription-stub').props().adherent).toStrictEqual(adhobj)

		expect(ceranaStore.state.defaultSeason).toBe('2018-09-01')

		await wrapper.vm.tabchange({ tab: { id: 'identity' } })
		await nextTick()
		await wrapper.vm.tabchange({ tab: { id: 'membership' } })
		await nextTick()
	})

})
