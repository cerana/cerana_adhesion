'use strict'

import { nextTick } from 'vue'
import { shallowMount } from '@vue/test-utils'
import { initMockAxios } from './functions.js'
import { ceranaI18n, ceranaStore } from '../../src/cerana_lib.js'
import axios from 'axios'
import CompSubscription from '../../src/CompSubscription'

jest.mock('axios')
let adhObj = null

beforeEach(() => {
	adhObj = { id: 357, list_year: ['2017-09-01', '2018-09-01', '2019-09-01'] }
	document.documentElement.innerHTML = '<html><body></body></html>'
	global.console.error = jest.fn()
	global.console.warn = jest.fn()
})

describe('Comp Subscription', () => {
	it('Show empty', async () => {
		ceranaStore.commit('clean')
		initMockAxios()
		const wrapper = shallowMount(CompSubscription, {
			ceranaI18n,
			ceranaStore,
			propsData: {},
		})
		await nextTick()
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(0)
		expect(wrapper.find('#emptysubscription > h2').text()).toBe('Pas de cotisation pour cette saison !')
		expect(wrapper.find('#emptysubscription > div.cerana-button').exists()).toBe(false)
		expect(wrapper.find('#subscription > compeditgeneric-stub').exists()).toBe(false)
		expect(wrapper.findAll('#subscription > div > fieldset').length).toBe(1)
		expect(wrapper.find('#subscription > div > fieldset > compshowgeneric-stub').props().models).toBe('subscriptions')
		expect(wrapper.find('#subscription > div > fieldset > compshowgeneric-stub').props().itemid).toBe(0)
		expect(wrapper.find('#subscription > div > fieldset > compshowgeneric-stub').props().ignoreid).toBe(0)
		expect(wrapper.find('#subscription > div > fieldset > compshowgeneric-stub').props().fields).toStrictEqual([])
		expect(wrapper.find('#subscription > div > fieldset > compshowgeneric-stub').props().search).toStrictEqual(null)
		expect(wrapper.find('#subscription > compconfirm-stub').exists()).toBe(false)
		expect(wrapper.emitted('update')).toStrictEqual(undefined)
		expect(console.error).toHaveBeenCalledTimes(0)
	})

	it('Create subscription', async () => {
		ceranaStore.commit('clean')
		initMockAxios()
		const wrapper = shallowMount(CompSubscription, {
			ceranaI18n,
			ceranaStore,
			propsData: {
				currentseason: '2019-09-01',
				adherent: adhObj,
			},
		})
		await nextTick()
		await wrapper.vm.showSubscriptionCurrent(null)
		await nextTick()
		expect(wrapper.find('#emptysubscription').exists()).toBe(true)
		expect(wrapper.find('#emptysubscription > div.cerana-button > input').element.value).toBe('Ajouter')
		expect(wrapper.find('#subscription > compeditgeneric-stub').exists()).toBe(false)
		expect(wrapper.findAll('#subscription > div > fieldset').length).toBe(1)
		expect(wrapper.find('#subscription > div > fieldset > compshowgeneric-stub').exists()).toBe(true)
		expect(wrapper.find('#subscription > compconfirm-stub').exists()).toBe(false)

		await wrapper.find('#emptysubscription > div > input').trigger('click')
		await nextTick()
		expect(wrapper.find('#emptysubscription').exists()).toBe(true)
		expect(wrapper.find('#subscription > compeditgeneric-stub').exists()).toBe(true)
		expect(wrapper.find('#subscription > compeditgeneric-stub').props().itemid).toBe(-1)
		expect(wrapper.find('#subscription > compeditgeneric-stub').props().models).toBe('subscriptions')
		expect(wrapper.find('#subscription > compeditgeneric-stub').props().fields.length).toBe(8)
		expect(wrapper.findAll('#subscription > div > fieldset').length).toBe(1)
		expect(wrapper.find('#subscription > div > fieldset > compshowgeneric-stub').exists()).toBe(true)
		expect(wrapper.find('#subscription > compconfirm-stub').exists()).toBe(false)
		expect(wrapper.emitted('update')).toStrictEqual(undefined)

		await wrapper.vm.refreshSubscription()
		await nextTick()
		expect(wrapper.find('#emptysubscription').exists()).toBe(true)
		expect(wrapper.find('#subscription > compeditgeneric-stub').exists()).toBe(false)
		expect(wrapper.findAll('#subscription > div > fieldset').length).toBe(1)
		expect(wrapper.find('#subscription > div > fieldset > compshowgeneric-stub').exists()).toBe(true)
		expect(wrapper.find('#subscription > compconfirm-stub').exists()).toBe(false)
		expect(wrapper.emitted('update')).toStrictEqual([[true]])

		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(0)
		expect(console.error).toHaveBeenCalledTimes(0)
	})

	it('Show from adherent', async () => {
		ceranaStore.commit('clean')
		initMockAxios()
		const wrapper = shallowMount(CompSubscription, {
			ceranaI18n,
			ceranaStore,
			propsData: {
				currentseason: '2019-09-01',
				adherent: adhObj,
			},
		})
		await nextTick()
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(0)
		expect(wrapper.find('#emptysubscription').exists()).toBe(true)
		expect(wrapper.find('#subscription > compeditgeneric-stub').exists()).toBe(false)
		expect(wrapper.findAll('#subscription > div > fieldset').length).toBe(1)
		expect(wrapper.find('#subscription > div > fieldset > compshowgeneric-stub').props().models).toBe('subscriptions')
		expect(wrapper.find('#subscription > div > fieldset > compshowgeneric-stub').props().itemid).toBe(0)
		expect(wrapper.find('#subscription > div > fieldset > compshowgeneric-stub').props().ignoreid).toBe(0)
		expect(wrapper.find('#subscription > div > fieldset > compshowgeneric-stub').props().fields.length).toBe(8)
		expect(wrapper.find('#subscription > div > fieldset > compshowgeneric-stub').props().search).toStrictEqual({ adherent: 357, dateref: '2019-09-01' })
		expect(wrapper.find('#subscription > compconfirm-stub').exists()).toBe(false)
		await wrapper.vm.showSubscriptionCurrent({ id: 111, structure: 222 })
		await nextTick()
		expect(wrapper.find('#emptysubscription').exists()).toBe(false)
		expect(wrapper.find('#subscription > compeditgeneric-stub').exists()).toBe(false)
		expect(wrapper.findAll('#subscription > div > fieldset').length).toBe(3)
		expect(wrapper.find('#subscription > div > fieldset:nth-child(1) > legend').text()).toBe('Cotisation')
		expect(wrapper.find('#subscription > div > fieldset:nth-child(1) > compshowgeneric-stub').exists()).toBe(true)
		expect(wrapper.find('#subscription > div > fieldset:nth-child(1) > compshowgeneric-stub').props().search).toStrictEqual({ adherent: 357, dateref: '2019-09-01' })
		expect(wrapper.find('#subscription > div > fieldset:nth-child(1) > div.cerana-button > input:nth-child(1)').element.value).toBe('Éditer')
		expect(wrapper.find('#subscription > div > fieldset:nth-child(1) > div.cerana-button > input:nth-child(2)').element.value).toBe('Supprimer')
		expect(wrapper.find('#subscription > div > fieldset:nth-child(2) > legend').text()).toBe('Structure')
		expect(wrapper.find('#subscription > div > fieldset:nth-child(2) > compshowgeneric-stub').exists()).toBe(true)
		expect(wrapper.find('#subscription > div > fieldset:nth-child(2) > compshowgeneric-stub').props().models).toBe('structures')
		expect(wrapper.find('#subscription > div > fieldset:nth-child(2) > compshowgeneric-stub').props().itemid).toBe(222)
		expect(wrapper.find('#subscription > div > fieldset:nth-child(2) > compshowgeneric-stub').props().ignoreid).toBe(0)
		expect(wrapper.find('#subscription > div > fieldset:nth-child(2) > compshowgeneric-stub').props().fields.length).toStrictEqual(5)
		expect(wrapper.findAll('#subscription > div > fieldset:nth-child(3) > tabs-stub > tab-stub').length).toBe(2)
		expect(wrapper.find('#subscription > div > fieldset:nth-child(3) > tabs-stub > tab-stub:nth-child(1) > complistgeneric-stub').exists()).toBe(true)
		expect(wrapper.find('#subscription > div > fieldset:nth-child(3) > tabs-stub > tab-stub:nth-child(1) > complistgeneric-stub').props().models).toBe('practices')
		expect(wrapper.find('#subscription > div > fieldset:nth-child(3) > tabs-stub > tab-stub:nth-child(1) > complistgeneric-stub').props().searching).toStrictEqual({ subscription: 111 })
		expect(wrapper.find('#subscription > div > fieldset:nth-child(3) > tabs-stub > tab-stub:nth-child(1) > complistgeneric-stub').props().fields.length).toBe(3)
		expect(wrapper.find('#subscription > div > fieldset:nth-child(3) > tabs-stub > tab-stub:nth-child(1) > complistgeneric-stub').props().showoption).toBe('0')
		expect(wrapper.find('#subscription > div > fieldset:nth-child(3) > tabs-stub > tab-stub:nth-child(1) > complistgeneric-stub').props().addoption).toBe('1')
		expect(wrapper.find('#subscription > div > fieldset:nth-child(3) > tabs-stub > tab-stub:nth-child(1) > complistgeneric-stub').props().editoption).toBe('1')
		expect(wrapper.find('#subscription > div > fieldset:nth-child(3) > tabs-stub > tab-stub:nth-child(1) > complistgeneric-stub').props().deleteoption).toBe('1')
		expect(wrapper.find('#subscription > div > fieldset:nth-child(3) > tabs-stub > tab-stub:nth-child(2) > complistgeneric-stub').exists()).toBe(true)
		expect(wrapper.find('#subscription > div > fieldset:nth-child(3) > tabs-stub > tab-stub:nth-child(2) > complistgeneric-stub').props().models).toBe('responsabilitys')
		expect(wrapper.find('#subscription > div > fieldset:nth-child(3) > tabs-stub > tab-stub:nth-child(2) > complistgeneric-stub').props().searching).toStrictEqual({ subscription: 111 })
		expect(wrapper.find('#subscription > div > fieldset:nth-child(3) > tabs-stub > tab-stub:nth-child(2) > complistgeneric-stub').props().fields.length).toBe(4)
		expect(wrapper.find('#subscription > div > fieldset:nth-child(3) > tabs-stub > tab-stub:nth-child(2) > complistgeneric-stub').props().showoption).toBe('0')
		expect(wrapper.find('#subscription > div > fieldset:nth-child(3) > tabs-stub > tab-stub:nth-child(2) > complistgeneric-stub').props().addoption).toBe('1')
		expect(wrapper.find('#subscription > div > fieldset:nth-child(3) > tabs-stub > tab-stub:nth-child(2) > complistgeneric-stub').props().editoption).toBe('1')
		expect(wrapper.find('#subscription > div > fieldset:nth-child(3) > tabs-stub > tab-stub:nth-child(2) > complistgeneric-stub').props().deleteoption).toBe('1')
		expect(wrapper.find('#subscription > compconfirm-stub').exists()).toBe(false)
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(0)
		await wrapper.vm.tabchange({ tab: { id: 'practice' } })
		await nextTick()
		await wrapper.vm.tabchange({ tab: { id: 'responsability' } })
		await nextTick()
		await wrapper.vm.updatedPractice()
		await nextTick()
		await wrapper.vm.updatedResponsabilitys()
		await nextTick()
		expect(wrapper.emitted('update')).toStrictEqual(undefined)
		expect(console.error).toHaveBeenCalledTimes(0)
	})

	it('Edit from adherent', async () => {
		ceranaStore.commit('clean')
		initMockAxios()
		const wrapper = shallowMount(CompSubscription, {
			ceranaI18n,
			ceranaStore,
			propsData: {
				currentseason: '2019-09-01',
				adherent: adhObj,
			},
		})
		await nextTick()
		await wrapper.vm.showSubscriptionCurrent({ id: 111, structure: 222 })
		await nextTick()
		expect(wrapper.find('#emptysubscription').exists()).toBe(false)
		expect(wrapper.find('#subscription > compeditgeneric-stub').exists()).toBe(false)
		expect(wrapper.findAll('#subscription > div > fieldset').length).toBe(3)
		expect(wrapper.find('#subscription > div > fieldset:nth-child(1) > div.cerana-button > input:nth-child(1)').element.value).toBe('Éditer')
		expect(wrapper.find('#subscription > div > fieldset:nth-child(1) > div.cerana-button > input:nth-child(2)').element.value).toBe('Supprimer')
		expect(wrapper.findAll('#subscription > div > fieldset:nth-child(3) > tabs-stub > tab-stub').length).toBe(2)
		expect(wrapper.find('#subscription > compconfirm-stub').exists()).toBe(false)
		expect(wrapper.emitted('update')).toStrictEqual(undefined)

		await wrapper.find('#subscription > div > fieldset:nth-child(1) > div.cerana-button > input:nth-child(1)').trigger('click')
		await nextTick()
		expect(wrapper.find('#emptysubscription').exists()).toBe(false)
		expect(wrapper.find('#subscription > compeditgeneric-stub').exists()).toBe(true)
		expect(wrapper.find('#subscription > compeditgeneric-stub').props().itemid).toBe(111)
		expect(wrapper.find('#subscription > compeditgeneric-stub').props().models).toBe('subscriptions')
		expect(wrapper.find('#subscription > compeditgeneric-stub').props().fields.length).toBe(8)
		expect(wrapper.findAll('#subscription > div > fieldset').length).toBe(3)
		expect(wrapper.find('#subscription > div > fieldset > compshowgeneric-stub').exists()).toBe(true)
		expect(wrapper.findAll('#subscription > div > fieldset:nth-child(3) > tabs-stub > tab-stub').length).toBe(2)
		expect(wrapper.find('#subscription > compconfirm-stub').exists()).toBe(false)
		expect(wrapper.emitted('update')).toStrictEqual(undefined)

		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(0)
		expect(console.error).toHaveBeenCalledTimes(0)
	})

	it('Delete from adherent', async () => {
		ceranaStore.commit('clean')
		initMockAxios()
		const wrapper = shallowMount(CompSubscription, {
			ceranaI18n,
			ceranaStore,
			propsData: {
				currentseason: '2019-09-01',
				adherent: adhObj,
			},
		})
		await nextTick()
		await wrapper.vm.showSubscriptionCurrent({ id: 111, structure: 222 })
		await nextTick()
		expect(wrapper.find('#emptysubscription').exists()).toBe(false)
		expect(wrapper.find('#subscription > compeditgeneric-stub').exists()).toBe(false)
		expect(wrapper.findAll('#subscription > div > fieldset').length).toBe(3)
		expect(wrapper.find('#subscription > div > fieldset:nth-child(1) > div.cerana-button > input:nth-child(1)').element.value).toBe('Éditer')
		expect(wrapper.find('#subscription > div > fieldset:nth-child(1) > div.cerana-button > input:nth-child(2)').element.value).toBe('Supprimer')
		expect(wrapper.findAll('#subscription > div > fieldset:nth-child(3) > tabs-stub > tab-stub').length).toBe(2)
		expect(wrapper.find('#subscription > compconfirm-stub').exists()).toBe(false)
		expect(wrapper.emitted('update')).toStrictEqual(undefined)

		await wrapper.find('#subscription > div > fieldset:nth-child(1) > div.cerana-button > input:nth-child(2)').trigger('click')
		await nextTick()
		expect(wrapper.find('#emptysubscription').exists()).toBe(false)
		expect(wrapper.find('#subscription > compeditgeneric-stub').exists()).toBe(false)
		expect(wrapper.findAll('#subscription > div > fieldset').length).toBe(3)
		expect(wrapper.findAll('#subscription > div > fieldset:nth-child(3) > tabs-stub > tab-stub').length).toBe(2)
		expect(wrapper.find('#subscription > compconfirm-stub').exists()).toBe(true)
		expect(wrapper.emitted('update')).toStrictEqual(undefined)

		await wrapper.vm.confirmRemove(true)
		await nextTick()
		await nextTick()
		await nextTick()
		expect(wrapper.find('#emptysubscription').exists()).toBe(false)
		expect(wrapper.find('#subscription > compeditgeneric-stub').exists()).toBe(false)
		expect(wrapper.findAll('#subscription > div > fieldset').length).toBe(3)
		expect(wrapper.find('#subscription > div > fieldset:nth-child(1) > div.cerana-button > input:nth-child(1)').element.value).toBe('Éditer')
		expect(wrapper.find('#subscription > div > fieldset:nth-child(1) > div.cerana-button > input:nth-child(2)').element.value).toBe('Supprimer')
		expect(wrapper.findAll('#subscription > div > fieldset:nth-child(3) > tabs-stub > tab-stub').length).toBe(2)
		expect(wrapper.find('#subscription > compconfirm-stub').exists()).toBe(false)
		expect(wrapper.emitted('update')).toStrictEqual([[true]])

		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(1)
		expect(axios.get).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledWith('/apps/cerana_adhesion/subscriptions/111')
		expect(console.error).toHaveBeenCalledTimes(0)
	})

})
