'use strict'

import { nextTick } from 'vue'
import { shallowMount } from '@vue/test-utils'
import { getSimpleObj, getSimpleFieldsj, initMockAxios } from './functions.js'
import { ceranaI18n, ceranaStore } from '../../src/cerana_lib'
import CompShowGeneric from '../../src/CompShowGeneric'
import axios from 'axios'
const $ = require('jquery')

jest.mock('axios')
const specialOpt = [{ name: 'aaa', value: 1 }, { name: 'bbb', value: 2 }, { name: 'ccc', value: 3 }]

beforeEach(() => {
	document.documentElement.innerHTML = '<html><body></body></html>'
	global.console.error = jest.fn()
	global.console.warn = jest.fn()
})

describe('Show Generic Component', () => {
	it('Empty', async () => {
		initMockAxios()
		const wrapper = shallowMount(CompShowGeneric, {
			ceranaI18n,
			ceranaStore,
			propsData: {},
		})
		while (wrapper.vm.loading) {
			await nextTick()
		}
		expect(wrapper.html()).toBe('')
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(0)
		expect(console.error).toHaveBeenCalledTimes(0)
	})

	it('Static', async () => {
		initMockAxios()
		const objVal = getSimpleObj(987)
		const wrapper = shallowMount(CompShowGeneric, {
			ceranaI18n,
			ceranaStore,
			propsData: {
				object: objVal,
				itemid: 1,
				fields: getSimpleFieldsj(specialOpt),
			},
		})
		while (wrapper.vm.loading) {
			await nextTick()
		}
		await nextTick()
		expect(console.error).toHaveBeenCalledTimes(0)
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(0)

		expect(wrapper.find('div').exists()).toBe(true)
		expect(wrapper.findAll('div > div').length).toBe(7)
		expect(wrapper.find('div > div:nth-child(1)').attributes('name')).toBe('name')
		expect(wrapper.find('div > div:nth-child(2)').attributes('name')).toBe('value')
		expect(wrapper.find('div > div:nth-child(3)').attributes('name')).toBe('birth')
		expect(wrapper.find('div > div:nth-child(4)').attributes('name')).toBe('mail')
		expect(wrapper.find('div > div:nth-child(5)').attributes('name')).toBe('link')
		expect(wrapper.find('div > div:nth-child(6)').attributes('name')).toBe('comment')
		expect(wrapper.find('div > div:nth-child(7)').attributes('name')).toBe('image')

		expect(wrapper.find('div > div:nth-child(1) > label').text()).toBe('Name')
		expect(wrapper.find('div > div:nth-child(2) > label').text()).toBe('Value')
		expect(wrapper.find('div > div:nth-child(3) > label').text()).toBe('Birthday')
		expect(wrapper.find('div > div:nth-child(4) > label').text()).toBe('Email')
		expect(wrapper.find('div > div:nth-child(5) > label').text()).toBe('URL')
		expect(wrapper.find('div > div:nth-child(6) > label').text()).toBe('Comment')
		expect(wrapper.find('div > div:nth-child(7) > label').text()).toBe('Image')

		expect(wrapper.find('div > div:nth-child(1) > span > span').text()).toBe('foo')
		expect(wrapper.find('div > div:nth-child(2) > span > span').text()).toBe('bbb')
		expect(wrapper.find('div > div:nth-child(3) > span > span').text()).toBe('21/07/1983')
		expect(wrapper.find('div > div:nth-child(4) > span > a').text()).toBe('truc@machin.org')
		expect(wrapper.find('div > div:nth-child(4) > span > a').attributes('href')).toBe('mailto:truc@machin.org')
		expect(wrapper.find('div > div:nth-child(5) > span > a').text()).toBe('https://www.machin.org')
		expect(wrapper.find('div > div:nth-child(5) > span > a').attributes('href')).toBe('https://www.machin.org')
		expect(wrapper.find('div > div:nth-child(5) > span > a').attributes('target')).toBe('_blank')
		expect(wrapper.find('div > div:nth-child(6) > span > pre').text()).toBe('aaa\nbbbb\nccc')
		expect(wrapper.find('div > div:nth-child(7) > span > img').attributes('src')).toBe('/apps/cerana_adhesion/img/empty.png')

		expect(wrapper.emitted('current')).toStrictEqual([[objVal]])
		expect($('div.toastify').length).toBe(0)
	})

	it('Static image', async () => {
		initMockAxios()
		const objVal = getSimpleObj(987)
		objVal.image = 'data:*/*;base64,/9j/4AAQSkZJRgABAQEBLAEsAAD/4QvKRXhpZgAASUkqAAgAAAAGABoBBQABAAAAVgAAABsBBQAB'
		const wrapper = shallowMount(CompShowGeneric, {
			ceranaI18n,
			ceranaStore,
			propsData: {
				object: objVal,
				itemid: 1,
				fields: getSimpleFieldsj(specialOpt),
			},
		})
		while (wrapper.vm.loading) {
			await nextTick()
		}
		await nextTick()
		expect(wrapper.find('div').exists()).toBe(true)
		expect(wrapper.findAll('div > div').length).toBe(7)
		expect(wrapper.find('div > div:nth-child(7)').attributes('name')).toBe('image')
		expect(wrapper.find('div > div:nth-child(7) > label').text()).toBe('Image')
		expect(wrapper.find('div > div:nth-child(7) > span > img').attributes('src')).toBe(objVal.image)

		expect(console.error).toHaveBeenCalledTimes(0)
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(0)

		expect(wrapper.emitted('current')).toStrictEqual([[objVal]])
		expect($('div.toastify').length).toBe(0)
	})

	it('Static bad date', async () => {
		initMockAxios()
		const objVal = getSimpleObj(987)
		objVal.birth = '18 juin 1944'
		const wrapper = shallowMount(CompShowGeneric, {
			ceranaI18n,
			ceranaStore,
			propsData: {
				object: objVal,
				itemid: 1,
				fields: getSimpleFieldsj(specialOpt),
			},
		})
		while (wrapper.vm.loading) {
			await nextTick()
		}
		await nextTick()
		expect(wrapper.find('div').exists()).toBe(true)
		expect(wrapper.findAll('div > div').length).toBe(7)
		expect(wrapper.find('div > div:nth-child(3)').attributes('name')).toBe('birth')
		expect(wrapper.find('div > div:nth-child(3) > label').text()).toBe('Birthday')
		expect(wrapper.find('div > div:nth-child(3) > span > span').text()).toBe('18 juin 1944')

		expect(console.error).toHaveBeenCalledTimes(1)
		expect(console.error).toHaveBeenCalledWith('Invalid Date')
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(0)

		expect(wrapper.emitted('current')).toStrictEqual([[objVal]])
		expect($('div.toastify').length).toBe(0)
	})

	it('Simple', async () => {
		initMockAxios()
		const wrapper = shallowMount(CompShowGeneric, {
			ceranaI18n,
			ceranaStore,
			propsData: {
				models: 'simple',
				itemid: 123,
				fields: getSimpleFieldsj(specialOpt),
			},
		})
		while (wrapper.vm.loading) {
			await nextTick()
		}
		await nextTick()
		expect(console.error).toHaveBeenCalledTimes(0)
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(1)
		expect(axios.get).toHaveBeenCalledWith('/apps/cerana_adhesion/simple/123')

		expect(wrapper.find('div').exists()).toBe(true)
		expect(wrapper.findAll('div > div').length).toBe(7)
		expect(wrapper.find('div > div:nth-child(1)').attributes('name')).toBe('name')
		expect(wrapper.find('div > div:nth-child(2)').attributes('name')).toBe('value')
		expect(wrapper.find('div > div:nth-child(3)').attributes('name')).toBe('birth')
		expect(wrapper.find('div > div:nth-child(4)').attributes('name')).toBe('mail')
		expect(wrapper.find('div > div:nth-child(5)').attributes('name')).toBe('link')
		expect(wrapper.find('div > div:nth-child(6)').attributes('name')).toBe('comment')
		expect(wrapper.find('div > div:nth-child(7)').attributes('name')).toBe('image')

		expect(wrapper.find('div > div:nth-child(1) > label').text()).toBe('Name')
		expect(wrapper.find('div > div:nth-child(2) > label').text()).toBe('Value')
		expect(wrapper.find('div > div:nth-child(3) > label').text()).toBe('Birthday')
		expect(wrapper.find('div > div:nth-child(4) > label').text()).toBe('Email')
		expect(wrapper.find('div > div:nth-child(5) > label').text()).toBe('URL')
		expect(wrapper.find('div > div:nth-child(6) > label').text()).toBe('Comment')
		expect(wrapper.find('div > div:nth-child(7) > label').text()).toBe('Image')

		expect(wrapper.find('div > div:nth-child(1) > span > span').text()).toBe('foo')
		expect(wrapper.find('div > div:nth-child(2) > span > span').text()).toBe('bbb')
		expect(wrapper.find('div > div:nth-child(3) > span > span').text()).toBe('21/07/1983')
		expect(wrapper.find('div > div:nth-child(4) > span > a').text()).toBe('truc@machin.org')
		expect(wrapper.find('div > div:nth-child(4) > span > a').attributes('href')).toBe('mailto:truc@machin.org')
		expect(wrapper.find('div > div:nth-child(5) > span > a').text()).toBe('https://www.machin.org')
		expect(wrapper.find('div > div:nth-child(5) > span > a').attributes('href')).toBe('https://www.machin.org')
		expect(wrapper.find('div > div:nth-child(5) > span > a').attributes('target')).toBe('_blank')
		expect(wrapper.find('div > div:nth-child(6) > span > pre').text()).toBe('aaa\nbbbb\nccc')
		expect(wrapper.find('div > div:nth-child(7) > span > img').attributes('src')).toBe('/apps/cerana_adhesion/img/empty.png')
		expect($('div.toastify').length).toBe(0)
	})

	it('Search', async () => {
		initMockAxios()
		const wrapper = shallowMount(CompShowGeneric, {
			ceranaI18n,
			ceranaStore,
			propsData: {
				models: 'simple',
				itemid: 123,
				fields: getSimpleFieldsj(specialOpt),
				search: { val1: 'aaa', val2: 456 },
			},
		})
		while (wrapper.vm.loading) {
			await nextTick()
		}
		await nextTick()
		expect(console.error).toHaveBeenCalledTimes(0)
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(1)
		expect(axios.get).toHaveBeenCalledWith('/apps/cerana_adhesion/simple/123?val1=aaa&val2=456')

		expect(wrapper.find('div').exists()).toBe(true)
		expect(wrapper.findAll('div > div').length).toBe(7)
		expect($('div.toastify').length).toBe(0)
	})

	it('Not exist', async () => {
		initMockAxios()
		const wrapper = shallowMount(CompShowGeneric, {
			ceranaI18n,
			ceranaStore,
			propsData: {
				models: 'simple',
				itemid: 666,
				fields: getSimpleFieldsj(specialOpt)
			},
		})
		while (wrapper.vm.loading) {
			await nextTick()
		}
		await nextTick()

		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(1)
		expect(axios.get).toHaveBeenCalledWith('/apps/cerana_adhesion/simple/666')
		expect(wrapper.find('div').exists()).toBe(false)
		expect($('div.toastify').length).toBe(1)
		expect(console.error).toHaveBeenCalledTimes(1)
		expect(console.error).toHaveBeenCalledWith({ response: { data: 'not exist!', status: 404 } })
	})

	it('Not exist and ignored', async () => {
		initMockAxios()
		const wrapper = shallowMount(CompShowGeneric, {
			ceranaI18n,
			ceranaStore,
			propsData: {
				models: 'simple',
				itemid: 666,
				ignoreid: 666,
				fields: getSimpleFieldsj(specialOpt)
			},
		})
		while (wrapper.vm.loading) {
			await nextTick()
		}
		await nextTick()

		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(1)
		expect(axios.get).toHaveBeenCalledWith('/apps/cerana_adhesion/simple/666')
		expect(wrapper.find('div').exists()).toBe(false)
		expect($('div.toastify').length).toBe(0)
		expect(console.error).toHaveBeenCalledTimes(0)
	})

	it('bad model', async () => {
		initMockAxios()
		const wrapper = shallowMount(CompShowGeneric, {
			ceranaI18n,
			ceranaStore,
			propsData: {
				models: 'bad',
				itemid: 123,
				fields: getSimpleFieldsj(specialOpt)
			},
		})
		while (wrapper.vm.loading) {
			await nextTick()
		}
		await nextTick()

		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(1)
		expect(axios.get).toHaveBeenCalledWith('/apps/cerana_adhesion/bad/123')
		expect(wrapper.find('div').exists()).toBe(false)
		expect($('div.toastify').length).toBe(1)
		expect(console.error).toHaveBeenCalledTimes(1)
		expect(console.error).toHaveBeenCalledWith({ response: { data: 'no address!', status: 404 } })
	})
})
