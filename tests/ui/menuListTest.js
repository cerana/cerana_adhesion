'use strict'

import { nextTick } from 'vue'
import { shallowMount } from '@vue/test-utils'
import { initMockAxios } from './functions.js'
import { ceranaI18n, ceranaStore } from '../../src/cerana_lib.js'
import axios from 'axios'
import MenuList from '../../src/MenuList'

jest.mock('axios')

beforeEach(() => {
	document.documentElement.innerHTML = '<html><body></body></html>'
	global.console.error = jest.fn()
	global.console.warn = jest.fn()
})

describe('Menu List', () => {
	it('Show list', async () => {
		ceranaStore.commit('clean')
		initMockAxios()
		const wrapper = shallowMount(MenuList, {
			ceranaI18n,
			ceranaStore,
			propsData: {},
		})
		while (wrapper.vm.loading) {
			await nextTick()
		}
		expect(wrapper.find('#listadherent > h1').text()).toBe('Les adhérents')
		expect(wrapper.find('#listadherent > div > div.currentfile').exists()).toBe(false)
		expect(wrapper.find('#listadherent > div > complistgeneric-stub').props().models).toBe('adherents')
		expect(wrapper.find('#listadherent > div > complistgeneric-stub').props().showoption).toBe('2')
		expect(wrapper.find('#listadherent > div > complistgeneric-stub').props().addoption).toBe('1')
		expect(wrapper.find('#listadherent > div > complistgeneric-stub').props().editoption).toBe('1')
		expect(wrapper.find('#listadherent > div > complistgeneric-stub').props().deleteoption).toBe('1')
		expect(wrapper.find('#listadherent > div > complistgeneric-stub').props().searching).toStrictEqual({})
		expect(console.error).toHaveBeenCalledTimes(0)
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(5)
	})

	it('Show selected', async () => {
		ceranaStore.commit('clean')
		initMockAxios()
		const wrapper = shallowMount(MenuList, {
			ceranaI18n,
			ceranaStore,
			propsData: {},
		})
		while (wrapper.vm.loading) {
			await nextTick()
		}
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(5)
		await wrapper.vm.showAdherent({ id: 123, firstname: 'Charles', lastname: 'Attand' })
		await nextTick()
		expect(wrapper.find('#listadherent > h1').text()).toBe('Les adhérents')
		expect(wrapper.find('#listadherent > div > div.currentfile').exists()).toBe(true)
		expect(wrapper.find('#listadherent > div > div.currentfile > h1').text()).toBe('Fiche de Charles Attand')
		expect(wrapper.find('#listadherent > div > div.currentfile > input').element.value).toBe('Retour')
		expect(wrapper.find('#listadherent > div > div.currentfile > compadherent-stub').props().initadherentid).toBe(123)
		expect(wrapper.find('#listadherent > div > complistgeneric-stub').exists()).toBe(false)
		await wrapper.find('#listadherent > div > div.currentfile > input').trigger('click')
		await nextTick()
		expect(wrapper.find('#listadherent > div > div.currentfile').exists()).toBe(false)
		expect(wrapper.find('#listadherent > div > complistgeneric-stub').exists()).toBe(true)
	})
})