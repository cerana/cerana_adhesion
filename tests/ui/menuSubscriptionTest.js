'use strict'

import { nextTick } from 'vue'
import { shallowMount } from '@vue/test-utils'
import { initMockAxios } from './functions.js'
import axios from 'axios'
import MenuSubscription from '../../src/MenuSubscription'
import { ceranaI18n, ceranaStore } from '../../src/cerana_lib.js'

jest.mock('axios')

beforeEach(() => {
	document.documentElement.innerHTML = '<html><body></body></html>'
	global.console.error = jest.fn()
	global.console.warn = jest.fn()
})

describe('Menu Subscription', () => {
	it('Show default', async () => {
		ceranaStore.commit('clean')
		initMockAxios()
		const wrapper = shallowMount(MenuSubscription, {
			ceranaI18n,
			ceranaStore,
			propsData: {},
		})
		while (wrapper.vm.loading) {
			await nextTick()
		}
		await nextTick()
		expect(wrapper.find('div > h1').text()).toBe('Cotisations')
		expect(wrapper.findAll('div > tabs-stub > tab-stub').length).toBe(3)
		expect(wrapper.find('div > tabs-stub > tab-stub:nth-child(1)').attributes('name')).toBe('Types de cotisations')
		expect(wrapper.find('div > tabs-stub > tab-stub:nth-child(1) > complistgeneric-stub').props().models).toBe('type_adhesions')
		expect(wrapper.find('div > tabs-stub > tab-stub:nth-child(1) > complistgeneric-stub').props().fields).toStrictEqual([
			{ id: 'name', value: 'Nom', type: 'text', needed: true, default: '' },
		])
		expect(wrapper.find('div > tabs-stub > tab-stub:nth-child(2)').attributes('name')).toBe('Types de responsabilités')
		expect(wrapper.find('div > tabs-stub > tab-stub:nth-child(2) > complistgeneric-stub').props().models).toBe('type_responsabilitys')
		expect(wrapper.find('div > tabs-stub > tab-stub:nth-child(2) > complistgeneric-stub').props().fields[0].id).toBe('level')
		expect(wrapper.find('div > tabs-stub > tab-stub:nth-child(2) > complistgeneric-stub').props().fields[0].type).toBe('select')
		expect(wrapper.find('div > tabs-stub > tab-stub:nth-child(2) > complistgeneric-stub').props().fields[1].id).toBe('name')
		expect(wrapper.find('div > tabs-stub > tab-stub:nth-child(2) > complistgeneric-stub').props().fields[1].type).toBe('text')
		expect(wrapper.find('div > tabs-stub > tab-stub:nth-child(3)').attributes('name')).toBe('Types de pratiques')
		expect(wrapper.find('div > tabs-stub > tab-stub:nth-child(3) > complistgeneric-stub').props().models).toBe('type_practices')
		expect(wrapper.find('div > tabs-stub > tab-stub:nth-child(3) > complistgeneric-stub').props().fields).toStrictEqual([
			{ id: 'name', value: 'Nom', type: 'text', needed: true, default: '' },
		])
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(5)
	})

	it('change', async () => {
		ceranaStore.commit('clean')
		initMockAxios()
		const wrapper = shallowMount(MenuSubscription, {
			ceranaI18n,
			ceranaStore,
			propsData: {},
		})
		 while (wrapper.vm.loading) {
			await nextTick()
		}
		expect(axios.get).toHaveBeenCalledTimes(5)

		await wrapper.vm.tabchange({ tab: 'typeadhesions' })
		await nextTick()
		expect(axios.get).toHaveBeenCalledTimes(5)

		await wrapper.vm.updatedTypeAdhesions(true)
		while (wrapper.vm.loading) {
			await nextTick()
		}
		expect(axios.get).toHaveBeenCalledTimes(6)
		await wrapper.vm.updatedResponsabilitys(true)
		while (wrapper.vm.loading) {
			await nextTick()
		}
		expect(axios.get).toHaveBeenCalledTimes(7)

		await wrapper.vm.updatedPractices(true)
		while (wrapper.vm.loading) {
			await nextTick()
		}
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(8)
	})

})
