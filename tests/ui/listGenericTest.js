'use strict'

import { nextTick } from 'vue'
import { shallowMount } from '@vue/test-utils'
import { setSelectValue, setInputValue, getSimpleObj, getSimpleFieldsj, initMockAxios } from './functions.js'
import { ceranaI18n, ceranaStore } from '../../src/cerana_lib'
import CompListGeneric from '../../src/CompListGeneric'
import axios from 'axios'

jest.mock('axios')
const specialOpt = [{ name: 'aaa', value: 1 }, { name: 'bbb', value: 2 }, { name: 'ccc', value: 3 }]

beforeEach(() => {
	document.documentElement.innerHTML = '<html><body></body></html>'
	global.console.error = jest.fn()
	global.console.warn = jest.fn()
})

describe('List Generic Component', () => {
	it('Empty', async () => {
		initMockAxios()
		const wrapper = shallowMount(CompListGeneric, {
			ceranaI18n,
			ceranaStore,
			propsData: {},
		})
		while (wrapper.vm.loading) {
			await nextTick()
		}
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(0)
		expect(console.error).toHaveBeenCalledTimes(0)
		expect(wrapper.find('div.list').exists()).toBe(true)
		expect(wrapper.find('div.list > input.iconadd').exists()).toBe(false)
		expect(wrapper.find('div.list > div.navsearch').exists()).toBe(false)
		expect(wrapper.find('div.list > div.navlist').exists()).toBe(false)
		expect(wrapper.findAll('div.list > table > thead > tr > th').length).toBe(1)
		expect(wrapper.findAll('div.list > table > tbody > tr > td').length).toBe(0)
		expect(wrapper.find('div.list > CompConfirm-Stub').exists()).toBe(false)
		expect(wrapper.find('div.list > CompEdit-Stub').exists()).toBe(false)
		expect(wrapper.find('div.list > Modal-Stub').exists()).toBe(false)
	})

	it('Static', async () => {
		initMockAxios()
		const objVal = getSimpleObj(987)
		const wrapper = shallowMount(CompListGeneric, {
			ceranaI18n,
			ceranaStore,
			propsData: {
				objects: [objVal],
				addoption: '0',
				showoption: '0',
				editoption: '0',
				deleteoption: '0',
				fields: getSimpleFieldsj(specialOpt),
			},
		})
		while (wrapper.vm.loading) {
			await nextTick()
		}
		await nextTick()
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(0)
		expect(console.error).toHaveBeenCalledTimes(0)
		expect(wrapper.find('div.list').exists()).toBe(true)
		expect(wrapper.find('div.list > input.iconadd').exists()).toBe(false)
		expect(wrapper.find('div.list > div.navsearch').exists()).toBe(false)
		expect(wrapper.find('div.list > div.navlist').exists()).toBe(false)
		expect(wrapper.findAll('div.list > table > thead > tr > th').length).toBe(7)
		expect(wrapper.find('div.list > table > thead > tr > th:nth-child(1)').text()).toBe('Name')
		expect(wrapper.find('div.list > table > thead > tr > th:nth-child(2)').text()).toBe('Value')
		expect(wrapper.find('div.list > table > thead > tr > th:nth-child(3)').text()).toBe('Birthday')
		expect(wrapper.find('div.list > table > thead > tr > th:nth-child(4)').text()).toBe('Email')
		expect(wrapper.find('div.list > table > thead > tr > th:nth-child(5)').text()).toBe('URL')
		expect(wrapper.find('div.list > table > thead > tr > th:nth-child(6)').text()).toBe('Comment')
		expect(wrapper.find('div.list > table > thead > tr > th:nth-child(7)').text()).toBe('Hide')
		expect(wrapper.findAll('div.list > table > tbody > tr').length).toBe(1)
		expect(wrapper.findAll('div.list > table > tbody > tr:nth-child(1) > td').length).toBe(7)
		expect(wrapper.find('div.list > table > tbody > tr:nth-child(1) > td:nth-child(1) > span').text()).toBe('foo')
		expect(wrapper.find('div.list > table > tbody > tr:nth-child(1) > td:nth-child(2) > span').text()).toBe('bbb')
		expect(wrapper.find('div.list > table > tbody > tr:nth-child(1) > td:nth-child(3) > span').text()).toBe('21/07/1983')
		expect(wrapper.find('div.list > table > tbody > tr:nth-child(1) > td:nth-child(4) > a').text()).toBe('truc@machin.org')
		expect(wrapper.find('div.list > table > tbody > tr:nth-child(1) > td:nth-child(4) > a').attributes('href')).toBe('mailto:truc@machin.org')
		expect(wrapper.find('div.list > table > tbody > tr:nth-child(1) > td:nth-child(5) > a').text()).toBe('https://www.machin.org')
		expect(wrapper.find('div.list > table > tbody > tr:nth-child(1) > td:nth-child(5) > a').attributes('href')).toBe('https://www.machin.org')
		expect(wrapper.find('div.list > table > tbody > tr:nth-child(1) > td:nth-child(5) > a').attributes('target')).toBe('_blank')
		expect(wrapper.find('div.list > table > tbody > tr:nth-child(1) > td:nth-child(6) > pre').text()).toBe('aaa\nbbbb\nccc')
		expect(wrapper.find('div.list > table > tbody > tr:nth-child(1) > td:nth-child(7) > span').text()).toBe('uvw')

		expect(wrapper.find('div.list > CompConfirm-Stub').exists()).toBe(false)
		expect(wrapper.find('div.list > CompEdit-Stub').exists()).toBe(false)
		expect(wrapper.find('div.list > Modal-Stub').exists()).toBe(false)
	})

	it('Simple', async () => {
		initMockAxios()
		const wrapper = shallowMount(CompListGeneric, {
			ceranaI18n,
			ceranaStore,
			propsData: {
				models: 'simple',
				fields: getSimpleFieldsj(specialOpt),
			},
		})
		while (wrapper.vm.loading) {
			await nextTick()
		}
		await nextTick()
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(1)
		expect(axios.get).toHaveBeenCalledWith('/apps/cerana_adhesion/simple?page=0&size=25')
		expect(console.error).toHaveBeenCalledTimes(0)
		expect(wrapper.find('div.list').exists()).toBe(true)
		expect(wrapper.find('div.list > input.iconadd').exists()).toBe(true)
		expect(wrapper.find('div.list > div.navsearch').exists()).toBe(false)
		expect(wrapper.find('div.list > div.navlist').exists()).toBe(true)
		expect(wrapper.findAll('div.list > table > thead > tr > th').length).toBe(8)
		expect(wrapper.find('div.list > table > thead > tr > th:nth-child(1)').text()).toBe('Name')
		expect(wrapper.find('div.list > table > thead > tr > th:nth-child(2)').text()).toBe('Value')
		expect(wrapper.find('div.list > table > thead > tr > th:nth-child(3)').text()).toBe('Birthday')
		expect(wrapper.find('div.list > table > thead > tr > th:nth-child(4)').text()).toBe('Email')
		expect(wrapper.find('div.list > table > thead > tr > th:nth-child(5)').text()).toBe('URL')
		expect(wrapper.find('div.list > table > thead > tr > th:nth-child(6)').text()).toBe('Comment')
		expect(wrapper.find('div.list > table > thead > tr > th:nth-child(7)').text()).toBe('Hide')
		expect(wrapper.find('div.list > table > thead > tr > th:nth-child(8)').text()).toBe('')
		expect(wrapper.findAll('div.list > table > tbody > tr').length).toBe(1)
		expect(wrapper.findAll('div.list > table > tbody > tr:nth-child(1) > td').length).toBe(8)
		expect(wrapper.find('div.list > table > tbody > tr:nth-child(1) > td:nth-child(1) > span').text()).toBe('foo')
		expect(wrapper.find('div.list > table > tbody > tr:nth-child(1) > td:nth-child(2) > span').text()).toBe('bbb')
		expect(wrapper.find('div.list > table > tbody > tr:nth-child(1) > td:nth-child(3) > span').text()).toBe('21/07/1983')
		expect(wrapper.find('div.list > table > tbody > tr:nth-child(1) > td:nth-child(4) > a').text()).toBe('truc@machin.org')
		expect(wrapper.find('div.list > table > tbody > tr:nth-child(1) > td:nth-child(4) > a').attributes('href')).toBe('mailto:truc@machin.org')
		expect(wrapper.find('div.list > table > tbody > tr:nth-child(1) > td:nth-child(5) > a').text()).toBe('https://www.machin.org')
		expect(wrapper.find('div.list > table > tbody > tr:nth-child(1) > td:nth-child(5) > a').attributes('href')).toBe('https://www.machin.org')
		expect(wrapper.find('div.list > table > tbody > tr:nth-child(1) > td:nth-child(5) > a').attributes('target')).toBe('_blank')
		expect(wrapper.find('div.list > table > tbody > tr:nth-child(1) > td:nth-child(6) > pre').text()).toBe('aaa\nbbbb\nccc')
		expect(wrapper.find('div.list > table > tbody > tr:nth-child(1) > td:nth-child(7) > span').text()).toBe('uvw')
		expect(wrapper.findAll('div.list > table > tbody > tr:nth-child(1) > td:nth-child(8) > actions-stub').length).toBe(2)
		expect(wrapper.find('div.list > table > tbody > tr:nth-child(1) > td:nth-child(8) > actions-stub:nth-child(1) > actionbutton-stub').text()).toBe('Éditer')
		expect(wrapper.find('div.list > table > tbody > tr:nth-child(1) > td:nth-child(8) > actions-stub:nth-child(2) > actionbutton-stub').text()).toBe('Supprimer')

		expect(wrapper.find('div.list > CompConfirm-Stub').exists()).toBe(false)
		expect(wrapper.find('div.list > CompEditGeneric-Stub').exists()).toBe(false)
		expect(wrapper.find('div.list > Modal-Stub').exists()).toBe(false)
	})

	it('Simple edit', async () => {
		initMockAxios()
		const wrapper = shallowMount(CompListGeneric, {
			ceranaI18n,
			ceranaStore,
			propsData: {
				models: 'simple',
				fields: getSimpleFieldsj(specialOpt),
			},
		})
		while (wrapper.vm.loading) {
			await nextTick()
		}
		await nextTick()
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(1)
		expect(axios.get).toHaveBeenCalledWith('/apps/cerana_adhesion/simple?page=0&size=25')
		expect(console.error).toHaveBeenCalledTimes(0)
		expect(wrapper.find('div.list > CompConfirm-Stub').exists()).toBe(false)
		expect(wrapper.find('div.list > CompEditGeneric-Stub').exists()).toBe(false)
		expect(wrapper.find('div.list > Modal-Stub').exists()).toBe(false)
		expect(wrapper.findAll('div.list > table > tbody > tr').length).toBe(1)
		await wrapper.vm.editItem(wrapper.vm.items[0])
		await nextTick()
		await nextTick()
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(1)
		expect(console.error).toHaveBeenCalledTimes(0)
		expect(wrapper.emitted('edititem')).toStrictEqual(undefined)
		expect(wrapper.find('div.list > CompConfirm-Stub').exists()).toBe(false)
		expect(wrapper.find('div.list > CompEditGeneric-Stub').exists()).toBe(true)
		expect(wrapper.find('div.list > CompEditGeneric-Stub').props().models).toBe('simple')
		expect(wrapper.find('div.list > CompEditGeneric-Stub').props().itemid).toBe(123)
		expect(wrapper.find('div.list > CompEditGeneric-Stub').props().fields).toStrictEqual(getSimpleFieldsj(specialOpt))
		expect(wrapper.find('div.list > Modal-Stub').exists()).toBe(false)
	})

	it('Simple edit nogui', async () => {
		initMockAxios()
		const wrapper = shallowMount(CompListGeneric, {
			ceranaI18n,
			ceranaStore,
			propsData: {
				models: 'simple',
				editoption: '2',
				fields: getSimpleFieldsj(specialOpt),
			},
		})
		while (wrapper.vm.loading) {
			await nextTick()
		}
		await nextTick()
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(1)
		expect(axios.get).toHaveBeenCalledWith('/apps/cerana_adhesion/simple?page=0&size=25')
		expect(console.error).toHaveBeenCalledTimes(0)
		expect(wrapper.find('div.list > CompConfirm-Stub').exists()).toBe(false)
		expect(wrapper.find('div.list > CompEditGeneric-Stub').exists()).toBe(false)
		expect(wrapper.find('div.list > Modal-Stub').exists()).toBe(false)
		expect(wrapper.findAll('div.list > table > tbody > tr').length).toBe(1)
		await wrapper.vm.editItem(wrapper.vm.items[0])
		await nextTick()
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(1)
		expect(console.error).toHaveBeenCalledTimes(0)
		expect(wrapper.emitted('edititem')).toStrictEqual([[wrapper.vm.items[0]]])
		expect(wrapper.find('div.list > CompConfirm-Stub').exists()).toBe(false)
		expect(wrapper.find('div.list > CompEditGeneric-Stub').exists()).toBe(false)
		expect(wrapper.find('div.list > Modal-Stub').exists()).toBe(false)
	})

	it('Simple delete', async () => {
		initMockAxios()
		const wrapper = shallowMount(CompListGeneric, {
			ceranaI18n,
			ceranaStore,
			propsData: {
				models: 'simple',
				fields: getSimpleFieldsj(specialOpt),
			},
		})
		while (wrapper.vm.loading) {
			await nextTick()
		}
		await nextTick()
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(1)
		expect(axios.get).toHaveBeenCalledWith('/apps/cerana_adhesion/simple?page=0&size=25')
		expect(console.error).toHaveBeenCalledTimes(0)
		expect(wrapper.find('div.list > CompConfirm-Stub').exists()).toBe(false)
		expect(wrapper.find('div.list > CompEditGeneric-Stub').exists()).toBe(false)
		expect(wrapper.find('div.list > Modal-Stub').exists()).toBe(false)
		expect(wrapper.findAll('div.list > table > tbody > tr').length).toBe(1)
		await wrapper.vm.wantDelete(wrapper.vm.items[0])
		await nextTick()
		await nextTick()
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(1)
		expect(console.error).toHaveBeenCalledTimes(0)
		expect(wrapper.emitted('deleteitem')).toStrictEqual(undefined)
		expect(wrapper.find('div.list > CompConfirm-Stub').exists()).toBe(true)
		expect(wrapper.find('div.list > CompConfirm-Stub').props().message).toBe('Voulez-vous supprimer cet élément ?')
		expect(wrapper.find('div.list > CompEditGeneric-Stub').exists()).toBe(false)
		expect(wrapper.find('div.list > Modal-Stub').exists()).toBe(false)

		await wrapper.vm.confirmDelete(true)
		while (wrapper.vm.loading) {
			await nextTick()
		}
		await nextTick()
		while (wrapper.vm.loading) {
			await nextTick()
		}
		await nextTick()
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(1)
		expect(axios.get).toHaveBeenCalledTimes(2)
		expect(axios.delete).toHaveBeenCalledWith('/apps/cerana_adhesion/simple/123')
		expect(console.error).toHaveBeenCalledTimes(0)
		expect(wrapper.find('div.list > CompConfirm-Stub').exists()).toBe(false)
		expect(wrapper.find('div.list > CompEditGeneric-Stub').exists()).toBe(false)
		expect(wrapper.find('div.list > Modal-Stub').exists()).toBe(false)
		expect(wrapper.emitted('updated')).toStrictEqual([[true]])
	})

	it('Simple delete nogui', async () => {
		initMockAxios()
		const wrapper = shallowMount(CompListGeneric, {
			ceranaI18n,
			ceranaStore,
			propsData: {
				models: 'simple',
				deleteoption: '2',
				fields: getSimpleFieldsj(specialOpt),
			},
		})
		while (wrapper.vm.loading) {
			await nextTick()
		}
		await nextTick()
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(1)
		expect(axios.get).toHaveBeenCalledWith('/apps/cerana_adhesion/simple?page=0&size=25')
		expect(console.error).toHaveBeenCalledTimes(0)
		expect(wrapper.find('div.list > CompConfirm-Stub').exists()).toBe(false)
		expect(wrapper.find('div.list > CompEditGeneric-Stub').exists()).toBe(false)
		expect(wrapper.find('div.list > Modal-Stub').exists()).toBe(false)
		expect(wrapper.findAll('div.list > table > tbody > tr').length).toBe(1)
		await wrapper.vm.wantDelete(wrapper.vm.items[0])
		await nextTick()
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(1)
		expect(console.error).toHaveBeenCalledTimes(0)
		expect(wrapper.emitted('deleteitem')).toStrictEqual([[wrapper.vm.items[0]]])
		expect(wrapper.find('div.list > CompConfirm-Stub').exists()).toBe(false)
		expect(wrapper.find('div.list > Modal-Stub').exists()).toBe(false)
	})

	it('Simple new', async () => {
		initMockAxios()
		const wrapper = shallowMount(CompListGeneric, {
			ceranaI18n,
			ceranaStore,
			propsData: {
				models: 'simple',
				fields: getSimpleFieldsj(specialOpt),
			},
		})
		while (wrapper.vm.loading) {
			await nextTick()
		}
		await nextTick()
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(1)
		expect(console.error).toHaveBeenCalledTimes(0)
		expect(wrapper.find('div.list > input.iconadd').exists()).toBe(true)
		expect(wrapper.find('div.list > CompConfirm-Stub').exists()).toBe(false)
		expect(wrapper.find('div.list > CompEditGeneric-Stub').exists()).toBe(false)
		expect(wrapper.find('div.list > Modal-Stub').exists()).toBe(false)
		expect(wrapper.findAll('div.list > table > tbody > tr').length).toBe(1)

		await wrapper.find('div.list > input.iconadd').trigger('click')
		await nextTick()
		await nextTick()
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(1)
		expect(console.error).toHaveBeenCalledTimes(0)
		expect(wrapper.emitted('edititem')).toStrictEqual(undefined)
		expect(wrapper.find('div.list > CompConfirm-Stub').exists()).toBe(false)
		expect(wrapper.find('div.list > CompEditGeneric-Stub').exists()).toBe(true)
		expect(wrapper.find('div.list > CompEditGeneric-Stub').props().models).toBe('simple')
		expect(wrapper.find('div.list > CompEditGeneric-Stub').props().itemid).toBe(-1)
		expect(wrapper.find('div.list > CompEditGeneric-Stub').props().fields).toStrictEqual(getSimpleFieldsj(specialOpt))
		expect(wrapper.find('div.list > Modal-Stub').exists()).toBe(false)
	})

	it('Simple onlyshow', async () => {
		initMockAxios()
		const wrapper = shallowMount(CompListGeneric, {
			ceranaI18n,
			ceranaStore,
			propsData: {
				models: 'simple',
				addoption: '0',
				showoption: '1',
				editoption: '0',
				deleteoption: '0',
				fields: getSimpleFieldsj(specialOpt),
			},
		})
		while (wrapper.vm.loading) {
			await nextTick()
		}
		await nextTick()
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(1)
		expect(axios.get).toHaveBeenCalledWith('/apps/cerana_adhesion/simple?page=0&size=25')
		expect(console.error).toHaveBeenCalledTimes(0)
		expect(wrapper.find('div.list').exists()).toBe(true)
		expect(wrapper.find('div.list > input.iconadd').exists()).toBe(false)
		expect(wrapper.find('div.list > div.navsearch').exists()).toBe(false)
		expect(wrapper.find('div.list > div.navlist').exists()).toBe(true)
		expect(wrapper.findAll('div.list > table > thead > tr > th').length).toBe(8)
		expect(wrapper.findAll('div.list > table > tbody > tr').length).toBe(1)
		expect(wrapper.findAll('div.list > table > tbody > tr:nth-child(1) > td').length).toBe(8)
		expect(wrapper.findAll('div.list > table > tbody > tr:nth-child(1) > td:nth-child(8) > actions-stub').length).toBe(1)
		expect(wrapper.find('div.list > table > tbody > tr:nth-child(1) > td:nth-child(8) > actions-stub:nth-child(1) > actionbutton-stub').text()).toBe('Consulter')

		expect(wrapper.find('div.list > CompConfirm-Stub').exists()).toBe(false)
		expect(wrapper.find('div.list > CompEditGeneric-Stub').exists()).toBe(false)
		expect(wrapper.find('div.list > Modal-Stub').exists()).toBe(false)

		await wrapper.vm.showItem(wrapper.vm.items[0])
		await nextTick()
		await nextTick()
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(1)
		expect(console.error).toHaveBeenCalledTimes(0)
		expect(wrapper.emitted('showitem')).toStrictEqual(undefined)
		expect(wrapper.find('div.list > CompConfirm-Stub').exists()).toBe(false)
		expect(wrapper.find('div.list > CompEditGeneric-Stub').exists()).toBe(false)
		expect(wrapper.find('div.list > Modal-Stub').exists()).toBe(true)
		expect(wrapper.find('div.list > Modal-Stub > div.show').exists()).toBe(true)
		expect(wrapper.find('div.list > Modal-Stub > div.show > h1').text()).toBe('Consultation')
		expect(wrapper.find('div.list > Modal-Stub > div.show > CompShowGeneric-Stub').exists()).toBe(true)
		expect(wrapper.find('div.list > Modal-Stub > div.show > CompShowGeneric-Stub').props().models).toBe('simple')
		expect(wrapper.find('div.list > Modal-Stub > div.show > CompShowGeneric-Stub').props().itemid).toBe(123)
		expect(wrapper.find('div.list > Modal-Stub > div.show > CompShowGeneric-Stub').props().fields).toStrictEqual(getSimpleFieldsj(specialOpt))
	})

	it('Simple onlyshow nogui', async () => {
		initMockAxios()
		const wrapper = shallowMount(CompListGeneric, {
			ceranaI18n,
			ceranaStore,
			propsData: {
				models: 'simple',
				addoption: '0',
				showoption: '2',
				editoption: '0',
				deleteoption: '0',
				fields: getSimpleFieldsj(specialOpt),
			},
		})
		while (wrapper.vm.loading) {
			await nextTick()
		}
		await nextTick()
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(1)
		expect(axios.get).toHaveBeenCalledWith('/apps/cerana_adhesion/simple?page=0&size=25')
		expect(console.error).toHaveBeenCalledTimes(0)
		expect(wrapper.find('div.list').exists()).toBe(true)
		expect(wrapper.find('div.list > input.iconadd').exists()).toBe(false)
		expect(wrapper.find('div.list > div.navsearch').exists()).toBe(false)
		expect(wrapper.find('div.list > div.navlist').exists()).toBe(true)
		expect(wrapper.findAll('div.list > table > thead > tr > th').length).toBe(8)
		expect(wrapper.findAll('div.list > table > tbody > tr').length).toBe(1)
		expect(wrapper.findAll('div.list > table > tbody > tr:nth-child(1) > td').length).toBe(8)
		expect(wrapper.findAll('div.list > table > tbody > tr:nth-child(1) > td:nth-child(8) > actions-stub').length).toBe(1)
		expect(wrapper.find('div.list > table > tbody > tr:nth-child(1) > td:nth-child(8) > actions-stub:nth-child(1) > actionbutton-stub').text()).toBe('Consulter')

		expect(wrapper.find('div.list > CompConfirm-Stub').exists()).toBe(false)
		expect(wrapper.find('div.list > CompEditGeneric-Stub').exists()).toBe(false)
		expect(wrapper.find('div.list > Modal-Stub').exists()).toBe(false)

		await wrapper.vm.showItem(wrapper.vm.items[0])
		await nextTick()
		await nextTick()
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(1)
		expect(console.error).toHaveBeenCalledTimes(0)
		expect(wrapper.emitted('showitem')).toStrictEqual([[wrapper.vm.items[0]]])
		expect(wrapper.find('div.list > CompConfirm-Stub').exists()).toBe(false)
		expect(wrapper.find('div.list > CompEditGeneric-Stub').exists()).toBe(false)
		expect(wrapper.find('div.list > Modal-Stub').exists()).toBe(false)
	})

	it('Simple nav', async () => {
		initMockAxios()
		const wrapper = shallowMount(CompListGeneric, {
			ceranaI18n,
			ceranaStore,
			propsData: {
				models: 'simple',
				fields: getSimpleFieldsj(specialOpt),
			},
		})
		while (wrapper.vm.loading) {
			await nextTick()
		}
		await nextTick()
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(1)
		expect(axios.get).toHaveBeenCalledWith('/apps/cerana_adhesion/simple?page=0&size=25')
		expect(console.error).toHaveBeenCalledTimes(0)
		expect(wrapper.find('div.list').exists()).toBe(true)
		expect(wrapper.find('div.list > input.iconadd').exists()).toBe(true)
		expect(wrapper.find('div.list > div.navsearch').exists()).toBe(false)
		expect(wrapper.find('div.list > div.navlist').exists()).toBe(true)
		expect(wrapper.find('div.list > div.navlist > slidingpagination-stub').exists()).toBe(true)
		expect(wrapper.findAll('div.list > div.navlist > select > option').length).toBe(5)
		expect(wrapper.find('div.list > div.navlist > label').text()).toBe('1 ligne(s)')
		expect(wrapper.findAll('div.list > table > tbody > tr').length).toBe(1)
		expect(wrapper.find('div.list > CompConfirm-Stub').exists()).toBe(false)
		expect(wrapper.find('div.list > CompEditGeneric-Stub').exists()).toBe(false)
		expect(wrapper.find('div.list > Modal-Stub').exists()).toBe(false)

		setSelectValue(wrapper, 'div.list > div.navlist > select', '100')
		await nextTick()
		while (wrapper.vm.loading) {
			await nextTick()
		}
		await nextTick()
		expect(console.error).toHaveBeenCalledTimes(0)
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(2)
		expect(axios.get).toHaveBeenNthCalledWith(1, '/apps/cerana_adhesion/simple?page=0&size=25')
		expect(axios.get).toHaveBeenNthCalledWith(2, '/apps/cerana_adhesion/simple?page=0&size=100')

		await wrapper.vm.pageChange(3)
		await nextTick()
		while (wrapper.vm.loading) {
			await nextTick()
		}
		await nextTick()
		expect(console.error).toHaveBeenCalledTimes(0)
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(3)
		expect(axios.get).toHaveBeenNthCalledWith(1, '/apps/cerana_adhesion/simple?page=0&size=25')
		expect(axios.get).toHaveBeenNthCalledWith(2, '/apps/cerana_adhesion/simple?page=0&size=100')
		expect(axios.get).toHaveBeenNthCalledWith(3, '/apps/cerana_adhesion/simple?page=2&size=100')
	})

	it('Simple searching text', async () => {
		initMockAxios()
		const wrapper = shallowMount(CompListGeneric, {
			ceranaI18n,
			ceranaStore,
			propsData: {
				models: 'simple',
				searching: {},
				fields: getSimpleFieldsj(specialOpt),
			},
		})
		while (wrapper.vm.loading) {
			await nextTick()
		}
		await nextTick()
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(1)
		expect(axios.get).toHaveBeenCalledWith('/apps/cerana_adhesion/simple?page=0&size=25')
		expect(console.error).toHaveBeenCalledTimes(0)
		expect(wrapper.find('div.list').exists()).toBe(true)
		expect(wrapper.find('div.list > input.iconadd').exists()).toBe(true)
		expect(wrapper.find('div.list > div.navsearch').exists()).toBe(true)
		expect(wrapper.find('div.list > div.navsearch > label').text()).toBe('Critère de recherche')
		expect(wrapper.find('div.list > div.navsearch > input').attributes('type')).toBe('text')
		expect(wrapper.findAll('div.list > div.navsearch > actions-stub > actionbutton-stub').length).toBe(1)
		expect(wrapper.find('div.list > div.navsearch > actions-stub > actionbutton-stub').text()).toBe('Rechercher')
		expect(wrapper.find('div.list > div.navlist').exists()).toBe(true)
		expect(wrapper.findAll('div.list > table > tbody > tr').length).toBe(1)
		expect(wrapper.find('div.list > CompConfirm-Stub').exists()).toBe(false)
		expect(wrapper.find('div.list > CompEditGeneric-Stub').exists()).toBe(false)
		expect(wrapper.find('div.list > Modal-Stub').exists()).toBe(false)

		setInputValue(wrapper, 'div.list > div.navsearch > input', 'foo')
		await nextTick()
		await wrapper.vm.sizeChange()
		while (wrapper.vm.loading) {
			await nextTick()
		}
		await nextTick()
		expect(axios.get).toHaveBeenCalledTimes(2)
		expect(axios.get).toHaveBeenNthCalledWith(1, '/apps/cerana_adhesion/simple?page=0&size=25')
		expect(axios.get).toHaveBeenNthCalledWith(2, '/apps/cerana_adhesion/simple?page=0&size=25&search=foo')
	})

	it('Simple searching object', async () => {
		initMockAxios()
		const wrapper = shallowMount(CompListGeneric, {
			ceranaI18n,
			ceranaStore,
			propsData: {
				models: 'simple',
				searching: { mode: 1, name: 'foo'},
				fields: getSimpleFieldsj(specialOpt),
			},
		})
		while (wrapper.vm.loading) {
			await nextTick()
		}
		await nextTick()
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(1)
		expect(axios.get).toHaveBeenCalledWith('/apps/cerana_adhesion/simple?page=0&size=25&mode=1&name=foo')
		expect(console.error).toHaveBeenCalledTimes(0)
		expect(wrapper.find('div.list').exists()).toBe(true)
		expect(wrapper.find('div.list > input.iconadd').exists()).toBe(true)
		expect(wrapper.find('div.list > div.navsearch').exists()).toBe(false)
		expect(wrapper.find('div.list > div.navlist').exists()).toBe(true)
		expect(wrapper.findAll('div.list > table > tbody > tr').length).toBe(1)
		expect(wrapper.find('div.list > CompConfirm-Stub').exists()).toBe(false)
		expect(wrapper.find('div.list > CompEditGeneric-Stub').exists()).toBe(false)
		expect(wrapper.find('div.list > Modal-Stub').exists()).toBe(false)
	})
})
