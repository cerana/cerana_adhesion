'use strict'

import { nextTick } from 'vue'
import { shallowMount } from '@vue/test-utils'
import { initMockAxios } from './functions.js'
import { ceranaI18n, ceranaStore } from '../../src/cerana_lib.js'
import axios from 'axios'
import MenuDashboard from '../../src/MenuDashboard'

jest.mock('axios')

beforeEach(() => {
	document.documentElement.innerHTML = '<html><body></body></html>'
	global.console.error = jest.fn()
	global.console.warn = jest.fn()
})

describe('Menu Dashboard', () => {
	it('No show', async () => {
		ceranaStore.commit('clean')
		initMockAxios()
		const wrapper = shallowMount(MenuDashboard, {
			ceranaI18n,
			ceranaStore,
			propsData: {},
		})
		await nextTick()
		while (wrapper.vm.loading) {
			await nextTick()
		}
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(5)
		expect(wrapper.find('#dashboard > h1').text()).toBe('Tableau de bord')
		expect(wrapper.find('#dashboard > div > #emptycontent > h2').text()).toBe('Tableau de bord vide !')
		expect(wrapper.findAll('#dashboard > div > div.show > div > fieldset').length).toBe(1)
		expect(wrapper.find('#dashboard > div > div.show > div > fieldset > compshowgeneric-stub').props().models).toBe('adherents')
		expect(wrapper.find('#dashboard > div > div.show > div > fieldset > compshowgeneric-stub').props().itemid).toBe(-1)
	})

	it('show default', async () => {
		ceranaStore.commit('clean')
		initMockAxios()
		const wrapper = shallowMount(MenuDashboard, {
			ceranaI18n,
			ceranaStore,
			propsData: {},
		})
		await nextTick()
		while (wrapper.vm.loading) {
			await nextTick()
		}
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(5)

		await wrapper.vm.showAdherentCurrent({ id: 123, last_structure: 5 })
		await nextTick()
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(5)
		expect(wrapper.find('#dashboard > h1').text()).toBe('Tableau de bord')
		expect(wrapper.find('#dashboard > div > #emptycontent').exists()).toBe(false)
		expect(wrapper.findAll('#dashboard > div > div.show > div > fieldset').length).toBe(2)
		expect(wrapper.find('#dashboard > div > div.show > div > fieldset:nth-child(1) > legend').text()).toBe('Identité')
		expect(wrapper.find('#dashboard > div > div.show > div > fieldset:nth-child(1) > compshowgeneric-stub').props().models).toBe('adherents')
		expect(wrapper.find('#dashboard > div > div.show > div > fieldset:nth-child(1) > compshowgeneric-stub').props().itemid).toBe(123)
		expect(wrapper.find('#dashboard > div > div.show > div > fieldset:nth-child(2) > legend').text()).toBe('Structure d\'appartenance')
		expect(wrapper.find('#dashboard > div > div.show > div > fieldset:nth-child(2) > compshowgeneric-stub').props().models).toBe('structures')
		expect(wrapper.find('#dashboard > div > div.show > div > fieldset:nth-child(2) > compshowgeneric-stub').props().itemid).toBe(5)
		expect(wrapper.find('#dashboard > div > div.show > div > fieldset:nth-child(2) > compshowgeneric-stub').props().ignoreid).toBe(0)
		await wrapper.vm.showStructureCurrent(true)
		await nextTick()
		await nextTick()
	})
})
