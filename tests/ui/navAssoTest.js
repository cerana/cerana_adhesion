'use strict'

import { nextTick } from 'vue'
import { shallowMount } from '@vue/test-utils'
import { initMockAxios } from './functions.js'
import { ceranaI18n, ceranaStore } from '../../src/cerana_lib.js'
import NavAsso from '../../src/NavAsso'
import axios from 'axios'

jest.mock('axios')

beforeEach(() => {
	document.documentElement.innerHTML = '<html><body></body></html>'
	global.console.error = jest.fn()
	global.console.warn = jest.fn()
})

describe('Nav asso', () => {
	it('Show default', async () => {
		ceranaStore.commit('clean')
		initMockAxios()
		const wrapper = shallowMount(NavAsso, {
			ceranaI18n,
			ceranaStore,
			propsData: {},
		})
		while (wrapper.vm.loading) {
			await nextTick()
		}
		await nextTick()
		expect(wrapper.find('div.app-asso').exists()).toBe(true)
		expect(wrapper.find('div.app-asso > AppNavigation-Stub').exists()).toBe(true)
		expect(wrapper.findAll('div.app-asso > AppNavigation-Stub > ul > AppNavigationItem-Stub').length).toBe(5)
		expect(wrapper.find('div.app-asso > AppNavigation-Stub > ul > AppNavigationItem-Stub:nth-child(1)').props().title).toBe('Tableau de bord')
		expect(wrapper.find('div.app-asso > AppNavigation-Stub > ul > AppNavigationItem-Stub:nth-child(2)').props().title).toBe('Ma fiche')
		expect(wrapper.find('div.app-asso > AppNavigation-Stub > ul > AppNavigationItem-Stub:nth-child(3)').props().title).toBe('Les structures')
		expect(wrapper.find('div.app-asso > AppNavigation-Stub > ul > AppNavigationItem-Stub:nth-child(4)').props().title).toBe('Les adhérents')
		expect(wrapper.find('div.app-asso > AppNavigation-Stub > ul > AppNavigationItem-Stub:nth-child(5)').props().title).toBe('Statistiques')
		expect(wrapper.findAll('div.app-asso > AppNavigation-Stub > SettingsSection-Stub > ul > AppNavigationItem-Stub').length).toBe(2)
		expect(wrapper.find('div.app-asso > AppNavigation-Stub > SettingsSection-Stub > ul > AppNavigationItem-Stub:nth-child(1)').props().title).toBe('Général')
		expect(wrapper.find('div.app-asso > AppNavigation-Stub > SettingsSection-Stub > ul > AppNavigationItem-Stub:nth-child(2)').props().title).toBe('Cotisations')
		expect(wrapper.find('div.app-asso > AppContent-Stub').exists()).toBe(true)
		expect(wrapper.find('div.app-asso > AppContent-Stub > menudashboard-stub').exists()).toBe(true)
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(5)
		expect(axios.get).toHaveBeenNthCalledWith(1, '/apps/cerana_adhesion/levels')
		expect(axios.get).toHaveBeenNthCalledWith(2, '/apps/cerana_adhesion/structures')
		expect(axios.get).toHaveBeenNthCalledWith(3, '/apps/cerana_adhesion/type_adhesions')
		expect(axios.get).toHaveBeenNthCalledWith(4, '/apps/cerana_adhesion/type_responsabilitys')
		expect(axios.get).toHaveBeenNthCalledWith(5, '/apps/cerana_adhesion/type_practices')
		expect(console.error).toHaveBeenCalledTimes(0)
		expect(ceranaStore.state.seasons.length).toBe(23)
		expect(ceranaStore.state.levels.length).toBe(3)
		expect(ceranaStore.state.structures.length).toBe(13)
		expect(ceranaStore.state.typeadhesions.length).toBe(4)
		expect(ceranaStore.state.typeresponsabilities.length).toBe(18)
		expect(ceranaStore.state.typepractices.length).toBe(5)
	})
	it('Check change', async () => {
		ceranaStore.commit('clean')
		initMockAxios()
		const wrapper = shallowMount(NavAsso, {
			ceranaI18n,
			ceranaStore,
			propsData: {},
		})
		await nextTick()
		expect(wrapper.find('div.app-asso').exists()).toBe(true)

		await wrapper.vm.openMenu('Subscription')
		await nextTick()
		expect(wrapper.find('div.app-asso > AppContent-Stub > menusubscription-stub').exists()).toBe(true)

		await wrapper.vm.openMenu('General')
		await nextTick()
		expect(wrapper.find('div.app-asso > AppContent-Stub > menugeneral-stub').exists()).toBe(true)

		await wrapper.vm.openMenu('Report')
		await nextTick()
		expect(wrapper.find('div.app-asso > AppContent-Stub > menureport-stub').exists()).toBe(true)

		await wrapper.vm.openMenu('List')
		await nextTick()
		expect(wrapper.find('div.app-asso > AppContent-Stub > menulist-stub').exists()).toBe(true)

		await wrapper.vm.openMenu('Structure')
		await nextTick()
		expect(wrapper.find('div.app-asso > AppContent-Stub > menustructure-stub').exists()).toBe(true)

		await wrapper.vm.openMenu('Current')
		await nextTick()
		expect(wrapper.find('div.app-asso > AppContent-Stub > menucurrent-stub').exists()).toBe(true)

		await wrapper.vm.openMenu('Dashboard')
		await nextTick()
		expect(wrapper.find('div.app-asso > AppContent-Stub > menudashboard-stub').exists()).toBe(true)
	})
})
