'use strict'

import { nextTick } from 'vue'
import { shallowMount } from '@vue/test-utils'
import { initMockAxios } from './functions.js'
import { ceranaI18n, ceranaStore } from '../../src/cerana_lib.js'
import axios from 'axios'
import CompShowAdherent from '../../src/CompShowAdherent'

jest.mock('axios')

beforeEach(() => {
	document.documentElement.innerHTML = '<html><body></body></html>'
	global.console.error = jest.fn()
	global.console.warn = jest.fn()
})

describe('Comp Show Adherent', () => {
	it('Show null', async () => {
		ceranaStore.commit('clean')
		initMockAxios()
		const wrapper = shallowMount(CompShowAdherent, {
			ceranaI18n,
			ceranaStore,
			propsData: {},
		})
		while (wrapper.vm.loading) {
			await nextTick()
		}
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(0)
		expect(wrapper.find('div').exists()).toBe(false)
	})

	it('Show default', async () => {
		ceranaStore.commit('clean')
		initMockAxios()
		const wrapper = shallowMount(CompShowAdherent, {
			ceranaI18n,
			ceranaStore,
			propsData: {
				initadherentid: 456,
			},
		})
		while (wrapper.vm.loading) {
			await nextTick()
		}
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(1)
		expect(axios.get).toHaveBeenCalledWith('/apps/cerana_adhesion/adherents/456')
		expect(console.error).toHaveBeenCalledTimes(0)
		expect(wrapper.find('div > div:nth-child(2) > label').text()).toBe('N° adhérent')
		expect(wrapper.find('div > div:nth-child(3) > label').text()).toBe('Genre')
		expect(wrapper.find('div > div:nth-child(4) > label').text()).toBe('Nom')
		expect(wrapper.find('div > div:nth-child(5) > label').text()).toBe('Prénom')
		expect(wrapper.find('div > div:nth-child(6) > label').text()).toBe('Date de naissance')
		expect(wrapper.find('div > div:nth-child(7) > label').text()).toBe('Lieu de naissance')
		expect(wrapper.find('div > div:nth-child(8) > label').text()).toBe('Tel. 1')
		expect(wrapper.find('div > div:nth-child(9) > label').text()).toBe('Tel. 2')
		expect(wrapper.find('div > div:nth-child(10) > label').text()).toBe('courriel')
		expect(wrapper.find('div > div:nth-child(11) > label').text()).toBe('Adresse')
		expect(wrapper.find('div > div:nth-child(12) > label').text()).toBe('Code postal')
		expect(wrapper.find('div > div:nth-child(13) > label').text()).toBe('ville')
		expect(wrapper.find('div > div:nth-child(14) > label').text()).toBe('Pays')
		expect(wrapper.find('div > div:nth-child(15) > label').text()).toBe('Commentaires')
		expect(wrapper.find('div > div:nth-child(16) > label').text()).toBe('Connexion')

		expect(wrapper.find('div > div:nth-child(1) > img').attributes('src')).toBe('data:*/*;base64,/9j/4AAQSkZJRgABAQEBLAEsAAD/4QvKRXhpZgAASUkqAAgAAAAGABoBBQABAAAAVgAAABsBBQAB')
		expect(wrapper.find('div > div:nth-child(2) > span').text()).toBe('456')
		expect(wrapper.find('div > div:nth-child(3) > span').text()).toBe('Homme')
		expect(wrapper.find('div > div:nth-child(4) > span').text()).toBe('Attand')
		expect(wrapper.find('div > div:nth-child(5) > span').text()).toBe('Charles')
		expect(wrapper.find('div > div:nth-child(6) > span').text()).toBe('28/04/1986')
		expect(wrapper.find('div > div:nth-child(7) > span').text()).toBe('Là-bas')
		expect(wrapper.find('div > div:nth-child(8) > span').text()).toBe('08.12.23.45.78')
		expect(wrapper.find('div > div:nth-child(9) > span').text()).toBe('07.98.76.54.32')
		expect(wrapper.find('div > div:nth-child(10) > span > a').text()).toBe('charles.attend@costy.net')
		expect(wrapper.find('div > div:nth-child(11) > pre').text()).toBe('4 place centrale')
		expect(wrapper.find('div > div:nth-child(12) > span').text()).toBe('99876')
		expect(wrapper.find('div > div:nth-child(13) > span').text()).toBe('Ici')
		expect(wrapper.find('div > div:nth-child(14) > span').text()).toBe('France')
		expect(wrapper.find('div > div:nth-child(15) > pre').text()).toBe('Bla bla bla\nBla bla')
		expect(wrapper.find('div > compconfirm-stub').exists()).toBe(false)
		expect(console.error).toHaveBeenCalledTimes(0)
	})

	it('Show no user / status = 0', async () => {
		ceranaStore.commit('clean')
		initMockAxios({ uid: null, status: 0 })
		const wrapper = shallowMount(CompShowAdherent, {
			ceranaI18n,
			ceranaStore,
			propsData: {
				initadherentid: 456,
			},
		})
		while (wrapper.vm.loading) {
			await nextTick()
		}
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(1)
		expect(axios.get).toHaveBeenCalledWith('/apps/cerana_adhesion/adherents/456')
		expect(wrapper.find('div > div:nth-child(16) > table.connect > tr > td:nth-child(1) > span').text()).toBe('---')
		expect(wrapper.findAll('div > div:nth-child(16) > table.connect > tr > td:nth-child(2) > actions-stub').length).toBe(1)
		expect(wrapper.find('div > div:nth-child(16) > table.connect > tr > td:nth-child(2) > actions-stub > actionbutton-stub').text()).toBe('Créer')
		await wrapper.vm.connect(1)
		await nextTick()
		expect(wrapper.find('div > compconfirm-stub').exists()).toBe(true)
		expect(wrapper.find('div > compconfirm-stub').props().message).toBe('Voulez vous modifier cette connexion ?')
		await wrapper.vm.confirmConnect(true)
		await nextTick()
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(1)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(2)
		expect(axios.put).toHaveBeenCalledWith('/apps/cerana_adhesion/connection', { id: 456, mode: 1 })
		expect(console.error).toHaveBeenCalledTimes(0)
	})

	it('Show user / status = 1', async () => {
		ceranaStore.commit('clean')
		initMockAxios({ uid: 'charles.attand', status: 1 })
		const wrapper = shallowMount(CompShowAdherent, {
			ceranaI18n,
			ceranaStore,
			propsData: {
				initadherentid: 456,
			},
		})
		while (wrapper.vm.loading) {
			await nextTick()
		}
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(1)
		expect(axios.get).toHaveBeenCalledWith('/apps/cerana_adhesion/adherents/456')
		expect(wrapper.find('div > div:nth-child(16) > table.connect > tr > td:nth-child(1) > span').text()).toBe('charles.attand')
		expect(wrapper.findAll('div > div:nth-child(16) > table.connect > tr > td:nth-child(2) > actions-stub').length).toBe(2)
		expect(wrapper.find('div > div:nth-child(16) > table.connect > tr > td:nth-child(2) > actions-stub:nth-child(1) > actionbutton-stub').text()).toBe('Activer')
		expect(wrapper.find('div > div:nth-child(16) > table.connect > tr > td:nth-child(2) > actions-stub:nth-child(2) > actionbutton-stub').text()).toBe('Supprimer')
		await wrapper.vm.connect(2)
		await nextTick()
		expect(wrapper.find('div > compconfirm-stub').exists()).toBe(true)
		expect(wrapper.find('div > compconfirm-stub').props().message).toBe('Voulez vous modifier cette connexion ?')
		await wrapper.vm.confirmConnect(true)
		await nextTick()
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(1)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(2)
		expect(axios.put).toHaveBeenCalledWith('/apps/cerana_adhesion/connection', { id: 456, mode: 2 })
		expect(console.error).toHaveBeenCalledTimes(0)
	})

	it('Show user / status = 2', async () => {
		ceranaStore.commit('clean')
		initMockAxios({ uid: 'charles.attand', status: 2 })
		const wrapper = shallowMount(CompShowAdherent, {
			ceranaI18n,
			ceranaStore,
			propsData: {
				initadherentid: 456,
			},
		})
		while (wrapper.vm.loading) {
			await nextTick()
		}
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(1)
		expect(axios.get).toHaveBeenCalledWith('/apps/cerana_adhesion/adherents/456')
		expect(wrapper.find('div > div:nth-child(16) > table.connect > tr > td:nth-child(1) > span').text()).toBe('charles.attand')
		expect(wrapper.findAll('div > div:nth-child(16) > table.connect > tr > td:nth-child(2) > actions-stub').length).toBe(2)
		expect(wrapper.find('div > div:nth-child(16) > table.connect > tr > td:nth-child(2) > actions-stub:nth-child(1) > actionbutton-stub').text()).toBe('Désactiver')
		expect(wrapper.find('div > div:nth-child(16) > table.connect > tr > td:nth-child(2) > actions-stub:nth-child(2) > actionbutton-stub').text()).toBe('Supprimer')
		await wrapper.vm.connect(3)
		await nextTick()
		expect(wrapper.find('div > compconfirm-stub').exists()).toBe(true)
		expect(wrapper.find('div > compconfirm-stub').props().message).toBe('Voulez vous modifier cette connexion ?')
		await wrapper.vm.confirmConnect(true)
		await nextTick()
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(1)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(2)
		expect(axios.put).toHaveBeenCalledWith('/apps/cerana_adhesion/connection', { id: 456, mode: 3 })
		expect(console.error).toHaveBeenCalledTimes(0)
	})

	it('Show user delete', async () => {
		ceranaStore.commit('clean')
		initMockAxios({ uid: 'charles.attand', status: 2 })
		const wrapper = shallowMount(CompShowAdherent, {
			ceranaI18n,
			ceranaStore,
			propsData: {
				initadherentid: 456,
			},
		})
		while (wrapper.vm.loading) {
			await nextTick()
		}
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(0)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(1)
		expect(axios.get).toHaveBeenCalledWith('/apps/cerana_adhesion/adherents/456')
		expect(wrapper.find('div > div:nth-child(16) > table.connect > tr > td:nth-child(1) > span').text()).toBe('charles.attand')
		expect(wrapper.findAll('div > div:nth-child(16) > table.connect > tr > td:nth-child(2) > actions-stub').length).toBe(2)
		expect(wrapper.find('div > div:nth-child(16) > table.connect > tr > td:nth-child(2) > actions-stub:nth-child(1) > actionbutton-stub').text()).toBe('Désactiver')
		expect(wrapper.find('div > div:nth-child(16) > table.connect > tr > td:nth-child(2) > actions-stub:nth-child(2) > actionbutton-stub').text()).toBe('Supprimer')
		await wrapper.vm.connect(4)
		await nextTick()
		expect(wrapper.find('div > compconfirm-stub').exists()).toBe(true)
		expect(wrapper.find('div > compconfirm-stub').props().message).toBe('Voulez vous modifier cette connexion ?')
		await wrapper.vm.confirmConnect(true)
		await nextTick()
		expect(axios.post).toHaveBeenCalledTimes(0)
		expect(axios.put).toHaveBeenCalledTimes(1)
		expect(axios.delete).toHaveBeenCalledTimes(0)
		expect(axios.get).toHaveBeenCalledTimes(2)
		expect(axios.put).toHaveBeenCalledWith('/apps/cerana_adhesion/connection', { id: 456, mode: 4 })
		expect(console.error).toHaveBeenCalledTimes(0)
	})

})
