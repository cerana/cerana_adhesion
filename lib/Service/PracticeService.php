<?php
namespace OCA\CeranaAdhesion\Service;

use Exception;
use OCA\CeranaAdhesion\Db\Practice;
use OCA\CeranaAdhesion\Db\PracticeMapper;

class PracticeService extends AbstractService
{

    public function __construct(PracticeMapper $mapper)
    {
        parent::__construct($mapper);
    }

    public function findAllEx($subscription, $page, $size): array
    {
        return $this->mapper->findAll($subscription, $page, $size);
    }

    public function findAdherents($structureid, $date, $typepractice, $page, $size): array
    {
        return $this->mapper->findAdherents($structureid, $date, $typepractice, $page, $size);
    }

    public function create($subscription, $typepractice, $comment)
    {
        $obj = new Practice();
        $obj->setSubscription($subscription);
        $obj->setTypepractice($typepractice);
        $obj->setComment($comment);
        return $this->mapper->insert($obj);
    }

    public function update($id, $subscription, $typepractice, $comment)
    {
        try {
            $obj = $this->find($id);
            $obj->setSubscription($subscription);
            $obj->setTypepractice($typepractice);
            $obj->setComment($comment);
            return $this->mapper->update($obj);
        } catch (Exception $e) {
            $this->handleException($e);
        }
    }
}
