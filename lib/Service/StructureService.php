<?php
namespace OCA\CeranaAdhesion\Service;

use Exception;
use OCA\CeranaAdhesion\Db\Structure;
use OCA\CeranaAdhesion\Db\StructureMapper;

class StructureService extends AbstractService
{

    public function __construct(StructureMapper $mapper)
    {
        parent::__construct($mapper);
    }

    public function create($name, $phone1, $phone2, $email, $website, $address, $postalcode, $city, $comment, $level, $parent)
    {
        $obj = new Structure();
        $obj->setName($name);
        $obj->setPhone1($phone1);
        $obj->setPhone2($phone2);
        $obj->setEmail($email);
        $obj->setWebsite($website);
        $obj->setAddress($address);
        $obj->setPostalcode($postalcode);
        $obj->setCity($city);
        $obj->setComment($comment);
        $obj->setLevel($level);
        $obj->setParent($parent);
        return $this->mapper->insert($obj);
    }

    public function update($id, $name, $phone1, $phone2, $email, $website, $address, $postalcode, $city, $comment, $level, $parent)
    {
        try {
            $obj = $this->find($id);
            $obj->setName($name);
            $obj->setPhone1($phone1);
            $obj->setPhone2($phone2);
            $obj->setEmail($email);
            $obj->setWebsite($website);
            $obj->setAddress($address);
            $obj->setPostalcode($postalcode);
            $obj->setCity($city);
            $obj->setComment($comment);
            $obj->setLevel($level);
            $obj->setParent($parent);
            return $this->mapper->update($obj);
        } catch (Exception $e) {
            $this->handleException($e);
        }
    }
}
