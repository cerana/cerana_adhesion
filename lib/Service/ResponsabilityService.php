<?php
namespace OCA\CeranaAdhesion\Service;

use Exception;
use OCA\CeranaAdhesion\Db\Responsability;
use OCA\CeranaAdhesion\Db\ResponsabilityMapper;

class ResponsabilityService extends AbstractService
{

    public function __construct(ResponsabilityMapper $mapper)
    {
        parent::__construct($mapper);
    }

    public function findAllEx($subscription, $page, $size): array
    {
        return $this->mapper->findAll($subscription, $page, $size);
    }

    public function findAdherents($structureid, $date, $page, $size): array
    {
        return $this->mapper->findAdherents($structureid, $date, $page, $size);
    }

    public function create($subscription, $typeresponsability, $structure, $comment)
    {
        $obj = new Responsability();
        $obj->setSubscription($subscription);
        $obj->setTyperesponsability($typeresponsability);
        $obj->setStructure($structure);
        $obj->setComment($comment);
        return $this->mapper->insert($obj);
    }

    public function update($id, $subscription, $typeresponsability, $structure, $comment)
    {
        try {
            $obj = $this->find($id);
            $obj->setSubscription($subscription);
            $obj->setTyperesponsability($typeresponsability);
            $obj->setStructure($structure);
            $obj->setComment($comment);
            return $this->mapper->update($obj);
        } catch (Exception $e) {
            $this->handleException($e);
        }
    }
}
