<?php
namespace OCA\CeranaAdhesion\Service;

use Exception;
use OCA\CeranaAdhesion\Db\TypeResponsability;
use OCA\CeranaAdhesion\Db\TypeResponsabilityMapper;

class TypeResponsabilityService extends AbstractService
{

    public function __construct(TypeResponsabilityMapper $mapper)
    {
        parent::__construct($mapper);
    }

    public function create($level, $name)
    {
        $obj = new TypeResponsability();
        $obj->setLevel($level);
        $obj->setName($name);
        return $this->mapper->insert($obj);
    }

    public function update($id, $level, $name)
    {
        try {
            $obj = $this->find($id);
            $obj->setLevel($level);
            $obj->setName($name);
            return $this->mapper->update($obj);
        } catch (Exception $e) {
            $this->handleException($e);
        }
    }
}
