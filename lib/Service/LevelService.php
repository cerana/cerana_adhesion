<?php
namespace OCA\CeranaAdhesion\Service;

use Exception;
use OCA\CeranaAdhesion\Db\Level;
use OCA\CeranaAdhesion\Db\LevelMapper;

class LevelService extends AbstractService
{

    public function __construct(LevelMapper $mapper)
    {
        parent::__construct($mapper);
    }

    public function create($level, $name, $comment)
    {
        $obj = new Level();
        $obj->setLevel($level);
        $obj->setName($name);
        $obj->setComment($comment);
        return $this->mapper->insert($obj);
    }

    public function update($id, $level, $name, $comment)
    {
        try {
            $obj = $this->find($id);
            $obj->setLevel($level);
            $obj->setName($name);
            $obj->setComment($comment);
            return $this->mapper->update($obj);
        } catch (Exception $e) {
            $this->handleException($e);
        }
    }
}
