<?php
namespace OCA\CeranaAdhesion\Service;

use Exception;
use OCA\CeranaAdhesion\Db\Adherent;
use OCA\CeranaAdhesion\Db\AdherentMapper;
use OCA\CeranaAdhesion\Db\SubscriptionMapper;
use OCP\IUser;

class AdherentService extends AbstractService
{

    /** @var SubscriptionMapper */
    private $subscription_mapper;

    /**
     * Constructor
     *
     * @param AdherentMapper $mapper
     */
    public function __construct(AdherentMapper $mapper)
    {
        parent::__construct($mapper);
        $this->subscription_mapper = $this->mapper->createSubscription();
    }

    /**
     * Search all Adherent with criteria $search - pagination with $page/$size
     *
     * @param string $search
     * @param int $page
     * @param int $size
     * @return array
     */
    public function findAllEx($search, $page, $size): array
    {
        return $this->mapper->findAll($search, $page, $size);
    }

    /**
     * Search all Adherent with subscription in date '$date' and in structure '$structureid' - pagination with $page/$size
     *
     * @param int $structureid,
     * @param string $date,
     * @param int $page
     * @param int $size
     * @return array
     */
    public function findAdherents($structureid, $date, $page, $size): array
    {
        return $this->mapper->findAdherents($structureid, $date, $page, $size);
    }

    public function findEx($id, $userId)
    {
        try {
            $adherent = null;
            if ($id == - 1) {
                $adherent = $this->mapper->findCurrent($userId);
            } else {
                $adherent = $this->mapper->find($id);
            }
            $adherent['id'] = intval($adherent['id']);
            $adherent['civility'] = intval($adherent['civility']);
            if ($adherent['uid'] != null) {
                $userManager = \OC::$server->getUserManager();
                $user = $userManager->get($adherent['uid']);
                if (! is_null($user)) {
                    if ($user->isEnabled()) {
                        $adherent['status'] = 2;
                    } else {
                        $adherent['status'] = 1;
                    }
                } else {
                    $adherent['uid'] = null;
                }
            } else {
                $adherent['status'] = 0;
            }
            $sub_service = new SubscriptionService($this->subscription_mapper);
            $adherent = array_merge($adherent, $sub_service->getAdherentInfo($adherent['id']));
            return $adherent;
        } catch (Exception $e) {
            $this->handleException($e);
        }
    }

    public function create($civility, $lastname, $firstname, $birthdaydate, $birthdayplace, $email, $address, $postalcode, $city, $country, $phone1, $phone2, $comment, $photoid = null, $dateref = "", $structure = 0, $typeadhesion = 0, $typepractice = 0, $typeresponsability = 0)
    {
        $adherent = new Adherent();
        $adherent->setCivility($civility);
        $adherent->setLastname($lastname);
        $adherent->setFirstname($firstname);
        $adherent->setBirthdaydate($birthdaydate);
        $adherent->setBirthdayplace($birthdayplace);
        $adherent->setEmail($email);
        $adherent->setAddress($address);
        $adherent->setPostalcode($postalcode);
        $adherent->setCity($city);
        $adherent->setCountry($country);
        $adherent->setPhone1($phone1);
        $adherent->setPhone2($phone2);
        $adherent->setComment($comment);
        if ($photoid == null) {
            $adherent->setPhotoid('');
        } else {
            $adherent->setPhotoid($photoid);
        }
        $adherent->setNum(sprintf("% 2s%07d", $adherent->getCountry(), $this->mapper->getNextNum()));
        $new_adherent = $this->mapper->insert($adherent);
        if (($dateref != "") && ($structure != 0) && ($typeadhesion != 0)) {
            $sub_service = new SubscriptionService($this->subscription_mapper);
            $sub_service->create($new_adherent->getId(), $typeadhesion, $structure, $dateref, '', $typepractice, $typeresponsability);
        }
        return $new_adherent;
    }

    public function update($id, $civility, $lastname, $firstname, $birthdaydate, $birthdayplace, $email, $address, $postalcode, $city, $country, $phone1, $phone2, $comment, $photoid = null)
    {
        try {
            $adherent = Adherent::fromRow($this->mapper->find($id, null));
            $adherent->setCivility($civility);
            $adherent->setLastname($lastname);
            $adherent->setFirstname($firstname);
            $adherent->setBirthdaydate($birthdaydate);
            $adherent->setBirthdayplace($birthdayplace);
            $adherent->setEmail($email);
            $adherent->setAddress($address);
            $adherent->setPostalcode($postalcode);
            $adherent->setCity($city);
            $adherent->setCountry($country);
            $adherent->setPhone1($phone1);
            $adherent->setPhone2($phone2);
            $adherent->setComment($comment);
            if ($photoid != null) {
                $adherent->setPhotoid($photoid);
            }
            return $this->mapper->update($adherent);
        } catch (Exception $e) {
            $this->handleException($e);
        }
    }

    public function delete($id)
    {
        try {
            $adherent = Adherent::fromRow($this->mapper->find($id));
            $this->mapper->delete($adherent);
            return $adherent;
        } catch (Exception $e) {
            $this->handleException($e);
        }
    }

    public function connection(int $id, int $mode)
    {
        try {
            $result = "";
            $userManager = \OC::$server->getUserManager();
            $adherent = Adherent::fromRow($this->mapper->find($id));
            if (($adherent->getUid() == null) && ($mode == 1)) { // create user
                $result = $this->create_user($userManager, $adherent);
            } else if (($adherent->getUid() != null) && ($mode == 2)) { // activate user
                $result = $this->activating_user($userManager, $adherent, true);
            } else if (($adherent->getUid() != null) && ($mode == 3)) { // unactivate user
                $result = $this->activating_user($userManager, $adherent, false);
            } else if (($adherent->getUid() != null) && ($mode == 4)) { // delete user
                $result = $this->delete_user($userManager, $adherent);
            } else {
                $result = "BAD";
            }
            return $result;
        } catch (Exception $e) {
            $this->handleException($e);
        }
    }

    protected function delete_user($userManager, $adherent)
    {
        $user = $userManager->get($adherent->getUid());
        if (! is_null($user)) {
            if ($user->delete()) {
                $adherent->setUid(null);
                return $this->mapper->update($adherent);
            } else {
                return "DELETE/NO ASSOCIATE";
            }
        } else {
            $adherent->setUid(null);
            return $this->mapper->update($adherent);
        }
    }

    protected function activating_user($userManager, $adherent, bool $value)
    {
        $user = $userManager->get($adherent->getUid());
        if (! is_null($user)) {
            $user->setEnabled($value);
            return "ACTIVATING:" . $value;
        } else {
            return "NO ACTIVATING";
        }
    }

    protected function create_user($userManager, $adherent)
    {
        $new_passwd = substr(str_shuffle("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!-+=?;:"), 0, 10);
        $new_uid = str_replace(' ', '-', strtolower($adherent->getFirstname() . "." . $adherent->getLastname()));
        $exist = True;
        $num = 0;
        $newlogin = '';
        while ($exist == True) {
            $newlogin = $new_uid;
            if ($num != 0) {
                $newlogin .= "-" . $num;
            }
            $exist = $userManager->userExists($newlogin);
            $num ++;
        }
        $new_user = $userManager->createUser($newlogin, $new_passwd);
        if ($new_user instanceof IUser) {
            $new_user->setDisplayName($adherent->getFirstname() . " " . $adherent->getLastname());
            $new_user->setEMailAddress($adherent->getEmail());
            $adherent->setUid($newlogin);
            $result = $this->mapper->update($adherent);
        } else {
            $result = "CREATE/NOT ASSOCIATE";
        }
        return $result;
    }
}
