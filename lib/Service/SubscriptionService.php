<?php
namespace OCA\CeranaAdhesion\Service;

use Exception;
use OCA\CeranaAdhesion\Db\Subscription;
use OCA\CeranaAdhesion\Db\SubscriptionMapper;
use OCA\CeranaAdhesion\Db\PracticeMapper;
use OCA\CeranaAdhesion\Db\Practice;
use OCA\CeranaAdhesion\Db\ResponsabilityMapper;
use OCA\CeranaAdhesion\Db\Responsability;

class SubscriptionService extends AbstractService
{

    /** @var PracticeMapper */
    private $practice_mapper;

    /** @var ResponsabilityMapper */
    private $responsability_mapper;

    public function __construct(SubscriptionMapper $mapper)
    {
        parent::__construct($mapper);
        $this->practice_mapper = $this->mapper->createPractice();
        $this->responsability_mapper = $this->mapper->createResponsability();
    }

    public function statistics($structure, $dateref): array
    {
        try {
            $result = array();
            $result['subscriptions'] = $this->mapper->statistics($structure, $dateref);
            $result['practices'] = $this->practice_mapper->statistics($structure, $dateref);
            $result['responsabilities'] = $this->responsability_mapper->statistics($structure, $dateref);
            return $result;
        } catch (Exception $e) {
            $this->handleException($e);
        }
    }

    public function search($adherent, $dateref)
    {
        try {
            return $this->mapper->search($adherent, $dateref);
        } catch (Exception $e) {
            $this->handleException($e);
        }
    }

    public function create($adherent, $typeadhesion, $structure, $dateref, $comment, $typepractice = 0, $typeresponsability = 0)
    {
        $obj = new Subscription();
        $obj->setAdherent($adherent);
        $obj->setTypeadhesion($typeadhesion);
        $obj->setStructure($structure);
        $obj->setDateref($dateref);
        $obj->setComment($comment);
        $new_subscription = $this->mapper->insert($obj);
        if ($typepractice != 0) {
            $practice = new Practice();
            $practice->setSubscription($new_subscription->getId());
            $practice->setTypepractice($typepractice);
            $practice->setComment("");
            $this->practice_mapper->insert($practice);
        }
        if ($typeresponsability != 0) {
            $responsability = new Responsability();
            $responsability->setSubscription($new_subscription->getId());
            $responsability->setStructure($structure);
            $responsability->setTyperesponsability($typeresponsability);
            $responsability->setComment("");
            $this->responsability_mapper->insert($responsability);
        }
        return $new_subscription;
    }

    public function update($id, $adherent, $typeadhesion, $structure, $dateref, $comment)
    {
        try {
            $obj = $this->find($id);
            $obj->setAdherent($adherent);
            $obj->setTypeadhesion($typeadhesion);
            $obj->setStructure($structure);
            $obj->setDateref($dateref);
            $obj->setComment($comment);
            return $this->mapper->update($obj);
        } catch (Exception $e) {
            $this->handleException($e);
        }
    }

    public function getAdherentInfo($adherent)
    {
        $subscription_info = Array();
        $subscription_info['last_structure'] = 0;
        $subscription_info['last_season'] = '';
        $subscription_info['last_typeadhesion'] = 0;
        $subscription_info['last_practice'] = 0;
        $subscription_info['last_responsability'] = 0;
        
        $last_subscription = null;
        $list_year = array();
        foreach ($this->mapper->searchAdherent($adherent) as $subscription) {
            if ($last_subscription == null) {
                $last_subscription = $subscription;
                $subscription_info['last_season'] = $subscription->getDateref();
            }
            $list_year[] = $subscription->getDateref();
        }
        if ($last_subscription != null) {
            $subscription_info['last_structure'] = intval($last_subscription->getStructure());
            $subscription_info['last_typeadhesion'] = intval($last_subscription->getTypeadhesion());
            $practices = $this->practice_mapper->findAll($last_subscription->getId(), 0, 1);
            if (count($practices["list"]) == 1) {
                $subscription_info['last_practice'] = intval($practices["list"][0]->getTypepractice());
            }
            $responsabilities = $this->responsability_mapper->findAll($last_subscription->getId(), 0, 1, $subscription_info['last_structure']);
            if (count($responsabilities["list"]) > 0) {
                $subscription_info['last_responsability'] = intval($responsabilities["list"][0]->getTyperesponsability());
            }
        }
        $subscription_info['list_year'] = $list_year;
        return $subscription_info;
    }
}
