<?php
namespace OCA\CeranaAdhesion\Service;

use Exception;
use OCA\CeranaAdhesion\Db\TypePractice;
use OCA\CeranaAdhesion\Db\TypePracticeMapper;

class TypePracticeService extends AbstractService
{

    public function __construct(TypePracticeMapper $mapper)
    {
        parent::__construct($mapper);
    }

    public function create($name)
    {
        $obj = new TypePractice();
        $obj->setName($name);
        return $this->mapper->insert($obj);
    }

    public function update($id, $name)
    {
        try {
            $obj = $this->find($id);
            $obj->setName($name);
            return $this->mapper->update($obj);
        } catch (Exception $e) {
            $this->handleException($e);
        }
    }
}
