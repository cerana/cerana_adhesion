<?php
namespace OCA\CeranaAdhesion\Service;

use Exception;
use OCA\CeranaAdhesion\Db\TypeAdhesion;
use OCA\CeranaAdhesion\Db\TypeAdhesionMapper;

class TypeAdhesionService extends AbstractService
{

    public function __construct(TypeAdhesionMapper $mapper)
    {
        parent::__construct($mapper);
    }

    public function create($name)
    {
        $obj = new TypeAdhesion();
        $obj->setName($name);
        return $this->mapper->insert($obj);
    }

    public function update($id, $name)
    {
        try {
            $obj = $this->find($id);
            $obj->setName($name);
            return $this->mapper->update($obj);
        } catch (Exception $e) {
            $this->handleException($e);
        }
    }
}
