<?php
namespace OCA\CeranaAdhesion\Service;

use Exception;
use OCP\AppFramework\Db\DoesNotExistException;
use OCP\AppFramework\Db\MultipleObjectsReturnedException;
use OCA\CeranaAdhesion\Db\AbstractMapper;

abstract class AbstractService
{

    /** @var AbstractMapper */
    protected $mapper;

    /**
     * Service abstract constructor
     *
     * @param AbstractMapper $mapper
     */
    public function __construct(AbstractMapper $mapper)
    {
        $this->mapper = $mapper;
    }

    /**
     * Find entity from id
     *
     * @param int $id
     * @return Entity
     */
    public function find($id)
    {
        try {
            return $this->mapper->find($id);
        } catch (Exception $e) {
            $this->handleException($e);
        }
    }

    /**
     * Find entity from page & size
     * 
     * @param int $page
     * @param int $size
     * @return array
     */
    public function findAll($page, $size): array
    {
        return $this->mapper->findAll($page, $size);
    }
    
    /**
     * Throw exception
     *
     * @param Exception $e
     * @throws AdhesionObjectNotFound
     * @throws Ambigous <DoesNotExistException, MultipleObjectsReturnedException>
     */
    protected function handleException(Exception $e): void
    {
        if ($e instanceof DoesNotExistException || $e instanceof MultipleObjectsReturnedException) {
            throw new AdhesionObjectNotFound($e->getMessage());
        } else {
            throw $e;
        }
    }

    /**
     * Delete entity from id
     *
     * @param int $id
     * @return Entity
     */
    public function delete($id)
    {
        try {
            $obj = $this->find($id);
            $this->mapper->delete($obj);
            return $obj;
        } catch (Exception $e) {
            $this->handleException($e);
        }
    }
}
    