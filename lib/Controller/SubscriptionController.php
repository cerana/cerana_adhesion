<?php
namespace OCA\CeranaAdhesion\Controller;

use OCP\IRequest;
use OCP\AppFramework\Http\DataResponse;
use OCA\CeranaAdhesion\Service\SubscriptionService;

class SubscriptionController extends AbstractControler
{

    public function __construct($appName, IRequest $request, SubscriptionService $service, $userId)
    {
        parent::__construct($appName, $request, $service, $userId);
    }

    /**
     *
     * @NoAdminRequired
     */
    public function statistics(): Array
    {
        return $this->service->statistics($this->request->getParam('structure', 0), $this->request->getParam('date', ''));
    }

    /**
     *
     * @NoAdminRequired
     */
    public function show(int $id): DataResponse
    {
        return $this->handleNotFound(function () use ($id) {
            if ($id != 0) {
                return $this->service->find($id);
            } else {
                return $this->service->search($this->request->getParam('adherent', 0), $this->request->getParam('dateref', '2000-01-01'));
            }
        });
    }

    /**
     *
     * @NoAdminRequired
     */
    public function create(int $adherent, int $typeadhesion, int $structure, string $dateref, string $comment, int $typepractice = 0, int $typeresponsability = 0): DataResponse
    {
        return new DataResponse($this->service->create($adherent, $typeadhesion, $structure, $dateref, $comment, $typepractice, $typeresponsability));
    }

    /**
     *
     * @NoAdminRequired
     */
    public function update(int $id, int $adherent, int $typeadhesion, int $structure, string $dateref, string $comment): DataResponse
    {
        return $this->handleNotFound(function () use ($id, $adherent, $typeadhesion, $structure, $dateref, $comment) {
            return $this->service->update($id, $adherent, $typeadhesion, $structure, $dateref, $comment);
        });
    }
}
