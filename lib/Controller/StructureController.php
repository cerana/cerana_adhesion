<?php
namespace OCA\CeranaAdhesion\Controller;

use OCP\IRequest;
use OCP\AppFramework\Http\DataResponse;
use OCA\CeranaAdhesion\Service\StructureService;

class StructureController extends AbstractControler
{

    public function __construct($appName, IRequest $request, StructureService $service, $userId)
    {
        parent::__construct($appName, $request, $service, $userId);
    }

    /**
     *
     * @NoAdminRequired
     */
    public function create(string $name, string $phone1, string $phone2, string $email, string $website, string $address, string $postalcode, string $city, string $comment, int $level, int $parent = null): DataResponse
    {
        return new DataResponse($this->service->create($name, $phone1, $phone2, $email, $website, $address, $postalcode, $city, $comment, $level, $parent));
    }

    /**
     *
     * @NoAdminRequired
     */
    public function update(int $id, string $name, string $phone1, string $phone2, string $email, string $website, string $address, string $postalcode, string $city, string $comment, int $level, int $parent = null): DataResponse
    {
        return $this->handleNotFound(function () use ($id, $name, $phone1, $phone2, $email, $website, $address, $postalcode, $city, $comment, $level, $parent) {
            return $this->service->update($id, $name, $phone1, $phone2, $email, $website, $address, $postalcode, $city, $comment, $level, $parent);
        });
    }
}
