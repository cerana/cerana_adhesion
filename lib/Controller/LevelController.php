<?php
namespace OCA\CeranaAdhesion\Controller;

use OCP\IRequest;
use OCP\AppFramework\Http\DataResponse;
use OCA\CeranaAdhesion\Service\LevelService;

class LevelController extends AbstractControler
{

    public function __construct($appName, IRequest $request, LevelService $service, $userId)
    {
        parent::__construct($appName, $request, $service, $userId);
    }

    /**
     *
     * @NoAdminRequired
     */
    public function create(int $level, string $name, string $comment): DataResponse
    {
        return new DataResponse($this->service->create($level, $name, $comment));
    }

    /**
     *
     * @NoAdminRequired
     */
    public function update(int $id, int $level, string $name, string $comment): DataResponse
    {
        return $this->handleNotFound(function () use ($id, $level, $name, $comment) {
            return $this->service->update($id, $level, $name, $comment);
        });
    }
}
