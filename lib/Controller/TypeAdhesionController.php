<?php
namespace OCA\CeranaAdhesion\Controller;

use OCP\IRequest;
use OCP\AppFramework\Http\DataResponse;
use OCA\CeranaAdhesion\Service\TypeAdhesionService;

class TypeAdhesionController extends AbstractControler
{

    public function __construct($appName, IRequest $request, TypeAdhesionService $service, $userId)
    {
        parent::__construct($appName, $request, $service, $userId);
    }

    /**
     *
     * @NoAdminRequired
     */
    public function create(string $name): DataResponse
    {
        return new DataResponse($this->service->create($name));
    }

    /**
     *
     * @NoAdminRequired
     */
    public function update(int $id, string $name): DataResponse
    {
        return $this->handleNotFound(function () use ($id, $name) {
            return $this->service->update($id, $name);
        });
    }
}
