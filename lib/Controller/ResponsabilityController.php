<?php
namespace OCA\CeranaAdhesion\Controller;

use OCP\IRequest;
use OCP\AppFramework\Http\DataResponse;
use OCA\CeranaAdhesion\Service\ResponsabilityService;

class ResponsabilityController extends AbstractControler
{

    public function __construct($appName, IRequest $request, ResponsabilityService $service, $userId)
    {
        parent::__construct($appName, $request, $service, $userId);
    }

    /**
     *
     * @NoAdminRequired
     */
    public function index(): DataResponse
    {
        $structure = $this->request->getParam('structure', 0);
        $date = $this->request->getParam('date', '');
        if (($structure == 0) && ($date == '')) {
            return new DataResponse($this->service->findAllEx($this->request->getParam('subscription', 0), $this->request->getParam('page', 0), $this->request->getParam('size', 25)));
        } else {
            return new DataResponse($this->service->findAdherents($structure, $date, $this->request->getParam('page', 0), $this->request->getParam('size', 25)));
        }
    }

    /**
     *
     * @NoAdminRequired
     */
    public function create(int $subscription, int $typeresponsability, int $structure, string $comment): DataResponse
    {
        return new DataResponse($this->service->create($subscription, $typeresponsability, $structure, $comment));
    }

    /**
     *
     * @NoAdminRequired
     */
    public function update(int $id, int $subscription, int $typeresponsability, int $structure, string $comment): DataResponse
    {
        return $this->handleNotFound(function () use ($id, $subscription, $typeresponsability, $structure, $comment) {
            return $this->service->update($id, $subscription, $typeresponsability, $structure, $comment);
        });
    }
}
