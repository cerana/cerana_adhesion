<?php
namespace OCA\CeranaAdhesion\Controller;

use OCP\IRequest;
use OCP\AppFramework\Http\DataResponse;
use OCA\CeranaAdhesion\Service\AdherentService;

class AdherentController extends AbstractControler
{

    public function __construct($appName, IRequest $request, AdherentService $service, $userId)
    {
        parent::__construct($appName, $request, $service, $userId);
    }

    /**
     *
     * @NoAdminRequired
     */
    public function index(): DataResponse
    {
        $structure = $this->request->getParam('structure', 0);
        $date = $this->request->getParam('date', '');
        if (($structure == 0) && ($date == '')) {
            return new DataResponse($this->service->findAllEx($this->request->getParam('search', ''), $this->request->getParam('page', 0), $this->request->getParam('size', 25)));
        } else {
            return new DataResponse($this->service->findAdherents($structure, $date, $this->request->getParam('page', 0), $this->request->getParam('size', 25)));
        }
    }

    /**
     *
     * @NoAdminRequired
     */
    public function show(int $id): DataResponse
    {
        return $this->handleNotFound(function () use ($id) {
            return $this->service->findEx($id, $this->userId);
        });
    }

    /**
     *
     * @NoAdminRequired
     */
    public function create(int $civility, string $lastname, string $firstname, string $birthdaydate, string $birthdayplace, string $email, string $address, string $postalcode, string $city, string $country, string $phone1, string $phone2, string $comment, string $photoid = null, string $dateref = "", int $structure = 0, int $typeadhesion = 0, int $typepractice = 0, int $typeresponsability = 0): DataResponse
    {
        return new DataResponse($this->service->create($civility, $lastname, $firstname, $birthdaydate, $birthdayplace, $email, $address, $postalcode, $city, $country, $phone1, $phone2, $comment, $photoid, $dateref, $structure, $typeadhesion, $typepractice, $typeresponsability));
    }

    /**
     *
     * @NoAdminRequired
     */
    public function update(int $id, int $civility, string $lastname, string $firstname, string $birthdaydate, string $birthdayplace, string $email, string $address, string $postalcode, string $city, string $country, string $phone1, string $phone2, string $comment, string $photoid = null): DataResponse
    {
        return $this->handleNotFound(function () use ($id, $civility, $lastname, $firstname, $birthdaydate, $birthdayplace, $email, $address, $postalcode, $city, $country, $phone1, $phone2, $comment, $photoid) {
            return $this->service->update($id, $civility, $lastname, $firstname, $birthdaydate, $birthdayplace, $email, $address, $postalcode, $city, $country, $phone1, $phone2, $comment, $photoid);
        });
    }
    
    /**
     *
     * @NoAdminRequired
     */
    public function connection(int $id, int $mode) {
        return $this->handleNotFound(function () use ($id, $mode) {
            return $this->service->connection($id, $mode);
        });            
    }
}
