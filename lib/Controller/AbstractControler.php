<?php
namespace OCA\CeranaAdhesion\Controller;

use OCP\IRequest;
use OCP\AppFramework\Http\DataResponse;
use OCP\AppFramework\Controller;
use OCA\CeranaAdhesion\Service\AbstractService;

abstract class AbstractControler extends Controller
{

    /** @var AbstractService */
    protected $service;

    /** @var string */
    protected $userId;
    
    use Errors;

    public function __construct($appName, IRequest $request, AbstractService $service, $userId)
    {
        parent::__construct($appName, $request);
        $this->service = $service;
        $this->userId = $userId;
    }

    /**
     *
     * @NoAdminRequired
     */
    public function index(): DataResponse
    {
        return new DataResponse($this->service->findAll($this->request->getParam('page', 0), $this->request->getParam('size', 25)));
    }

    /**
     *
     * @NoAdminRequired
     */
    public function show(int $id): DataResponse
    {
        return $this->handleNotFound(function () use ($id) {
            return $this->service->find($id);
        });
    }

    /**
     *
     * @NoAdminRequired
     */
    public function destroy(int $id): DataResponse
    {
        return $this->handleNotFound(function () use ($id) {
            return $this->service->delete($id);
        });
    }
}