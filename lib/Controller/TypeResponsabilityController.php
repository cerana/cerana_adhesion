<?php
namespace OCA\CeranaAdhesion\Controller;

use OCP\IRequest;
use OCA\CeranaAdhesion\Service\TypeResponsabilityService;
use OCP\AppFramework\Http\DataResponse;

class TypeResponsabilityController extends AbstractControler
{

    public function __construct($appName, IRequest $request, TypeResponsabilityService $service, $userId)
    {
        parent::__construct($appName, $request, $service, $userId);
    }

    /**
     *
     * @NoAdminRequired
     */
    public function create(int $level, string $name): DataResponse
    {
        return new DataResponse($this->service->create($level, $name));
    }

    /**
     *
     * @NoAdminRequired
     */
    public function update(int $id, int $level, string $name): DataResponse
    {
        return $this->handleNotFound(function () use ($id, $level, $name) {
            return $this->service->update($id, $level, $name);
        });
    }
}
