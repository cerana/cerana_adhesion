<?php
declare(strict_types = 1);
namespace OCA\CeranaAdhesion\Migration;

use Closure;
use OCP\DB\ISchemaWrapper;
use OCP\Migration\SimpleMigrationStep;
use OCP\Migration\IOutput;

class Version000001Date20200809113600 extends SimpleMigrationStep
{

    /**
     *
     * @param IOutput $output
     * @param Closure $schemaClosure
     *            The `\Closure` returns a `ISchemaWrapper`
     * @param array $options
     * @return null|ISchemaWrapper
     */
    public function changeSchema(IOutput $output, Closure $schemaClosure, array $options)
    {
        /** @var ISchemaWrapper $schema */
        $schema = $schemaClosure();

        if (! $schema->hasTable('cerana_level')) {
            $table = $schema->createTable('cerana_level');
            $table->addColumn('id', 'integer', [
                'autoincrement' => true,
                'notnull' => true
			]);
			$table->addColumn('level', 'integer', [
                'notnull' => true
			]);
            $table->addColumn('name', 'string', [
                'notnull' => true,
                'length' => 100
            ]);
			$table->addColumn('comment', 'text', [
                'notnull' => true
            ]);
			$table->setPrimaryKey([
                'id'
            ]);
		}

        if (! $schema->hasTable('cerana_structure')) {
            $table = $schema->createTable('cerana_structure');
            $table->addColumn('id', 'integer', [
                'autoincrement' => true,
                'notnull' => true
			]);
			
            $table->addColumn('name', 'string', [
                'notnull' => true,
                'length' => 100
            ]);
            $table->addColumn('phone1', 'string', [
                'notnull' => true,
                'length' => 16
            ]);
            $table->addColumn('phone2', 'string', [
                'notnull' => true,
                'length' => 16
            ]);
            $table->addColumn('email', 'string', [
                'notnull' => true,
                'length' => 200
            ]);
            $table->addColumn('website', 'string', [
                'notnull' => true,
                'length' => 200
            ]);
            $table->addColumn('address', 'text', [
                'notnull' => true
            ]);
            $table->addColumn('postalcode', 'string', [
                'notnull' => true,
                'length' => 8
            ]);
            $table->addColumn('city', 'string', [
                'notnull' => true,
                'length' => 200
            ]);
            $table->addColumn('comment', 'text', [
                'notnull' => true
			]);
            $table->addColumn('level', 'integer', [
                'notnull' => true
            ]);
            $table->addColumn('parent', 'integer', [
                'notnull' => false
			]);

            $table->setPrimaryKey([
                'id'
            ]);
            $table->addForeignKeyConstraint($schema->getTable('cerana_level')->getName(), [
                'level'
            ], [
                'id'
            ], [
                'onDelete' => 'NO ACTION'
            ], "FK_structure_level");
            $table->addForeignKeyConstraint($schema->getTable('cerana_structure')->getName(), [
                'parent'
            ], [
                'id'
            ], [
                'onDelete' => 'SET NULL'
            ], "FK_structure_structure");

		}

		return $schema;
    }
}
