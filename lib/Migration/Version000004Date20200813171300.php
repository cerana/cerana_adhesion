<?php
declare(strict_types = 1);
namespace OCA\CeranaAdhesion\Migration;

use Closure;
use OCP\DB\ISchemaWrapper;
use OCP\Migration\SimpleMigrationStep;
use OCP\Migration\IOutput;

class Version000004Date20200813171300 extends SimpleMigrationStep
{

    /**
     *
     * @param IOutput $output
     * @param Closure $schemaClosure
     *            The `\Closure` returns a `ISchemaWrapper`
     * @param array $options
     * @return null|ISchemaWrapper
     */
    public function changeSchema(IOutput $output, Closure $schemaClosure, array $options)
    {
        /** @var ISchemaWrapper $schema */
        $schema = $schemaClosure();
        
        if (! $schema->hasTable('cerana_practice')) {
            $table = $schema->createTable('cerana_practice');
            $table->addColumn('id', 'integer', [
                'autoincrement' => true,
                'notnull' => true
            ]);
            $table->addColumn('subscription', 'integer', [
                'notnull' => true
			]);            
            $table->addColumn('typepractice', 'integer', [
                'notnull' => true
			]);
            $table->addColumn('comment', 'text', [
                'notnull' => true
            ]);
            $table->setPrimaryKey([
                'id'
            ]);
            $table->addForeignKeyConstraint($schema->getTable('cerana_subscription')->getName(), [
                'subscription'
            ], [
                'id'
            ], [
                'onDelete' => 'CASCADE'
            ], "FK_practice_subscription");            
            $table->addForeignKeyConstraint($schema->getTable('cerana_typepractice')->getName(), [
                'typepractice'
            ], [
                'id'
            ], [
                'onDelete' => 'NO ACTION'
            ], "FK_practice_typepractice");
		}

        if (! $schema->hasTable('cerana_responsability')) {
            $table = $schema->createTable('cerana_responsability');
            $table->addColumn('id', 'integer', [
                'autoincrement' => true,
                'notnull' => true
            ]);
            $table->addColumn('subscription', 'integer', [
                'notnull' => true
			]);                        
            $table->addColumn('typeresponsability', 'integer', [
                'notnull' => true
			]);
            $table->addColumn('structure', 'integer', [
                'notnull' => true
			]);
            $table->addColumn('comment', 'text', [
                'notnull' => true
            ]);
            $table->setPrimaryKey([
                'id'
			]);
            $table->addForeignKeyConstraint($schema->getTable('cerana_subscription')->getName(), [
                'subscription'
            ], [
                'id'
            ], [
                'onDelete' => 'CASCADE'
            ], "FK_respo_subscription");            
            $table->addForeignKeyConstraint($schema->getTable('cerana_typerespo')->getName(), [
                'typeresponsability'
            ], [
                'id'
            ], [
                'onDelete' => 'NO ACTION'
            ], "FK_respo_typerespo");
            $table->addForeignKeyConstraint($schema->getTable('cerana_structure')->getName(), [
                'structure'
            ], [
                'id'
            ], [
                'onDelete' => 'NO ACTION'
            ], "FK_respo_structure");
		}

        return $schema;
    }
}
