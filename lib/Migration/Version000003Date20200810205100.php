<?php
declare(strict_types = 1);
namespace OCA\CeranaAdhesion\Migration;

use Closure;
use OCP\DB\ISchemaWrapper;
use OCP\Migration\SimpleMigrationStep;
use OCP\Migration\IOutput;

class Version000003Date20200810205100 extends SimpleMigrationStep
{

    /**
     *
     * @param IOutput $output
     * @param Closure $schemaClosure
     *            The `\Closure` returns a `ISchemaWrapper`
     * @param array $options
     * @return null|ISchemaWrapper
     */
    public function changeSchema(IOutput $output, Closure $schemaClosure, array $options)
    {
        /** @var ISchemaWrapper $schema */
        $schema = $schemaClosure();
        
        if (! $schema->hasTable('cerana_subscription')) {
            $table = $schema->createTable('cerana_subscription');
            $table->addColumn('id', 'integer', [
                'autoincrement' => true,
                'notnull' => true
			]);
            $table->addColumn('adherent', 'integer', [
                'notnull' => true
            ]);
            $table->addColumn('typeadhesion', 'integer', [
                'notnull' => true
            ]);
            $table->addColumn('structure', 'integer', [
                'notnull' => true
            ]);
            $table->addColumn('dateref', 'date', [
                'notnull' => true
            ]);
            $table->addColumn('comment', 'text', [
                'notnull' => true
            ]);			
            $table->setPrimaryKey([
                'id'
			]);
            $table->addForeignKeyConstraint($schema->getTable('cerana_typeadhesion')->getName(), [
                'typeadhesion'
            ], [
                'id'
            ], [
                'onDelete' => 'NO ACTION'
            ], "FK_adherent_subscription");
            $table->addForeignKeyConstraint($schema->getTable('cerana_adherent')->getName(), [
                'adherent'
            ], [
                'id'
            ], [
                'onDelete' => 'NO ACTION'
            ], "FK_adherent_subscription");
            $table->addForeignKeyConstraint($schema->getTable('cerana_structure')->getName(), [
                'structure'
            ], [
                'id'
            ], [
                'onDelete' => 'NO ACTION'
            ], "FK_structure_subscription");			
        }
        return $schema;
    }
}
