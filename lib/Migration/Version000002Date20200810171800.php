<?php
declare(strict_types = 1);
namespace OCA\CeranaAdhesion\Migration;

use Closure;
use OCP\DB\ISchemaWrapper;
use OCP\Migration\SimpleMigrationStep;
use OCP\Migration\IOutput;

class Version000002Date20200810171800 extends SimpleMigrationStep
{

    /**
     *
     * @param IOutput $output
     * @param Closure $schemaClosure
     *            The `\Closure` returns a `ISchemaWrapper`
     * @param array $options
     * @return null|ISchemaWrapper
     */
    public function changeSchema(IOutput $output, Closure $schemaClosure, array $options)
    {
        /** @var ISchemaWrapper $schema */
        $schema = $schemaClosure();

        if (! $schema->hasTable('cerana_typeadhesion')) {
            $table = $schema->createTable('cerana_typeadhesion');
            $table->addColumn('id', 'integer', [
                'autoincrement' => true,
                'notnull' => true
			]);
            $table->addColumn('name', 'string', [
                'notnull' => true,
                'length' => 100
            ]);
            $table->setPrimaryKey([
                'id'
            ]);
		}

        if (! $schema->hasTable('cerana_typerespo')) {
            $table = $schema->createTable('cerana_typerespo');
            $table->addColumn('id', 'integer', [
                'autoincrement' => true,
                'notnull' => true
			]);
            $table->addColumn('level', 'integer', [
                'notnull' => true
            ]);
            $table->addColumn('name', 'string', [
                'notnull' => true,
                'length' => 100
            ]);
            $table->setPrimaryKey([
                'id'
            ]);
            $table->addForeignKeyConstraint($schema->getTable('cerana_level')->getName(), [
                'level'
            ], [
                'id'
            ], [
                'onDelete' => 'NO ACTION'
            ], "FK_typeadhesion_level");
		}

        if (! $schema->hasTable('cerana_typepractice')) {
            $table = $schema->createTable('cerana_typepractice');
            $table->addColumn('id', 'integer', [
                'autoincrement' => true,
                'notnull' => true
			]);
            $table->addColumn('name', 'string', [
                'notnull' => true,
                'length' => 100
            ]);
            $table->setPrimaryKey([
                'id'
            ]);
		}

		return $schema;
    }
}
				