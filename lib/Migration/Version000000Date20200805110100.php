<?php
declare(strict_types = 1);
namespace OCA\CeranaAdhesion\Migration;

use Closure;
use OCP\DB\ISchemaWrapper;
use OCP\Migration\SimpleMigrationStep;
use OCP\Migration\IOutput;

class Version000000Date20200805110100 extends SimpleMigrationStep
{

    /**
     *
     * @param IOutput $output
     * @param Closure $schemaClosure
     *            The `\Closure` returns a `ISchemaWrapper`
     * @param array $options
     * @return null|ISchemaWrapper
     */
    public function changeSchema(IOutput $output, Closure $schemaClosure, array $options)
    {
        /** @var ISchemaWrapper $schema */
        $schema = $schemaClosure();
        
        if (! $schema->hasTable('cerana_adherent')) {
            $table = $schema->createTable('cerana_adherent');
            $table->addColumn('id', 'integer', [
                'autoincrement' => true,
                'notnull' => true
            ]);
            
            $table->addColumn('num', 'string', [
                'notnull' => true,
                'length' => 12
            ]);
            $table->addColumn('civility', 'smallint', [
                'notnull' => true
            ]);
            $table->addColumn('lastname', 'string', [
                'notnull' => true,
                'length' => 50
            ]);
            $table->addColumn('firstname', 'string', [
                'notnull' => true,
                'length' => 50
            ]);
            $table->addColumn('birthdaydate', 'date', [
                'notnull' => true
            ]);
            $table->addColumn('birthdayplace', 'string', [
                'notnull' => true,
                'length' => 200
            ]);
            $table->addColumn('email', 'string', [
                'notnull' => true,
                'length' => 200
            ]);
            $table->addColumn('address', 'text', [
                'notnull' => true
            ]);
            $table->addColumn('postalcode', 'string', [
                'notnull' => true,
                'length' => 8
            ]);
            $table->addColumn('city', 'string', [
                'notnull' => true,
                'length' => 200
            ]);
            $table->addColumn('country', 'string', [
                'notnull' => true,
                'length' => 2
            ]);
            $table->addColumn('phone1', 'string', [
                'notnull' => true,
                'length' => 16
            ]);
            $table->addColumn('phone2', 'string', [
                'notnull' => true,
                'length' => 16
            ]);
            $table->addColumn('comment', 'text', [
                'notnull' => true
            ]);
            $table->addColumn('photoid', 'text', [
                'notnull' => true,
            ]);
            $table->addColumn('uid', 'string', [
                'notnull' => false
            ]);            
            $table->setPrimaryKey([
                'id'
            ]);
            $table->addForeignKeyConstraint('oc_users', [
                'uid'
            ], [
                'uid'
            ], [
                'onDelete' => 'SET NULL'
            ], "FK_adherent_user");
        }
        return $schema;
    }
}
