<?php
namespace OCA\CeranaAdhesion\Db;

use JsonSerializable;
use OCP\AppFramework\Db\Entity;

class Responsability extends Entity implements JsonSerializable
{
    protected $subscription;

    protected $structure;

    protected $typeresponsability;

    protected $comment;

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->id,
            'subscription' => intval($this->subscription),
            'structure' => intval($this->structure),
            'typeresponsability' => intval($this->typeresponsability),
            'comment' => $this->comment,
        ];
    }
}
