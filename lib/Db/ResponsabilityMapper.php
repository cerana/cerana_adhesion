<?php
namespace OCA\CeranaAdhesion\Db;

use OCP\AppFramework\Db\DoesNotExistException;
use OCP\AppFramework\Db\Entity;
use OCP\DB\QueryBuilder\IQueryBuilder;
use OCP\IDBConnection;

class ResponsabilityMapper extends AbstractMapper
{

    public function __construct(IDBConnection $db)
    {
        parent::__construct($db, 'cerana_responsability', Responsability::class);
    }

    /**
     *
     * @param int $id
     * @return Entity|Responsability
     * @throws \OCP\AppFramework\Db\MultipleObjectsReturnedException
     * @throws DoesNotExistException
     */
    public function find(int $id): Responsability
    {
        /* @var $qb IQueryBuilder */
        $qb = $this->db->getQueryBuilder();
        $qb->select('*')
            ->from($this->getTableName())
            ->where($qb->expr()
            ->eq('id', $qb->createNamedParameter($id, IQueryBuilder::PARAM_INT)));
       return $this->findEntity($qb);
    }
   
    public function statistics($structure, $dateref): array
    {
        $qb = $this->db->getQueryBuilder();
        $qb->select('r.typeresponsability')
            ->selectAlias($qb->func()->count('typeresponsability'), 'nb')
            ->from($this->getTableName(), 'r')
            ->leftJoin('r', 'cerana_subscription', 'sub', 'r.subscription = sub.id')            
            ->where($qb->expr()->eq('sub.id', 'r.subscription'));
        $qb->where($qb->expr()->eq('sub.dateref', $qb->createNamedParameter($dateref)));
        if ($structure != 0) {
            $qb->andWhere($qb->expr()->eq('r.structure', $qb->createNamedParameter($structure, IQueryBuilder::PARAM_INT)));
        }
        $qb->groupBy('typeresponsability');
        $total = 0;
        $result=$this->findArray($qb);
        foreach($result as $value) {
            $total += intval($value['nb']);
        }
        $result[] = array('typeresponsability'=>0, 'nb'=>$total);
        return $result;
    }
    
    /**
     *
     * @param string $nom
     * @param string $prenom
     * @return array
     */
    public function findAll($subscription, $page, $size, $structureid=0): array
    {
        /* @var $qb IQueryBuilder */
        $qb = $this->db->getQueryBuilder();
        $qb->select('*')
            ->from($this->getTableName())
            ->orderBy('structure')
            ->addOrderBy('typeresponsability');
        if ($subscription != 0) {
            $qb->where($qb->expr()->eq('subscription', $qb->createNamedParameter($subscription, IQueryBuilder::PARAM_INT)));
        }
        if ($structureid != 0) {
            $qb->andWhere($qb->expr()->eq('structure', $qb->createNamedParameter($structureid, IQueryBuilder::PARAM_INT)));
        }
        return $this->findEntitiesWithPage($qb, $page, $size);
    }

    /**
     *
     * @param int $structureid, 
     * @param string $date, 
     * @param int $page
     * @param int $size
     * @return array
     */
    public function findAdherents($structureid, $date, $page, $size): array
    {
        /* @var $qb IQueryBuilder */
        $qb = $this->db->getQueryBuilder();
        $qb->select('t.*')
            ->selectAlias('a.id', 'adherentid')
            ->selectAlias('a.num', 'num')
            ->selectAlias('a.civility', 'civility')
            ->selectAlias('a.firstname', 'firstname')
            ->selectAlias('a.lastname', 'lastname')
            ->selectAlias('a.birthdaydate', 'birthdaydate')
            ->selectAlias('a.email', 'email')
	        ->from($this->getTableName(), 't')
            ->leftJoin('t', 'cerana_subscription', 's', 't.subscription = s.id')
            ->leftJoin('s', 'cerana_adherent', 'a', 's.adherent = a.id')
            ->orderBy('typeresponsability')
            ->addOrderBy('firstname')
            ->addOrderBy('lastname')
            ->where($qb->expr()->eq('t.structure', $qb->createNamedParameter($structureid, IQueryBuilder::PARAM_INT)))
            ->andWhere($qb->expr()->eq('s.dateref', $qb->createNamedParameter($date)));
        return $this->findWithPage($qb, $page, $size);
    }

}
