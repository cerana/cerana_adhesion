<?php
namespace OCA\CeranaAdhesion\Db;

use JsonSerializable;
use OCP\AppFramework\Db\Entity;

class Subscription extends Entity implements JsonSerializable
{

    protected $adherent;

    protected $typeadhesion;

    protected $structure;

    protected $dateref;

    protected $comment;

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->id,
            'adherent' => $this->adherent,
            'typeadhesion' => intval($this->typeadhesion),
            'structure' => intval($this->structure),
            'dateref' => $this->dateref,
            'comment' => $this->comment,
        ];
    }
}
