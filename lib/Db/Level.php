<?php
namespace OCA\CeranaAdhesion\Db;

use JsonSerializable;
use OCP\AppFramework\Db\Entity;

class Level extends Entity implements JsonSerializable
{

    protected $level;

    protected $name;

    protected $comment;

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->id,
            'level' => intval($this->level),
            'name' => $this->name,
            'comment' => $this->comment,
        ];
    }
}
