<?php
namespace OCA\CeranaAdhesion\Db;

use OCP\AppFramework\Db\DoesNotExistException;
use OCP\AppFramework\Db\Entity;
use OCP\DB\QueryBuilder\IQueryBuilder;
use OCP\IDBConnection;

class TypeAdhesionMapper extends AbstractMapper
{

    public function __construct(IDBConnection $db)
    {
        parent::__construct($db, 'cerana_typeadhesion', TypeAdhesion::class);
    }

    /**
     *
     * @param int $id
     * @return Entity|TypeAdhesion
     * @throws \OCP\AppFramework\Db\MultipleObjectsReturnedException
     * @throws DoesNotExistException
     */
    public function find(int $id): TypeAdhesion
    {
        /* @var $qb IQueryBuilder */
        $qb = $this->db->getQueryBuilder();
        $qb->select('*')
            ->from($this->getTableName())
            ->where($qb->expr()
            ->eq('id', $qb->createNamedParameter($id, IQueryBuilder::PARAM_INT)));
        return $this->findEntity($qb);
    }
   
    /**
     *
     * @param string $nom
     * @param string $prenom
     * @return array
     */
    public function findAll($page, $size): array
    {
        /* @var $qb IQueryBuilder */
        $qb = $this->db->getQueryBuilder();
        $qb->select('*')
            ->from($this->getTableName())
            ->orderBy('name');
        return $this->findEntitiesWithPage($qb, $page, $size);
    }

}
