<?php
namespace OCA\CeranaAdhesion\Db;

use OCP\AppFramework\Db\DoesNotExistException;
use OCP\AppFramework\Db\Entity;
use OCP\DB\QueryBuilder\IQueryBuilder;
use OCP\IDBConnection;

class PracticeMapper extends AbstractMapper
{

    public function __construct(IDBConnection $db)
    {
        parent::__construct($db, 'cerana_practice', Practice::class);
    }

    /**
     *
     * @param int $id
     * @return Entity|Practice
     * @throws \OCP\AppFramework\Db\MultipleObjectsReturnedException
     * @throws DoesNotExistException
     */
    public function find(int $id): Practice
    {
        /* @var $qb IQueryBuilder */
        $qb = $this->db->getQueryBuilder();
        $qb->select('*')
            ->from($this->getTableName())
            ->where($qb->expr()
            ->eq('id', $qb->createNamedParameter($id, IQueryBuilder::PARAM_INT)));
        return $this->findEntity($qb);
    }
    
    public function statistics($structure, $dateref): array
    {
        $qb = $this->db->getQueryBuilder();
        $qb->select('p.typepractice')
            ->selectAlias($qb->func()->count('typepractice'), 'nb')
            ->from($this->getTableName(), 'p')
            ->leftJoin('p', 'cerana_subscription', 'sub', 'p.subscription = sub.id')
            ->where($qb->expr()->eq('sub.id', 'p.subscription'));
        $qb->where($qb->expr()->eq('sub.dateref', $qb->createNamedParameter($dateref)));
        if ($structure != 0) {
            $qb->andWhere($qb->expr()->eq('sub.structure', $qb->createNamedParameter($structure, IQueryBuilder::PARAM_INT)));
        }
        $qb->groupBy('typepractice');
        $total = 0;
        $result=$this->findArray($qb);
        foreach($result as $value) {
            $total += intval($value['nb']);
        }
        $result[] = array('typepractice'=>0, 'nb'=>$total);
        return $result;
    }
   
    /**
     *
     * @param int $subscription, 
     * @param int $page
     * @param int $size
     * @return array
     */
    public function findAll($subscription, $page, $size): array
    {
        /* @var $qb IQueryBuilder */
        $qb = $this->db->getQueryBuilder();
        $qb->select('*')
            ->from($this->getTableName())
            ->orderBy('typepractice');
        if ($subscription != 0) {
            $qb->where($qb->expr()->eq('subscription', $qb->createNamedParameter($subscription, IQueryBuilder::PARAM_INT)));
        }
        return $this->findEntitiesWithPage($qb, $page, $size);
    }

    /**
     *
     * @param int $structureid, 
     * @param string $date, 
     * @param int $page
     * @param int $size
     * @return array
     */
    public function findAdherents($structureid, $date, $typepractice, $page, $size): array
    {
        /* @var $qb IQueryBuilder */
        $qb = $this->db->getQueryBuilder();
        $qb->select('t.*')
            ->selectAlias('a.id', 'adherentid')
            ->selectAlias('a.num', 'num')
            ->selectAlias('a.civility', 'civility')
            ->selectAlias('a.firstname', 'firstname')
            ->selectAlias('a.lastname', 'lastname')
            ->selectAlias('a.birthdaydate', 'birthdaydate')
            ->selectAlias('a.email', 'email')
	        ->from($this->getTableName(), 't')
            ->leftJoin('t', 'cerana_subscription', 's', 't.subscription = s.id')
            ->leftJoin('s', 'cerana_adherent', 'a', 's.adherent = a.id')
            ->orderBy('typepractice')
            ->addOrderBy('firstname')
            ->addOrderBy('lastname')
            ->where($qb->expr()->eq('s.structure', $qb->createNamedParameter($structureid, IQueryBuilder::PARAM_INT)))
            ->andWhere($qb->expr()->eq('s.dateref', $qb->createNamedParameter($date)));
        if ($typepractice != 0) {
            $qb->andWhere($qb->expr()->eq('t.typepractice', $qb->createNamedParameter($typepractice, IQueryBuilder::PARAM_INT)));
        }
        return $this->findWithPage($qb, $page, $size);
    }

}
