<?php
namespace OCA\CeranaAdhesion\Db;

use OCP\AppFramework\Db\DoesNotExistException;
use OCP\AppFramework\Db\Entity;
use OCP\DB\QueryBuilder\IQueryBuilder;
use OC\DB\QueryBuilder\QueryFunction;
use OCP\IDBConnection;

class AdherentMapper extends AbstractMapper
{

    private $logger;

    /**
     * Constructor
     *
     * @param IDBConnection $db
     */
    public function __construct(IDBConnection $db)
    {
        parent::__construct($db, 'cerana_adherent', Adherent::class);
    }

    /**
     * Creator
     * 
     * @return SubscriptionMapper
     */
    public function createSubscription()
    {
        return new SubscriptionMapper($this->getDb());
    }

    /**
     * Search an Adherent with its 'id'
     *
     * @param int $id
     * @return Entity|Adherent
     * @throws \OCP\AppFramework\Db\MultipleObjectsReturnedException
     * @throws DoesNotExistException
     */
    public function find(int $id): array
    {
        /* @var $qb IQueryBuilder */
        $qb = $this->db->getQueryBuilder();
        $qb->select('*')
            ->from($this->getTableName())
            ->where($qb->expr()
            ->eq('id', $qb->createNamedParameter($id, IQueryBuilder::PARAM_INT)));
        return $this->findOneQuery($qb);
    }

    /**
     * Search an Adherent associate with connection user '$userId'
     *
     * @param string $userId
     * @return Entity|Adherent
     * @throws \OCP\AppFramework\Db\MultipleObjectsReturnedException
     * @throws DoesNotExistException
     */
    public function findCurrent(string $userId): array
    {
        /* @var $qb IQueryBuilder */
        $qb = $this->db->getQueryBuilder();
        $qb->select('*')
            ->from($this->getTableName())
            ->where($qb->expr()
            ->eq('uid', $qb->createNamedParameter($userId)));
        return $this->findOneQuery($qb);
    }

    /**
     * Search all Adherent with criteria $search - pagination with $page/$size
     *
     * @param string $search
     * @param int $page
     * @param int $size
     * @return array
     */
    public function findAll($search, $page, $size): array
    {
        /* @var $qb IQueryBuilder */
        $qb = $this->db->getQueryBuilder();
        $qb->select('*')
            ->from($this->getTableName())
            ->orderBy('lastname')
            ->addOrderBy('firstname');
        if ($search != '') {
            $where_statement = $qb->expr()->andX();
            $search_list = explode(" ", $search);
            for ($search_idx = 0; $search_idx < count($search_list); $search_idx ++) {
                $search_item = $qb->createNamedParameter("%" . $search_list[$search_idx] . "%");
                $sub_statement = $qb->expr()->orx();
                $sub_statement->add($qb->expr()
                    ->iLike('num', $search_item));
                $sub_statement->add($qb->expr()
                    ->iLike('lastname', $search_item));
                $sub_statement->add($qb->expr()
                    ->iLike('firstname', $search_item));
                $sub_statement->add($qb->expr()
                    ->iLike('email', $search_item));
                $sub_statement->add($qb->expr()
                    ->iLike('postalcode', $search_item));
                $sub_statement->add($qb->expr()
                    ->iLike('city', $search_item));
                $sub_statement->add($qb->expr()
                    ->iLike('comment', $search_item));
                $where_statement->add($sub_statement);
            }
            $qb->where($where_statement);
        }
        return $this->findEntitiesWithPage($qb, $page, $size);
    }

    /**
     * Search all Adherent with subscription in date '$date' and in structure '$structureid' - pagination with $page/$size
     *
     * @param int $structureid,
     * @param string $date,
     * @param int $page
     * @param int $size
     * @return array
     */
    public function findAdherents(int $structureid, string $date, int $page, int $size): array
    {
        /* @var $qb_final IQueryBuilder */
        $qb_final = $this->db->getQueryBuilder();
        $sql_subquery = "SELECT a.*, ";
        $sql_subquery .= "(SELECT COUNT(*) FROM " . $qb_final->getTableName("cerana_practice") . " p WHERE (p.subscription = sub.id)) as nb_practice, ";
        $sql_subquery .= "(SELECT COUNT(*) FROM " . $qb_final->getTableName("cerana_responsability") . " r WHERE (r.subscription = sub.id) AND (r.structure=sub.structure)) as nb_responsability ";
        $sql_subquery .= "FROM " . $qb_final->getTableName("cerana_adherent") . " a, " . $qb_final->getTableName("cerana_subscription") . " sub ";
        $sql_subquery .= "WHERE (sub.adherent = a.id) AND (sub.structure = " . $structureid . ") AND (sub.dateref = '" . $date . "')";
        
        $qb_final->select('*')
            ->from(new QueryFunction('(' . $sql_subquery . ')'), 'list')
            ->addOrderBy('firstname')
            ->addOrderBy('lastname')
            ->Where('nb_practice=0')
            ->andWhere('nb_responsability=0');
        return $this->findWithPage($qb_final, $page, $size);
    }

    /**
     * Get next value for 'num'
     *
     * @return int
     */
    public function getNextNum(): int
    {
        $qb = $this->db->getQueryBuilder();
        $qb->selectAlias($qb->func()
            ->substring('num', $qb->createNamedParameter(2, $qb::PARAM_INT)), 'newnum')
            ->from($this->getTableName())
            ->orderBy('newnum');
        $cursor = $qb->execute();
        $row = $cursor->fetch();
        if ($row === false) {
            $numid = 1;
        } else {
            $numid = intval($row['newnum']) + 1;
        }
        return $numid;
    }
}
