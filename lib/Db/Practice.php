<?php
namespace OCA\CeranaAdhesion\Db;

use JsonSerializable;
use OCP\AppFramework\Db\Entity;

class Practice extends Entity implements JsonSerializable
{
    protected $subscription;

    protected $name;

    protected $typepractice;

    protected $comment;

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->id,
            'subscription' => intval($this->subscription),
            'typepractice' => intval($this->typepractice),
            'comment' => $this->name,
        ];
    }
}
