<?php
namespace OCA\CeranaAdhesion\Db;

use JsonSerializable;
use OCP\AppFramework\Db\Entity;

class TypePractice extends Entity implements JsonSerializable
{

    protected $name;

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
        ];
    }
}
