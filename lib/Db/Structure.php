<?php
namespace OCA\CeranaAdhesion\Db;

use JsonSerializable;
use OCP\AppFramework\Db\Entity;

class Structure extends Entity implements JsonSerializable
{

    protected $name;

    protected $phone1;

    protected $phone2;

    protected $email;

    protected $website;

    protected $address;

    protected $postalcode;

    protected $city;

    protected $comment;

    protected $level;

    protected $parent;

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'phone1' => $this->phone1,
            'phone2' => $this->phone2,
            'email' => $this->email,
            'website' => $this->website,
            'address' => $this->address,
            'postalcode' => $this->postalcode,
            'city' => $this->city,
            'comment' => $this->comment,
            'level' => $this->level,
            'parent' => $this->parent,
        ];
    }
}
