<?php
namespace OCA\CeranaAdhesion\Db;

use JsonSerializable;
use OCP\AppFramework\Db\Entity;

class TypeResponsability extends Entity implements JsonSerializable
{

    protected $level;

    protected $name;

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->id,
            'level' => intval($this->level),
            'name' => $this->name,
        ];
    }
}
