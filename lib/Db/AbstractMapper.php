<?php
namespace OCA\CeranaAdhesion\Db;

use OCP\DB\QueryBuilder\IQueryBuilder;
use OCP\AppFramework\Db\QBMapper;

abstract class AbstractMapper extends QBMapper {

    protected function findArray(IQueryBuilder $query): array 
    {
        $cursor = $query->execute();
        $data = [];
        while ($row = $cursor->fetch()) {
            $data[] = $row;
        }
        $cursor->closeCursor();
        return $data;
    }
    
	protected function findEntitiesWithPage(IQueryBuilder $query, int $page, int $size): array 
	{
		$cursor = $query->execute();
		$nb_total = $cursor->rowCount();
		$query->setFirstResult($page*$size)->setMaxResults($size);
		$entities = $this->findEntities($query);
		return array("list" =>$entities, "total"=>$nb_total);
	}

	protected function findWithPage(IQueryBuilder $query, int $page, int $size): array 
	{
		$cursor = $query->execute();
		$nb_total = $cursor->rowCount();
		$query->setFirstResult($page*$size)->setMaxResults($size);
		$entities = $this->findArray($query);
		return array("list" =>$entities, "total"=>$nb_total);
	}

	public function getDB() {
		return $this->db;
	}

}