<?php
namespace OCA\CeranaAdhesion\Db;

use OCP\AppFramework\Db\DoesNotExistException;
use OCP\AppFramework\Db\Entity;
use OCP\DB\QueryBuilder\IQueryBuilder;
use OCP\IDBConnection;

class LevelMapper extends AbstractMapper
{

    public function __construct(IDBConnection $db)
    {
        parent::__construct($db, 'cerana_level', Level::class);
    }

    /**
     *
     * @param int $id
     * @return Entity|Level
     * @throws \OCP\AppFramework\Db\MultipleObjectsReturnedException
     * @throws DoesNotExistException
     */
    public function find(int $id): Level
    {
        /* @var $qb IQueryBuilder */
        $qb = $this->db->getQueryBuilder();
        $qb->select('*')
            ->from($this->getTableName())
            ->where($qb->expr()
            ->eq('id', $qb->createNamedParameter($id, IQueryBuilder::PARAM_INT)));
        return $this->findEntity($qb);
    }
   
    /**
     *
     * @param string $nom
     * @param string $prenom
     * @return array
     */
    public function findAll($page, $size): array
    {
        /* @var $qb IQueryBuilder */
        $qb = $this->db->getQueryBuilder();
        $qb->select('*')
            ->from($this->getTableName())
            ->orderBy('level')
            ->addOrderBy('name');
        return $this->findEntitiesWithPage($qb, $page, $size);
    }

}
