<?php
namespace OCA\CeranaAdhesion\Db;

use JsonSerializable;
use OCP\AppFramework\Db\Entity;

class Adherent extends Entity implements JsonSerializable
{

    protected $num;

    protected $civility;

    protected $lastname;

    protected $firstname;

    protected $birthdaydate;

    protected $birthdayplace;

    protected $email;

    protected $address;

    protected $postalcode;

    protected $city;

    protected $country;

    protected $phone1;

    protected $phone2;

    protected $comment;

    protected $photoid;

    protected $uid;

    public function jsonSerialize(): array
    {
        if ($this->country == '') {
            $this->country = 'FR';
        }
        return [
            'id' => $this->id,
            'num' => $this->num,
            'civility' => $this->civility,
            'lastname' => $this->lastname,
            'firstname' => $this->firstname,
            'birthdaydate' => $this->birthdaydate,
            'birthdayplace' => $this->birthdayplace,
            'email' => $this->email,
            'address' => $this->address,
            'postalcode' => $this->postalcode,
            'city' => $this->city,
            'country' => $this->country,
            'phone1' => $this->phone1,
            'phone2' => $this->phone2,
            'comment' => $this->comment,
            'photoid' => $this->photoid
        ];
    }
}
