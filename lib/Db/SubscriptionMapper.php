<?php
namespace OCA\CeranaAdhesion\Db;

use OCP\AppFramework\Db\DoesNotExistException;
use OCP\AppFramework\Db\Entity;
use OCP\DB\QueryBuilder\IQueryBuilder;
use OCP\IDBConnection;

class SubscriptionMapper extends AbstractMapper
{

    public function __construct(IDBConnection $db)
    {
        parent::__construct($db, 'cerana_subscription', Subscription::class);
    }

    /**
     * Creator
     *
     * @return PracticeMapper
     */
    public function createPractice()
    {
        return new PracticeMapper($this->getDB());
    }

    /**
     * Creator
     *
     * @return ResponsabilityMapper
     */
    public function createResponsability()
    {
        return new ResponsabilityMapper($this->getDB());
    }

    /**
     *
     * @param int $id
     * @return Entity|Subscription
     * @throws \OCP\AppFramework\Db\MultipleObjectsReturnedException
     * @throws DoesNotExistException
     */
    public function find(int $id): Subscription
    {
        /* @var $qb IQueryBuilder */
        $qb = $this->db->getQueryBuilder();
        $qb->select('*')
            ->from($this->getTableName())
            ->where($qb->expr()
            ->eq('id', $qb->createNamedParameter($id, IQueryBuilder::PARAM_INT)));
        return $this->findEntity($qb);
    }

    public function statistics($structure, $dateref): array
    {
        $qb = $this->db->getQueryBuilder();
        $qb->select('typeadhesion')
            ->selectAlias($qb->func()
            ->count('typeadhesion'), 'nb')
            ->from($this->getTableName());
        $qb->where($qb->expr()
            ->eq('dateref', $qb->createNamedParameter($dateref)));
        if ($structure != 0) {
            $qb->andWhere($qb->expr()
                ->eq('structure', $qb->createNamedParameter($structure, IQueryBuilder::PARAM_INT)));
        }
        $qb->groupBy('typeadhesion');
        $total = 0;
        $result = $this->findArray($qb);
        foreach ($result as $value) {
            $total += intval($value['nb']);
        }
        $result[] = array(
            'typeadhesion' => 0,
            'nb' => $total
        );
        return $result;
    }

    public function search($adherent, $dateref): Subscription
    {
        /* @var $qb IQueryBuilder */
        $qb = $this->db->getQueryBuilder();
        $qb->select('*')
            ->from($this->getTableName())
            ->where($qb->expr()
            ->eq('adherent', $qb->createNamedParameter($adherent, IQueryBuilder::PARAM_INT)))
            ->andWhere($qb->expr()
            ->eq('dateref', $qb->createNamedParameter($dateref)))
            ->setFirstResult(0)
            ->setMaxResults(1);
        return $this->findEntity($qb);
    }

    public function searchAdherent($adherent): array
    {
        /* @var $qb IQueryBuilder */
        $qb = $this->db->getQueryBuilder();
        $qb->select('*')
            ->from($this->getTableName())
            ->where($qb->expr()
            ->eq('adherent', $qb->createNamedParameter($adherent, IQueryBuilder::PARAM_INT)))
            ->orderBy('dateref', 'DESC');
        return $this->findEntities($qb);
    }

    /**
     *
     * @param string $nom
     * @param string $prenom
     * @return array
     */
    public function findAll($page, $size): array
    {
        /* @var $qb IQueryBuilder */
        $qb = $this->db->getQueryBuilder();
        $qb->select('*')
            ->from($this->getTableName())
            ->orderBy('dateref')
            ->addOrderBy('structure')
            ->addOrderBy('adherent');
        return $this->findEntitiesWithPage($qb, $page, $size);
    }
}
