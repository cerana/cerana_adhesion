import axios from 'axios'
import Vue from 'vue'
import VueI18n from 'vue-i18n'
import Vuex, { Store } from 'vuex'
import { messages } from './messagesI18n'

function getVueI18nTest() {
	Vue.use(VueI18n)
	return new VueI18n({
		locale: window.navigator.userLanguage || window.navigator.language,
		fallbackLocale: 'fr',
		messages,
	})
}
export const ceranaI18n = getVueI18nTest()
Vue.prototype.$transI18n = ceranaI18n

export function getCountries() {
	return [
		{ value: 'FR', name: 'France' },
		{ value: 'AF', name: 'Afghanistan' },
		{ value: 'ZA', name: 'Afrique du Sud' },
		{ value: 'AL', name: 'Albanie' },
		{ value: 'DZ', name: 'Algérie' },
		{ value: 'DE', name: 'Allemagne' },
		{ value: 'AD', name: 'Andorre' },
		{ value: 'AO', name: 'Angola' },
		{ value: 'AI', name: 'Anguilla' },
		{ value: 'AQ', name: 'Antarctique' },
		{ value: 'AG', name: 'Antigua-et-Barbuda' },
		{ value: 'AN', name: 'Antilles néerlandaises' },
		{ value: 'SA', name: 'Arabie saoudite' },
		{ value: 'AR', name: 'Argentine' },
		{ value: 'AM', name: 'Arménie' },
		{ value: 'AW', name: 'Aruba' },
		{ value: 'AU', name: 'Australie' },
		{ value: 'AT', name: 'Autriche' },
		{ value: 'AZ', name: 'Azerbaidjan' },
		{ value: 'BJ', name: 'Bénin' },
		{ value: 'BS', name: 'Bahamas' },
		{ value: 'BH', name: 'Bahrein' },
		{ value: 'BD', name: 'Bangladesh' },
		{ value: 'BB', name: 'Barbade' },
		{ value: 'PW', name: 'Belau' },
		{ value: 'BE', name: 'Belgique' },
		{ value: 'BZ', name: 'Belize' },
		{ value: 'BM', name: 'Bermudes' },
		{ value: 'BT', name: 'Bhoutan' },
		{ value: 'BY', name: 'Biéorussie' },
		{ value: 'MM', name: 'Birmanie' },
		{ value: 'BO', name: 'Bolivie' },
		{ value: 'BA', name: 'Bosnie-Herzégovine' },
		{ value: 'BW', name: 'Botswana' },
		{ value: 'BR', name: 'Brésil' },
		{ value: 'BN', name: 'Brunei' },
		{ value: 'BG', name: 'Bulgarie' },
		{ value: 'BF', name: 'Burkina Faso' },
		{ value: 'BI', name: 'Burundi' },
		{ value: 'CI', name: 'Cote d\'Ivoire' },
		{ value: 'KH', name: 'Cambodge' },
		{ value: 'CM', name: 'Cameroun' },
		{ value: 'CA', name: 'Canada' },
		{ value: 'CV', name: 'Cap-Vert' },
		{ value: 'CL', name: 'Chili' },
		{ value: 'CN', name: 'Chine' },
		{ value: 'CY', name: 'Chypre' },
		{ value: 'CO', name: 'Colombie' },
		{ value: 'KM', name: 'Comores' },
		{ value: 'CG', name: 'Congo' },
		{ value: 'KP', name: 'Corée du Nord' },
		{ value: 'KR', name: 'Corée du Sud' },
		{ value: 'CR', name: 'Costa Rica' },
		{ value: 'HR', name: 'Croatie' },
		{ value: 'CU', name: 'Cuba' },
		{ value: 'DK', name: 'Danemark' },
		{ value: 'DJ', name: 'Djibouti' },
		{ value: 'DM', name: 'Dominique' },
		{ value: 'EG', name: 'Egypte' },
		{ value: 'AE', name: 'Emirats arabes unis' },
		{ value: 'EC', name: 'Equateur' },
		{ value: 'ER', name: 'Erythrée' },
		{ value: 'ES', name: 'Espagne' },
		{ value: 'EE', name: 'Estonie' },
		{ value: 'US', name: 'Etats-Unis' },
		{ value: 'ET', name: 'Ethiopie' },
		{ value: 'FI', name: 'Finlande' },
		{ value: 'GE', name: 'Géorgie' },
		{ value: 'GA', name: 'Gabon' },
		{ value: 'GM', name: 'Gambie' },
		{ value: 'GH', name: 'Ghana' },
		{ value: 'GI', name: 'Gibraltar' },
		{ value: 'GR', name: 'Grèce' },
		{ value: 'GD', name: 'Grenade' },
		{ value: 'GL', name: 'Groenland' },
		{ value: 'GP', name: 'Guadeloupe' },
		{ value: 'GU', name: 'Guam' },
		{ value: 'GT', name: 'Guatemala' },
		{ value: 'GN', name: 'Guinée' },
		{ value: 'GQ', name: 'Guinée équatoriale' },
		{ value: 'GW', name: 'Guinée-Bissao' },
		{ value: 'GY', name: 'Guyana' },
		{ value: 'GF', name: 'Guyane francaise' },
		{ value: 'HT', name: 'Haiti' },
		{ value: 'HN', name: 'Honduras' },
		{ value: 'HK', name: 'Hong Kong' },
		{ value: 'HU', name: 'Hongrie' },
		{ value: 'BV', name: 'Ile Bouvet' },
		{ value: 'CX', name: 'Ile Christmas' },
		{ value: 'NF', name: 'Ile Norfolk' },
		{ value: 'KY', name: 'Iles Cayman' },
		{ value: 'CK', name: 'Iles Cook' },
		{ value: 'FO', name: 'Iles Féroé' },
		{ value: 'FK', name: 'Iles Falkland' },
		{ value: 'FJ', name: 'Iles Fidji' },
		{ value: 'GS', name: 'Iles Géorgie et Sandwich du Sud' },
		{ value: 'HM', name: 'Iles Heard et McDonald' },
		{ value: 'MH', name: 'Iles Marshall' },
		{ value: 'PN', name: 'Iles Pitcairn' },
		{ value: 'SB', name: 'Iles Salomon' },
		{ value: 'SJ', name: 'Iles Svalbard et Jan Mayen' },
		{ value: 'TC', name: 'Iles Turks-et-Caicos' },
		{ value: 'VI', name: 'Iles Vierges améicaines' },
		{ value: 'VG', name: 'Iles Vierges britanniques' },
		{ value: 'CC', name: 'Iles des Cocos [Keeling]' },
		{ value: 'UM', name: 'Iles mineures des Etats-Unis' },
		{ value: 'IN', name: 'Inde' },
		{ value: 'ID', name: 'Indonésie' },
		{ value: 'IR', name: 'Iran' },
		{ value: 'IQ', name: 'Iraq' },
		{ value: 'IE', name: 'Irlande' },
		{ value: 'IS', name: 'Islande' },
		{ value: 'IL', name: 'Israel' },
		{ value: 'IT', name: 'Italie' },
		{ value: 'JM', name: 'Jamaique' },
		{ value: 'JP', name: 'Japon' },
		{ value: 'JO', name: 'Jordanie' },
		{ value: 'KZ', name: 'Kazakhstan' },
		{ value: 'KE', name: 'Kenya' },
		{ value: 'KG', name: 'Kirghizistan' },
		{ value: 'KI', name: 'Kiribati' },
		{ value: 'KW', name: 'Koweit' },
		{ value: 'LA', name: 'Laos' },
		{ value: 'LS', name: 'Lesotho' },
		{ value: 'LV', name: 'Lettonie' },
		{ value: 'LB', name: 'Liban' },
		{ value: 'LR', name: 'Liberia' },
		{ value: 'LY', name: 'Libye' },
		{ value: 'LI', name: 'Liechtenstein' },
		{ value: 'LT', name: 'Lituanie' },
		{ value: 'LU', name: 'Luxembourg' },
		{ value: 'MO', name: 'Macao' },
		{ value: 'MG', name: 'Madagascar' },
		{ value: 'MY', name: 'Malaisie' },
		{ value: 'MW', name: 'Malawi' },
		{ value: 'MV', name: 'Maldives' },
		{ value: 'ML', name: 'Mali' },
		{ value: 'MT', name: 'Malte' },
		{ value: 'MP', name: 'Mariannes du Nord' },
		{ value: 'MA', name: 'Maroc' },
		{ value: 'MQ', name: 'Martinique' },
		{ value: 'MU', name: 'Maurice' },
		{ value: 'MR', name: 'Mauritanie' },
		{ value: 'YT', name: 'Mayotte' },
		{ value: 'MX', name: 'Mexique' },
		{ value: 'FM', name: 'Micronésie' },
		{ value: 'MD', name: 'Moldavie' },
		{ value: 'MC', name: 'Monaco' },
		{ value: 'MN', name: 'Mongolie' },
		{ value: 'MS', name: 'Montserrat' },
		{ value: 'MZ', name: 'Mozambique' },
		{ value: 'NP', name: 'Népal' },
		{ value: 'NA', name: 'Namibie' },
		{ value: 'NR', name: 'Nauru' },
		{ value: 'NI', name: 'Nicaragua' },
		{ value: 'NE', name: 'Niger' },
		{ value: 'NG', name: 'Nigeria' },
		{ value: 'NU', name: 'Nioué' },
		{ value: 'NO', name: 'Norvège' },
		{ value: 'NC', name: 'Nouvelle-Calédonie' },
		{ value: 'NZ', name: 'Nouvelle-Zélande' },
		{ value: 'OM', name: 'Oman' },
		{ value: 'UG', name: 'Ouganda' },
		{ value: 'UZ', name: 'Ouzbékistan' },
		{ value: 'PE', name: 'Pérou' },
		{ value: 'PK', name: 'Pakistan' },
		{ value: 'PA', name: 'Panama' },
		{ value: 'PG', name: 'Papouasie-Nouvelle-Guinée' },
		{ value: 'PY', name: 'Paraguay' },
		{ value: 'NL', name: 'Pays-Bas' },
		{ value: 'PH', name: 'Philippines' },
		{ value: 'PL', name: 'Pologne' },
		{ value: 'PF', name: 'Polynésie francaise' },
		{ value: 'PR', name: 'Porto Rico' },
		{ value: 'PT', name: 'Portugal' },
		{ value: 'QA', name: 'Qatar' },
		{ value: 'CF', name: 'République centrafricaine' },
		{ value: 'CD', name: 'Rép démocratique du Congo' },
		{ value: 'DO', name: 'République dominicaine' },
		{ value: 'CZ', name: 'République tchèque' },
		{ value: 'RE', name: 'Réunion' },
		{ value: 'RO', name: 'Roumanie' },
		{ value: 'GB', name: 'Royaume-Uni' },
		{ value: 'RU', name: 'Russie' },
		{ value: 'RW', name: 'Rwanda' },
		{ value: 'SN', name: 'Sénégal' },
		{ value: 'EH', name: 'Sahara occidental' },
		{ value: 'KN', name: 'Saint-Christophe-et-Niévès' },
		{ value: 'SM', name: 'Saint-Marin' },
		{ value: 'PM', name: 'Saint-Pierre-et-Miquelon' },
		{ value: 'VA', name: 'Saint-Siège' },
		{ value: 'VC', name: 'Saint-Vincent-et-les-Grenadines' },
		{ value: 'SH', name: 'Sainte-Hélène' },
		{ value: 'LC', name: 'Sainte-Lucie' },
		{ value: 'SV', name: 'Salvador' },
		{ value: 'WS', name: 'Samoa' },
		{ value: 'AS', name: 'Samoa américaines' },
		{ value: 'ST', name: 'Sao Tomé-et-Principe' },
		{ value: 'SC', name: 'Seychelles' },
		{ value: 'SL', name: 'Sierra Leone' },
		{ value: 'SG', name: 'Singapour' },
		{ value: 'SI', name: 'Slovénie' },
		{ value: 'SK', name: 'Slovaquie' },
		{ value: 'SO', name: 'Somalie' },
		{ value: 'SD', name: 'Soudan' },
		{ value: 'LK', name: 'Sri Lanka' },
		{ value: 'SE', name: 'Suède' },
		{ value: 'CH', name: 'Suisse' },
		{ value: 'SR', name: 'Suriname' },
		{ value: 'SZ', name: 'Swaziland' },
		{ value: 'SY', name: 'Syrie' },
		{ value: 'TW', name: 'Taiwan' },
		{ value: 'TJ', name: 'Tadjikistan' },
		{ value: 'TZ', name: 'Tanzanie' },
		{ value: 'TD', name: 'Tchad' },
		{ value: 'TF', name: 'Terres australes francaises' },
		{ value: 'IO', name: 'Terr. britannique de l\'Océan Indien' },
		{ value: 'TH', name: 'Thailande' },
		{ value: 'TL', name: 'Timor Oriental' },
		{ value: 'TG', name: 'Togo' },
		{ value: 'TK', name: 'Tokéaou' },
		{ value: 'TO', name: 'Tonga' },
		{ value: 'TT', name: 'Trinité-et-Tobago' },
		{ value: 'TN', name: 'Tunisie' },
		{ value: 'TM', name: 'Turkménistan' },
		{ value: 'TR', name: 'Turquie' },
		{ value: 'TV', name: 'Tuvalu' },
		{ value: 'UA', name: 'Ukraine' },
		{ value: 'UY', name: 'Uruguay' },
		{ value: 'VU', name: 'Vanuatu' },
		{ value: 'VE', name: 'Venezuela' },
		{ value: 'VN', name: 'Viet Nam' },
		{ value: 'WF', name: 'Wallis-et-Futuna' },
		{ value: 'YE', name: 'Yémen' },
		{ value: 'YU', name: 'Yougoslavie' },
		{ value: 'ZM', name: 'Zambie' },
		{ value: 'ZW', name: 'Zimbabwe' },
		{ value: 'MK', name: 'ex-Rép. yougoslave de Macédoine' },
	]
}

export function getCivilities() {
	return [
		{ value: 1, name: ceranaI18n.t('Homme') },
		{ value: 2, name: ceranaI18n.t('Femme') },
	]
}

Vue.use(Vuex)
export const ceranaStore = new Store({
	state: {
		countries: getCountries(),
		civilities: getCivilities(),
		seasons: null,
		levels: null,
		structures: null,
		typeadhesions: null,
		typeresponsabilities: null,
		typepractices: null,
		defaultSeason: undefined,
		defaultStructure: undefined,
		defaultTypeadhesion: undefined,
	},
	getters: {
		getSeasons: (state) => (option) => {
			if (option === undefined) {
				return state.seasons
			} else {
				const result = []
				for (const seasonidx in state.seasons) {
					const seasonitem = state.seasons[seasonidx]
					if (option.indexOf(seasonitem.value) === -1) {
						result.push(seasonitem)
					} else {
						result.push({ value: seasonitem.value, name: seasonitem.name + ' * ' })
					}
				}
				return result
			}
		},
		getLevels: (state) => (option) => {
			return state.levels
		},
		levelParent: (state) => (levelid) => {
			let num = 0
			for (const levelidx in state.levels) {
				if (state.levels[levelidx].value === levelid) {
					num = state.levels[levelidx].level
				}
			}
			return state.levels.filter(level => level.level === (num - 1)).map(level => level.value)
		},
		getStructures: (state, getters) => (option) => {
			if ((option === undefined) || (option.level === undefined)) {
				return state.structures
			} else {
				const levels = getters.levelParent(option.level)
				return state.structures.filter(structure => levels.indexOf(structure.level) !== -1)
			}
		},
		searchLevelOfStructure: (state) => (structureid) => {
			for (const structureidx in state.structures) {
				if (state.structures[structureidx].value === structureid) {
					return state.structures[structureidx].level
				}
			}
			return 0
		},
		getTypeadhesions: (state) => (option) => {
			return state.typeadhesions
		},
		getTyperesponsabilities: (state, getters) => (option) => {
			if ((option === undefined) || (option.structure === undefined)) {
				return state.typeresponsabilities
			} else {
				const levelid = getters.searchLevelOfStructure(option.structure)
				return state.typeresponsabilities.filter(typeresponsability => typeresponsability.level === levelid)
			}
		},
		getTypepractices: (state) => (option) => {
			if (state.typepractices === null) {
				return []
			} else {
				return state.typepractices
			}
		},
	},
	mutations: {
		clean(state) {
			state.seasons = null
			state.levels = null
			state.structures = null
			state.typeadhesions = null
			state.typeresponsabilities = null
			state.typepractices = null
			state.defaultSeason = undefined
			state.defaultStructure = undefined
			state.defaultTypeadhesion = undefined
		},
		defaultSeason(state, season) {
			state.defaultSeason = season
		},
		setSeasons(state, seasons) {
			state.seasons = seasons
		},
		setLevels(state, levels) {
			state.levels = levels
		},
		setStructures(state, structures) {
			state.structures = structures
		},
		defaultStructure(state, structure) {
			state.defaultStructure = structure
		},
		setTypeadhesions(state, typeadhesions) {
			state.typeadhesions = typeadhesions
		},
		defaultTypeadhesion(state, typeadhesions) {
			state.defaultTypeadhesion = typeadhesions
		},
		setTyperesponsabilities(state, typeresponsabilities) {
			state.typeresponsabilities = typeresponsabilities
		},
		setTypepractices(state, typepractices) {
			state.typepractices = typepractices
		},
	},
	actions: {
		async refresh(context) {
			await context.dispatch('refreshSeasons')
			await context.dispatch('refreshLevels')
			await context.dispatch('refreshStructures')
			await context.dispatch('refreshTypeadhesions')
			await context.dispatch('refreshTyperesponsabilities')
			await context.dispatch('refreshTypepractices')
		},
		refreshSeasons(context) {
			if (context.state.seasons === null) {
				const seasons = []
				const currentYear = new Date().getFullYear()
				for (let year = (currentYear + 1); year >= 2000; year--) {
					const beginDate = new Date(year, 8, 1)
					const endDate = new Date(year + 1, 7, 31)
					let seasonTitle = String(ceranaI18n.t('du %BEGIN% au %END%'))
					const options = { year: 'numeric', month: 'long', day: 'numeric' }
					seasonTitle = seasonTitle.replace('%BEGIN%', beginDate.toLocaleDateString(undefined, options))
					seasonTitle = seasonTitle.replace('%END%', endDate.toLocaleDateString(undefined, options))
					seasons.push({ value: `${year}-09-01`, name: seasonTitle })
				}
				context.commit('setSeasons', seasons)
			}
			if (context.state.defaultSeason === undefined) {
				const currentDate = new Date()
				const year = currentDate.getFullYear()
				if (currentDate.getMonth() < 8) {
					context.commit('defaultSeason', `${year - 1}-09-01`)
				} else {
					context.commit('defaultSeason', `${year}-09-01`)
				}
			}
		},
		refreshLevels(context) {
			if (context.state.levels === null) {
				axios.get('/apps/cerana_adhesion/levels')
					.then(response => {
						const levels = []
						const resLevels = response.data.list
						for (const levelidx in resLevels) {
							levels.push({ value: resLevels[levelidx].id, name: resLevels[levelidx].name, level: resLevels[levelidx].level })
						}
						context.commit('setLevels', levels)
					}).catch((e) => {
						console.error(e)
						context.commit('setLevels', [])
					})
			}
		},
		refreshStructures(context) {
			if (context.state.structures === null) {
				axios.get('/apps/cerana_adhesion/structures')
					.then(response => {
						const structures = [{ value: 0, name: '---' }]
						const resStructures = response.data.list
						for (const structureidx in resStructures) {
							let levelname = ''
							context.state.levels.forEach(function(level) {
								if (String(level.value) === String(resStructures[structureidx].level)) {
									levelname = '[' + level.name + '] '
								}
							})
							structures.push({ value: resStructures[structureidx].id, name: levelname + resStructures[structureidx].name, level: parseInt(resStructures[structureidx].level, 10) })
						}
						context.commit('setStructures', structures)
					}).catch((e) => {
						console.error(e)
						context.commit('setStructures', [])
					})
			}
			if (context.state.defaultStructure === undefined) {
				context.commit('defaultStructure', 0)
			}
		},
		refreshTypeadhesions(context) {
			if (context.state.typeadhesions === null) {
				axios.get('/apps/cerana_adhesion/type_adhesions')
					.then(response => {
						const resTypeadhesions = []
						const typeadhesions = response.data.list
						for (const typeadhesionidx in typeadhesions) {
							resTypeadhesions.push({ value: typeadhesions[typeadhesionidx].id, name: typeadhesions[typeadhesionidx].name })
						}
						context.commit('setTypeadhesions', resTypeadhesions)
						if ((context.state.defaultTypeadhesion === undefined) && (resTypeadhesions.length > 0)) {
							context.commit('defaultTypeadhesion', context.state.typeadhesions[0].value)
						}
					}).catch((e) => {
						console.error(e)
						context.commit('setTypeadhesions', [])
					})
			}
			if ((context.state.defaultTypeadhesion === undefined) && (context.state.typeadhesions !== null) && (context.state.typeadhesions.length > 0)) {
				context.commit('defaultTypeadhesion', context.state.typeadhesions[0].value)
			}
		},
		refreshTyperesponsabilities(context) {
			if (context.state.typeresponsabilities === null) {
				axios.get('/apps/cerana_adhesion/type_responsabilitys')
					.then(response => {
						const typeresponsabilities = []
						const resTyperesponsabilities = response.data.list
						for (const typeresponsabilityidx in resTyperesponsabilities) {
							typeresponsabilities.push({ value: resTyperesponsabilities[typeresponsabilityidx].id, name: resTyperesponsabilities[typeresponsabilityidx].name, level: resTyperesponsabilities[typeresponsabilityidx].level })
						}
						context.commit('setTyperesponsabilities', typeresponsabilities)
					}).catch((e) => {
						console.error(e)
						context.commit('setTyperesponsabilities', [])
					})
			}
		},
		refreshTypepractices(context) {
			if (context.state.typepractices === null) {
				axios.get('/apps/cerana_adhesion/type_practices')
					.then(response => {
						const typepractices = []
						const resTypepractices = response.data.list
						for (const typepracticeidx in resTypepractices) {
							typepractices.push({ value: resTypepractices[typepracticeidx].id, name: resTypepractices[typepracticeidx].name })
						}
						context.commit('setTypepractices', typepractices)
					}).catch((e) => {
						console.error(e)
						context.commit('setTypepractices', [])
					})
			}
		},
	},
})
Vue.prototype.$store = ceranaStore

export const adherentsFields = [
	{ id: 'num', value: ceranaI18n.t('N° adhérent'), type: 'label', default: '---' },
	{ id: 'civility', value: ceranaI18n.t('Genre'), type: 'select', default: 1, needed: true, options: 'civilities' },
	{ id: 'lastname', value: ceranaI18n.t('Nom'), type: 'text', default: '', needed: true },
	{ id: 'firstname', value: ceranaI18n.t('Prénom'), type: 'text', default: '', needed: true },
	{ id: 'birthdaydate', value: ceranaI18n.t('Date de naissance'), type: 'date', default: new Date('2000-01-01'), needed: true },
	{ id: 'birthdayplace', value: ceranaI18n.t('Lieu de naissance'), type: 'text', default: '', needed: false, nolisting: true },
	{ id: 'address', value: ceranaI18n.t('Adresse'), type: 'textarea', default: '', needed: true, large: true, nolisting: true },
	{ id: 'postalcode', value: ceranaI18n.t('Code postal'), type: 'text', default: '', needed: true, nolisting: true },
	{ id: 'city', value: ceranaI18n.t('ville'), type: 'text', default: '', needed: true, nolisting: true },
	{ id: 'country', value: ceranaI18n.t('Pays'), type: 'select', default: '??', needed: true, options: 'countries', nolisting: true },
	{ id: 'email', value: ceranaI18n.t('courriel'), type: 'email', default: '', needed: false },
	{ id: 'phone1', value: ceranaI18n.t('Tel. 1'), type: 'text', default: '', needed: false, nolisting: true },
	{ id: 'phone2', value: ceranaI18n.t('Tel. 2'), type: 'text', default: '', needed: false, nolisting: true },
	{ id: 'comment', value: ceranaI18n.t('Commentaires'), type: 'textarea', default: '', needed: false, large: true, nolisting: true },
	{ id: 'photoid', value: ceranaI18n.t('Photo'), type: 'file', default: '', needed: false, large: true, nolisting: true },
]

export function getOptionsData($store, options) {
	if ((typeof options === 'string') || (options instanceof String)) {
		return $store.state[options]
	} else if (Array.isArray(options)) {
		return options
	} else {
		return []
	}
}

export function filterValueByField($store, value, field) {
	let result = value
	if (field.options !== undefined) {
		result = field.default
		const optionList = getOptionsData($store, field.options)
		optionList.forEach(function(item) {
			if (String(item.value) === String(value)) {
				result = item.name
			}
		})
	}
	if (field.type === 'date') {
		const valdate = new Date(String(result))
		if (!isNaN(valdate.getTime())) {
			result = valdate.toLocaleDateString()
		}
	}
	return result
}
