import Vue from 'vue'
import NavAsso from './NavAsso'
import { ceranaI18n, ceranaStore } from './cerana_lib.js'
import axios from 'axios'
import { getRequestToken } from '@nextcloud/auth'

axios.defaults.headers.requesttoken = getRequestToken()

Vue.prototype.t = window.t
Vue.prototype.n = window.n
Vue.prototype.OC = window.OC
Vue.prototype.OCA = window.OCA

export default new Vue({
	el: '#content',
	render: h => h(NavAsso),
	ceranaI18n,
	ceranaStore,
})
