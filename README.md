# Application Nextcloud de gestion d'adhésions d'un reseau associatif

Cerana est une extension du logiciel [NextCloud](https://nextcloud.com/fr_FR/athome/).

Elle permet de gérer une association organisée en antennes locales ou une fédération d'associations locales.

Notons les principales fonctionnalités:
- Definission d'une organisation 
- Gestion d'adhérents
- Travail en équipe et collaboratif

Pour en savoir plus, rendez vous sur le site web du projet : https://www.cerana.org

 