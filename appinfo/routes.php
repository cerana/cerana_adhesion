<?php
return [
    'resources' => [
        'level' => [
            'url' => '/levels'
        ],
        'structure' => [
            'url' => '/structures'
        ],
        'type_adhesion' => [
            'url' => '/type_adhesions'
        ],
        'type_responsability' => [
            'url' => '/type_responsabilitys'
        ],
        'type_practice' => [
            'url' => '/type_practices'
        ],
        'adherent' => [
            'url' => '/adherents'
        ],
        'subscription' => [
            'url' => '/subscriptions'
        ],
        'responsability' => [
            'url' => '/responsabilitys'
        ],
        'practice' => [
            'url' => '/practices'
        ],
    ],
    'routes' => [
        [
            'name' => 'page#index',
            'url' => '/',
            'verb' => 'GET'
        ],
        [
            'name' => 'subscription#statistics',
            'url' => '/statistics',
            'verb' => 'GET'
        ],
        [
            'name' => 'adherent#connection',
            'url' => '/connection',
            'verb' => 'PUT'
        ],
    ]
];
