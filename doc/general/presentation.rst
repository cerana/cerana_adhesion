Présentation
============

*Cerana* est une application *NextCloud* (une extension) de gestion d'activité d'association avec antenne locale ou de fédération d'associations locales.

NextCloud
---------
*NextCloud* est une application Libre (Open Source) permettant la gestion collaborative.  

Très adaptée au milieu associatif, cette application "web" permet de rassembler en une seule solution logiciel, de nombreux besoins numériques: partage de documents, gestion de calendriers, suivi de contacts, appel audio/vidéo, outil de papotage en ligne, ...  

En étant "dans les nuages", *NextCloud* permet justement à un grand nombre d'adhérents d'une association de pouvoir manipuler ses données de n'importe où, que ce soit sur son ordinateur de maison que sur son téléphone.

*NextCloud* est Libre, il est donc possible de l'installer sur un serveur de confiance et ainsi protéger ses données ainsi que celles des adhérents de son association.
Il est bien sur accessible depuis n'importe qu'elle navigateur Web, mais on peut également installer sur son PC, son Mac ou son appareil mobile une application dédiée afin d'utiliser des spécificité (synchronistation de document, ...). 
Reposant sur des protocoles de communication standard, il peut également être synchroniser avec d'autres application (calendrier, répertoire, ...)

Pour en savoir plus, rendez vous sur `le site officiel de NextCloud <https://nextcloud.com/fr_FR/athome/>`_.

L'extension Cerana
------------------
*Cerana* est donc une application *NextCloud* adaptée à la gestion spécifique d'adhésion.  

Les associations avec antenne locale, ou les fédérations d'associations, ont un besoin spécifique de gestion de leurs adhérents (ou licencier).  
En effet, ces derniers sont affiliés principalement sur leur antenne locale mais il peuvent également avoir des responsabilités sur d'autres échelons de l'organisation.  
De même, des responsables investis veulent pouvoir échanger des informations, tout en prenant en compte la spécificité de leur organisation locale.

*Cerana* se veut donc un outil, intégré dans l'application *NextCloud*, permettant non seulement de gérer au plus simple et de façon sécurisée les adhésions de chaque structure mais dans une optique nationale, tout en donnant les possibilités d'échanges et de collaboration de *NextCloud*.

Pour en savoir plus sur cette extension, `le site dédié de Cerana <https://www.cerana.org>`_ répondra à vos questions.
