Les utilisateurs Cerana
=======================

Toute fonctionnalité est faite pour donner un gain à un utilisateur.  
Afin de permettre à toutes personnes de contribuer à l'évolution de *Cerana*, nous définirons ici 4 grands profils utilisateurs pour homogénéiser nos réflexions.

L'administrateur
----------------
Cet utilisateur a les droits de tout faire sur *Cerana* et même plus généralement sur *NextCloud*.  
Ce sera donc lui qui réalisera les configurations de l'outil afin de l'adapter au besoin d'une *organisation*.

Le responsable
--------------
C'est un *adhérent responsable* qui a en charge une partie de la gestion d'une structure (*échelon local*, *échelon intermédiaire* ou *échelon national*).  
Son action de gestion des adhérents doit lui permettre de réaliser sa mission.

Le bénévole
-----------
C'est un *adhérent* qui a une responsabilité limitée qui lui permet de consulter les informations restreintes à sa structure de rattachement.  
Il a par contre accès à un certain nombre d'outils collaboratifs *NextCloud* lui permettant de réaliser son éventuel mission.

Le pratiquant
-------------
C'est un *adhérent* sans aucune responsabilité. Il ne participe qu'aux activités proposées par son *antenne locale*.
Il a la possibilité de consulter les informations lui concernant dans l'esprit de la protection des données (RGPD)
