Généralités
===========

Chapitre traitant de généralités sur *Cerana*.

.. toctree::
   :maxdepth: 2

   presentation.rst
   installation.rst
   definition.rst
   utilisateurs.rst
