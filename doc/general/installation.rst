Installation de Cerana
======================

L'installation de *Cerana* nécessite 2 principales étapes: l'accès à une instance *NextCloud* et l'installation de l'application Cerana proprement dite.

Utiliser un hébergement NextCloud
---------------------------------

De nombreux acteurs et hébergeurs sur internet proposent un hébergement du logiciel *NextCloud*.

Nous vous invitons à étudier les propositions des hébergeurs rassemblés dans le collectif `CHATONS <https://chatons.org/>`_ (dont `Sleto <https://www.sleto.net/>`_, notre partenaire officiel, fait partie).
Vous pourrez trouver des solutions d'hébergement qui correspondent à vos besoins et qui suivent une `charte <https://chatons.org/charte>`_ visant à respecter l'usage et à "dé-googliser" internet.

Installer son propre NextCloud
------------------------------

Si vous êtes un professionnel de l'informatique, ou même juste un peu bidouilleur, vous pouvez également installer votre propre *NextCloud*.

Pour cela, ne nombreux tutoriels se trouvent en ligne sur internet pour vous y aider.
Nous pouvons vous recommander celui de *framasoft*, en français et précis: `Installation de Nextcloud <https://framacloud.org/fr/cultiver-son-jardin/nextcloud.html>`_

Installer l'extension Cerana
----------------------------

Commencez par télécharger le paquet : `Cerana.zip <https://www.sd-libre.fr/download/Cerana.zip>`_

Ensuite, il faut l'extraire dans le sous répertoire **apps** de votre installation *NextCloud*.
Vérifiez également les permissions de ces nouveaux fichiers pour ne pas avoir de souci d'accessibilité.

Reconnectez vous à votre instance *NextCloud* en tant qu'administrateur (compte créé à l'installation). 
Allez dans la gestion des applications, recherchez l'application "cerana_adhesion" et activez-là.
L'outil va alors configurer par défaut l'extension: une nouvelle icone devrait alors apparaître dans la barre d'application *NextCloud*.

