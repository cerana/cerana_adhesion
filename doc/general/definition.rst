Définitions
===========

Cet article permet de définir un ensemble de termes et de vocabulaire utilisés dans l'outil.  
Ce ne sont pas des définitions "génériques" mais un vocabulaire utilisé dans l'outil *Cerana* qu'il est bon de préciser afin d'éviter des contre-sens suivant les différentes organisations.

L'organisation
--------------
Cela renvoie à votre entité nationale:
 * Dans le cas où vous êtes une seule association découpée en antennes locales, *l'organisation* sera donc la structure juridique complète de votre association.
 * Dans le cas où vous êtes rassemblés en une fédération (associant plusieurs associations locales adhérentes), *l'organisation* sera donc la structure juridique fédérale.

L'antenne locale
----------------
L'antenne locale est la structure organisationnelle qui propose la *pratique* de l'*organisation*.

Sa définition dépend également de votre organisation:
 * Si votre organisation correspond à une seule association nationale, vos antennes locales sont donc une entité de fonctionnement n'ayant pas de reconnaissance juridique propre, elles dépendent des statuts nationaux de l'association.
 * Si votre organisation correspond à une fédération, vos antennes locales sont donc des associations autonomes avec des statuts juridiques propres (et donc un président, un bureau, une A.G. spécifique, etc ...).

Structure & échelon
-------------------
Dans *Cerana*, la notion de *structure* regroupe plusieurs entités organisationnelles.  
Pour distinguer le niveau organisationnel de chaque structure, nous utiliserons la notion d'*échelon*

 * *L'échelon local* correspond à des structures d'*antennes locales*.
 * *L'échelon national* sera logiquement organisé autour d'une *structure* nationale gérant *l'organisation*
 * Un *échelon intermédiaire* correspond à toute structure servant à décentraliser *l'échelon national* (régionalement ou départementalement)  sur l'animation des *antennes locales*.

Un adhérent
-----------
Un *adhérent* est une personne physique qui souscrit via une *cotisation* à *l'organisation* par l'intermédiaire d'une *antenne locale*

Cotisation
----------
Une *cotisation* est une participation qu'un *adhérent* donne annuellement pour rejoindre *l'organisation*.  
Généralement, cette *cotisation* est prise au niveau d'une *antenne locale* mais elle pourrait également être pris au niveau d'un autre *échelon*.
Le montant peut être différent suivant la nature de son adhésion dans *l'organisation*

Saison
------
Période de temps d'une année de validité d'une *cotisation*.

Une pratique
------------
Une *pratique* est une activité que *l'adhérent* souhaite réaliser/faire/vivre au sein d'une *antenne locale* de *l'organisation*.

Responsable
-----------
Un *responsable* est un *adhérent* qui est en charge de faire vivre toujours plus de *pratiques*.  
Cette *responsabilité* peut être réalisée sur une *antenne locale*, dans une *structure intermédiaire* ou dans la *structure nationale*.  
Un *responsable* peut aussi bien être bénévole que salarié au sein d'une structure de l'*organisation*.

Adhérent associé
----------------
Un *adhérent associé* est une personne qui choisit de prendre une *cotisation* dans *l'organisation* afin de la soutenir sans participer à une *pratique* ni prendre une *responsabilité*
