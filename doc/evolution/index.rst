Évolution de l'outil
====================

*Cerana* est un outil libre, l'ensemble du code source est disponible ainsi que les manuels et les textes.

Ce chapitre explique comment vous pouvez proposer une amélioration que ce soit sur un élément textuel ou sur une évolution fonctionnel du logiciel.

.. toctree::
   :maxdepth: 2

   libre.rst
   manuel.rst
   bugs.rst
   nouvelle_fonctionnalite.rst
