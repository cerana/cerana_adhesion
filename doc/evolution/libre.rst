Qu'est-ce qu'un logiciel libre ?
================================

*Cerana* est un logiciel libre, c'est à dire qu'il respecte ces 4 critères de libertés :

 * La liberté d'exécuter le programme comme vous le souhaitez, et à toute fin (liberté 0);
 * La liberté d'étudier le fonctionnement du programme, et le modifier comme vous le souhaitez pour votre ordinateur (liberté 1);
 * La liberté de redistribuer les copies que vous avez reçues (liberté 2);
 * La liberté de distribuer des copies de vos versions modifiées pour donner à toute la communauté une chance de profiter de vos modifications (liberté 3).
