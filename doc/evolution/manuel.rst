Ce manuel
=========

Ce manuel, comme le reste de l'outil, est quelque chose de vivant.
En constante évolution, il tachera de reflèter au mieux l'outil.

Compléter le manuel
-------------------

Et si vous voulez proposer une amélioration de la documentation, voilà la procédure:

 * Créez un compte sur `framagit.org <https://framagit.org>`_
 * Allez dans le projet `Cerana <https://framagit.org/cerana/cerana_adhesion>`_
 * Cliquez sur "Clone" pour dupliquer le projet dans votre espace personnel
 * Depuis votre espace, vous pouvez modifier la documentation se trouvant dans le sous répertoire "doc"
 * Ajouter des pages ou des images suivant le besoin
 * Ce manuel utilise la notation `reStructuredTest <https://fr.wikipedia.org/wiki/ReStructuredText>`_
 * Une fois vos modifications prêtent, cliquez sur "merge request" pour les soumettre dans l'espace d'origine
 * Nous pourrons alors intégrer vos propositions et la proposer à tous lors d'une prochaine mise à jour.

Merci d'avance de votre collaboration.
 