Les anomalies
=============

Rédiger l'anomalie
------------------

Il est possible que vous observiez une anomalie (ou bug) dans l'utilisation de l'application *Cerana*.

Merci alors de créer un ticket sur `la page framagit dédiée <https://framagit.org/cerana/cerana_adhesion/-/issues>`_.
Avant, merci de bien vérifier que ce problème n'a pas déjà été soumi.
Dans ce cas, nous vous invitons à plutôt compléter un précédent ticket plutôt que d'en créer un nouveau.

Dans ce ticket, précisez:
 * La description du problème: message d'erreur, mauvais comportement, ...
 * L'enchainement qui vous a fait obtenir cela
 * Votre contexte d'utilisation (PC, mobile, navigateur web utilisé, ...)
 * Catégorisez le problème, via un des labels proposés

Soyez le plus précis possible dans l'explication du problème, de comment vous l'avez eu.
L'idée est de pouvoir le reproduire sur un autre environnement.
Idéalement, si vous pouvez l'observer également sur https://demo.cerana.org, c'est mieux.

Corriger une anomalie
---------------------

Pour développer l'application *NextCloud*, il est nécessaire de maitriser les compétences: php, html, css, javascript et vue.js.
Voilà quelques références spécifiques au développement dans *NextCloud*:

 * `Develop for Nextcloud <https://nextcloud.com/developer/>`_
 * `Tutorial <https://docs.nextcloud.com/server/19/developer_manual/app/tutorial.html>`_
 * `Nextcloud Vue Style Guide <https://nextcloud-vue-components.netlify.app/>`_

Et si vous êtes un développeur motivé et que voulez proposer une correction, voilà la procédure:

 * Créez un compte sur `framagit.org <https://framagit.org>`_
 * Allez dans le projet `Cerana <https://framagit.org/cerana/cerana_adhesion>`_
 * Cliquez sur "Clone" pour dupliquer le projet dans votre espace personnel
 * Depuis votre espace, vous pouvez réaliser toutes les modifications que vous voulez.
 * Une fois la correction prête, cliquez sur "merge request" pour la soumettre dans l'espace d'origine.
   Merci de faire une soumission par "merge request" différent afin de pouvoir gérer indépendamment telle ou telle proposition. 
   Précisez la référence d'un ticket qui correspondrait (totalement ou partiellement) à l'anomalie.
 * Nous pourrons alors intégrer la correction et la proposer à tous lors d'une prochaine mise à jour.
 
 Merci d'avance de votre collaboration.
 