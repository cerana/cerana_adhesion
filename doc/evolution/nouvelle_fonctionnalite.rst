Rédiger une future fonctionnalité
=================================

Cerana en constante évolution
-----------------------------

*Cerana* est un outil libre et gratuit, reposant sur une communauté d'utilisateurs actifs et impliqués dans son évolution.
Il est donc logique, qu'en tant qu'utilisateur, vous ayez des idées d'évolutions.

Si vous souhaitez rédiger une telle idée, vous êtes invités à visiter `la page framagit dédiée au évolution Cerana <https://framagit.org/cerana/cerana_adhesion/-/issues>`_.
De là, créez un nouveau ticket décrivant, comme précisé ci-dessous, votre fonctionnalité.

Avant de rédiger une nouvelle demande, vérifiez qu'un précédent ticket n'est pas déjà rédigé.
Il serait alors intéressant d'éventuellement le compléter.
De plus, en le commentant vous notifierez que ce besoin est commun à plusieurs, donnant alors plus de poids à cette demande.

Rédiger une histoire utilisateur
--------------------------------
Une histoire utilisateur est une demande fonctionnelle basée sur l’un des utilisateurs ci-dessus qui va rajouter de la valeur à l'outil.  
Elle sera écrite dans un langage naturel compris par tous.  

Pour cela, vous devez définir:
 * Un titre court, de quelques mots afin de classer ou répertorier toute les histoires.
 * Une description fonctionnelle.    
 	Pour cela, utilisez la formulation *"En tant que [utilisateur], je souhaite [souhait] afin de [but]"*.
 * Des précisions ou des conditions à cette histoire.    
	ex: liste de champs à remplir, valeur interdite, ...
 * Des exemples d'utilisation avec des cas qui se passent bien et éventuellement d'autres qui provoquent des erreurs.
 * Assignez aussi un ou des *étiquettes* afin d'identifier la catégories d'évolution de cette fonctionnalité.
 
**Examples:**

::

	En tant que pratiquant, je veux pouvoir consulter ou modifier ma fiche personnel afin de contrôler l`ensemble de mes informations gérés par l`organisation.
	
	Condition: avoir un accès de connexion NextCloud associé à sa fiche
	Visibilité: l`ensemble de ses informations personnels ainsi que l`historique de ses cotisations.

::

	Entant que responsable, je veux pouvoir visualisé des statistiques d`une structure afin de connaître le nombre de cotisation, de pratiques et de responsables.
	
	Information à saisir:
	- Structure à préciser
	- Si aucun structure, les statistiques se font sur l`ensemble des données nationales
	- Saison d`analyse
	
	Affichage:
	
	- statistiques des cotisations par type
	- statistiques des pratiques par type
	- statistiques des responsabilité par type

Développer une nouvelle fonctionnalité
--------------------------------------

Pour développer l'application *NextCloud*, il est nécessaire de maitriser les compétences: php, html, css, javascript et vue.js.
Voilà quelques références spécifiques au développement dans *NextCloud*:

 * `Develop for Nextcloud <https://nextcloud.com/developer/>`_
 * `Tutorial <https://docs.nextcloud.com/server/19/developer_manual/app/tutorial.html>`_
 * `Nextcloud Vue Style Guide <https://nextcloud-vue-components.netlify.app/>`_

Et si vous êtes un développeur motivé et que voulez proposer une amélioration, voilà la procédure:

 * Créez un compte sur `framagit.org <https://framagit.org>`_
 * Allez dans le projet `Cerana <https://framagit.org/cerana/cerana_adhesion>`_
 * Cliquez sur "Clone" pour dupliquer le projet dans votre espace personnel
 * Depuis votre espace, vous pouvez réaliser toutes les modifications que vous voulez
 * Une fois l'amélioration prête, cliquez sur "merge request" pour la soumettre dans l'espace d'origine. 
   Merci de faire une soumission par "merge request" différent afin de pouvoir gérer indépendamment telle ou telle proposition
 * Nous pourrons alors intégrer votre propositions et la proposer à tous lors d'une prochaine mise à jour.

Merci d'avance de votre collaboration.
 