Les adhésions
=============

Ce chapitre est un manuel d'utilisation de la gestion des adhésions dans *Cerana*.

.. toctree::
   :maxdepth: 2

   configuration.rst
   strutures.rst
   adherents.rst
   cotisations.rst
   statistiques.rst
